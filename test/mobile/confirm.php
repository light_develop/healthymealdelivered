<?php
if(isset($_POST["payment"]) && isset($_POST["shipping"]))
{
    $_SESSION["customerPaymentProfileId"] = $_POST["payment"];
    $_SESSION["customerShippingAddressId"] = $_POST["shipping"];
}
else if(!(isset($_SESSION["customerPaymentProfileId"]) && isset($_SESSION["customerShippingAddressId"])))
{
    printf("<script>location.href='payment.php';</script>");
}

if(isset($_POST["saveinfo"]))
{
    $_SESSION["saveinfo"] = $_POST["saveinfo"];
}

$cartContent = getCartContent();
$numItem = count($cartContent);

$subCoupon = 0;
$subTotal = 0;
for ($i = 0; $i < $numItem; $i++)
{
    extract($cartContent[$i]);
    $subTotal += $pd_price * $ct_qty;
?>

<div class="confirmItem">
    <span class="confirmFirstLine">
        <span class="confirmItemName"><?php echo $pd_name . " Shake <br />& " . $tea_name . " Tea"; ?></span>
        <span class="confirmItemPrice"><?php echo displayAmount($pd_price); ?></span>
    </span>
    <span class="confirmSub"><strong>subtotal</strong><?php echo displayAmount($pd_price * $ct_qty); ?></span>
    <span class="confirmQty">x <span class="confirmQtyNum"><?php echo $ct_qty; ?></span></span>
</div>

<?php
}
$_SESSION["subTotal"] = $subTotal;

$subFour = 0;
$days = 0;
$cdate = strtotime("this friday");
//$difference = $cdate - $today;
$delivery = strtotime(getDeliveryDate());
$difference = $cdate - $delivery;

if ($difference < 0) {
    $difference = 0;
}//delivery is friday or later
$days = floor($difference / 60 / 60 / 24);

if(isset($_SESSION["FiveDayOrder"]) && $_SESSION["FiveDayOrder"]==1)
{
    $subFour = $subTotal * $days;
    
    echo "<div class=\"confirmItem\">
            <span class=\"confirmItemPrice\" style=\"margin-top: .5em;\">".displayAmount($subFour)."</span>
            Repeat Order <span class=\"confirmQty\"><span class=\"confirmQtyNum\">$days days</span></span>
        </div>";
}
?>

<div class="cartTotal">
    <span id="wORepeat" class="totalWORepeat"><?php echo displayAmount($subTotal + $subFour - $subCoupon); ?></span>
    <span class="totalLabel">Total</span>
</div>

<div class="delivery">
    The first order will be delivered on <strong><?php echo date("m/d/Y", $delivery); ?></strong>
</div>

<a data-role="button" data-transition="slide" data-theme="b" href="checkout.php?step=3">
    Place Order
</a>