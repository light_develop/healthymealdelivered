<?php
include_once ("../../shared/authorize/php_cim/XML/vars.php");
include_once ("../../shared/authorize/php_cim/XML/util.php");
include_once ("../../shared/library/config.php");

$custProfID = null;
if(!isset($_SESSION['custProfID'])) 
{
    //header( 'Location: '.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/cart.php' ) ;
    printf("<script>location.href='http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/cart.php'</script>");
}

if(isset($_POST["chkFive"]))
{
    $_SESSION['FiveDayOrder'] = 1;
}
else if(isset($_POST["fromForm"]))
{
    $_SESSION['FiveDayOrder'] = 0;
}


$custProfID = $_SESSION['custProfID'];
$request = '<?xml version="1.0" encoding="utf-8"?>
                    <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                        '.MerchantAuthenticationBlock().'
                        <customerProfileId>'.$custProfID.'</customerProfileId>
                        <hostedProfileSettings>
                            <setting>
                                <settingName>hostedProfileReturnUrl</settingName>
                                <settingValue>http://'.BASE_URL.$_SESSION['organization'].'/mobile/checkout.php?step=1</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfileReturnUrlText</settingName>
                                <settingValue>Continue to checkout</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfilePageBorderVisible</settingName>
                                <settingValue>true</settingValue>
                            </setting>
                        </hostedProfileSettings>
                    </getHostedProfilePageRequest>';
        
$response = send_xml_request($request);

$parsedresponse = parse_api_response($response);
if ("Ok" == $parsedresponse->messages->resultCode) {
    $token = $parsedresponse->token;

}

$request = '<?xml version="1.0" encoding="utf-8"?>
            <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                '.MerchantAuthenticationBlock().'
                <customerProfileId>'.$custProfID.'</customerProfileId>
            </getCustomerProfileRequest>';

$response = send_xml_request($request);

$parsedresponse = parse_api_response($response);
if ("Ok" == $parsedresponse->messages->resultCode) {
    $paymentProfiles = $parsedresponse->profile;
    $shipTo = $parsedresponse->profile->shipToList;//array of all available shipping addresses
    
}
echo'
    <form method="post" action="https://test.authorize.net/profile/manage"
    id="formAuthorizeNetPage" style="display:none;">
    <input type="hidden" name="Token" value="'.$token.'"/>
    </form>';
?>
<h1>Choose Payment Method</h1>

<?php
if(isset($cardError) && $cardError == 1)
{
?>
<div style="color: red;">There was an issue with your payment method. Please review your information and try again.</div>
<?php } ?>


<form method="post" action="checkout.php?step=2" data-ajax="false" onsubmit="return validate();" >
<div data-role="fieldcontain">
    <fieldset data-role="controlgroup" data-type="vertical">
        <legend>
            Payment Method
        </legend>
    <?php 
        foreach($paymentProfiles->paymentProfiles as $profile)
        {
            $selected = '';
            $type;
            $label;
            $number;
            $paymentProfileID = $profile->customerPaymentProfileId;
            if($profile->payment->creditCard)
            {
                $label = 'Card ending in ';
                $number = $profile->payment->creditCard->cardNumber;

            }
            else
            {
                $label = 'Account number ending in';
                $number = $profile->payment->bankAccount->accountNumber;
            }
            
            if(isset($_SESSION["customerPaymentProfileId"]) && $_SESSION["customerPaymentProfileId"] == $paymentProfileID)
            {
                $selected = "checked='checked'";
            }
            else if(!isset($_SESSION["customerPaymentProfileId"]))
            {
                $_SESSION["customerPaymentProfileId"] = $paymentProfileID;
                $selected = "checked='checked'";
            }
            
            echo "            
                <input id=\"$paymentProfileID\" name=\"payment\" value=\"$paymentProfileID\" type=\"radio\" $selected>
                <label for=\"$paymentProfileID\">
                    $label $number
                </label>";
        }
    ?>
    </fieldset>
    
    <br />
    
    <fieldset data-role="controlgroup" data-type="vertical">
        <legend>
            Shipping Address
        </legend>
        
        <?php 
        
            foreach($paymentProfiles->shipToList as $ships)
            {
                $selected = '';
                $customerShippingAddressId = $ships->customerAddressId;
                
                if(isset($_SESSION["customerShippingAddressId"]) && $_SESSION["customerShippingAddressId"] == $customerShippingAddressId)
                {
                    $selected = "checked='checked'";
                }
                else if(!isset($_SESSION["customerShippingAddressId"]))
                {
                    $_SESSION["customerShippingAddressId"] = $customerShippingAddressId;
                    $selected = "checked='checked'";
                }
                
                echo "                
                    <input id=\"$customerShippingAddressId\" name=\"shipping\" value=\"$customerShippingAddressId\" type=\"radio\" $selected>
                    <label for=\"$customerShippingAddressId\">
                        $ships->firstName 
                        $ships->lastName</br>
                        $ships->address</br>
                        $ships->city, $ships->state $ships->zip
                    </label>";
            }
            
            $info = 'selected="selected"';
            if(!(isset($_SESSION['saveinfo']) && $_SESSION['saveinfo'] == 1))
            {
                $info = '';
            }
        ?>
        
        
    </fieldset>
    <div class="payEdit">
        <a href="#" onClick="document.getElementById('formAuthorizeNetPage').submit();">Edit Payment and Shipping Information</a>
    </div>
</div>
<div data-role="fieldcontain" style="text-align: right;">
    <label for="toggleswitch1">Save this information for next time?</label>
    <select name="saveinfo" id="saveinfo" data-theme="" data-role="slider">
        <option value="0">
            No
        </option>
        <option value="1" <?php echo $info; ?>>
            Yes
        </option>
    </select>
</div>
<input data-theme="b" value="Continue" data-transition="slide" type="submit">
</form>