-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 06 2015 г., 13:56
-- Версия сервера: 5.5.35-cll-lve
-- Версия PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `nutrition`
--

-- --------------------------------------------------------

--
-- Структура таблицы `reseller_user`
--

CREATE TABLE IF NOT EXISTS `reseller_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reseller_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_blacklist`
--

CREATE TABLE IF NOT EXISTS `tbl_blacklist` (
  `email` varchar(255) NOT NULL,
  `reseller_id` int(11) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='emails in this table are not allowed to create accounts';

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_cart`
--

CREATE TABLE IF NOT EXISTS `tbl_cart` (
  `ct_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ct_line` int(10) unsigned NOT NULL,
  `pd_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tea_id` int(10) unsigned NOT NULL,
  `ct_qty` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `ct_session_id` char(32) NOT NULL DEFAULT '',
  `ct_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ct_id`),
  KEY `pd_id` (`pd_id`),
  KEY `ct_session_id` (`ct_session_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=236 ;

--
-- Дамп данных таблицы `tbl_cart`
--

INSERT INTO `tbl_cart` (`ct_id`, `ct_line`, `pd_id`, `tea_id`, `ct_qty`, `ct_session_id`, `ct_date`) VALUES
(233, 0, 50, 202, 1, '5b02fb634edfde99f100554713c87334', '2015-07-28 13:12:38'),
(232, 0, 60, 201, 1, 'c198bdd04cbd85fb221b4b313fcf97b0', '2015-07-28 07:25:20');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_parent_id` int(11) NOT NULL DEFAULT '0',
  `cat_name` varchar(50) NOT NULL DEFAULT '',
  `cat_description` varchar(200) NOT NULL DEFAULT '',
  `cat_image` varchar(255) NOT NULL DEFAULT '',
  `reseller_id` int(11) NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `cat_parent_id` (`cat_parent_id`),
  KEY `cat_name` (`cat_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Дамп данных таблицы `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `cat_parent_id`, `cat_name`, `cat_description`, `cat_image`, `reseller_id`) VALUES
(23, 0, 'Cookies and Cream Shakes', 'Cookies and Cream', '', 1),
(22, 0, 'Vanilla Shakes', 'Vanilla', '', 1),
(18, 0, 'Chocolate Shakes', 'Chocolate', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_currency`
--

CREATE TABLE IF NOT EXISTS `tbl_currency` (
  `cy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cy_code` char(3) NOT NULL DEFAULT '',
  `cy_symbol` varchar(8) NOT NULL DEFAULT '',
  PRIMARY KEY (`cy_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_currency`
--

INSERT INTO `tbl_currency` (`cy_id`, `cy_code`, `cy_symbol`) VALUES
(1, 'EUR', '&#8364;'),
(2, 'GBP', '&pound;'),
(3, 'JPY', '&yen;'),
(4, 'USD', '$');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_holiday`
--

CREATE TABLE IF NOT EXISTS `tbl_holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `name` varchar(90) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `repeat_yearly` tinyint(1) NOT NULL DEFAULT '0',
  `reseller_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `date` (`date`,`reseller_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=225 ;

--
-- Дамп данных таблицы `tbl_holiday`
--

INSERT INTO `tbl_holiday` (`id`, `date`, `name`, `active`, `repeat_yearly`, `reseller_id`) VALUES
(217, '2015-06-18', 'Herbalife vacation', 1, 0, 1),
(216, '2015-06-17', 'Herbalife vacation', 1, 0, 1),
(215, '2015-06-16', 'Herbalife vacation', 1, 0, 1),
(214, '2015-06-15', 'Herbalife vacation', 1, 0, 1),
(213, '2015-06-12', 'Herbalife vacation', 1, 0, 1),
(212, '2015-06-11', 'Herbalife vacation', 1, 0, 1),
(210, '2015-12-25', 'Christmas Day', 1, 1, 1),
(211, '2015-12-31', 'New Years Eve', 1, 1, 1),
(209, '2015-11-26', 'Thanksgiving Day', 1, 1, 1),
(208, '2015-11-11', 'Veteran''s Day', 1, 0, 1),
(207, '2015-10-12', 'Columbus Day', 1, 0, 1),
(206, '2015-09-07', 'Labor Day', 1, 1, 1),
(205, '2015-07-03', 'Independence Day', 1, 1, 1),
(204, '2015-05-25', 'Memorial Day', 1, 1, 1),
(203, '2015-02-16', 'President''s Day', 0, 0, 1),
(202, '2015-01-19', 'MLK Day', 1, 0, 1),
(201, '2015-01-01', 'New Years Day', 1, 1, 1),
(218, '2015-06-19', 'Herbalife vacation', 1, 0, 1),
(219, '2015-06-22', 'Herbalife vacation', 1, 0, 1),
(220, '2015-06-10', 'Herbalife vacation', 1, 0, 1),
(221, '2015-06-23', 'Herbalife vacation', 1, 0, 1),
(222, '2015-07-06', 'Independence Vacation', 1, 0, 1),
(223, '2015-07-07', 'Independence Vacation', 1, 0, 1),
(224, '2015-07-08', 'Independence Holiday', 1, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_invalid_cards`
--

CREATE TABLE IF NOT EXISTS `tbl_invalid_cards` (
  `payment_profile_id` int(11) NOT NULL,
  `mem_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_profile_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_members`
--

CREATE TABLE IF NOT EXISTS `tbl_members` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `organization_id` int(11) NOT NULL,
  `name` varchar(65) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=suspended, 1=active',
  `email` varchar(65) NOT NULL DEFAULT '',
  `password` varchar(65) NOT NULL DEFAULT '',
  `passreset` varchar(41) NOT NULL COMMENT 'Set if user requests pw reset',
  `phone` varchar(65) NOT NULL DEFAULT '',
  `usecc` tinyint(1) NOT NULL COMMENT 'set if member wants to auto-use cc info',
  `sdate` date NOT NULL COMMENT 'Member Start Date',
  `mdata` mediumblob NOT NULL,
  `information` varchar(200) DEFAULT NULL,
  `customerProfileId` varchar(10) NOT NULL COMMENT 'Authorize.Net Customer Profile ID',
  `saveInfo` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `tbl_members`
--

INSERT INTO `tbl_members` (`id`, `organization_id`, `name`, `status`, `email`, `password`, `passreset`, `phone`, `usecc`, `sdate`, `mdata`, `information`, `customerProfileId`, `saveInfo`) VALUES
(6, 1, 'Mark Hoffman', 1, 'horse56636@hotmail.com', '6c749f5a13df6c42f09604cfd6e5a147', '', '(218) 821-3261', 0, '2015-03-10', '', NULL, '', 0),
(5, 1, 'Don Lewis', 1, 'don.lewis@go2020.com', '9ac7b87fd67628fce3531ad122d42a03', '', '(612) 802-4189', 0, '2015-03-10', '', NULL, '', 0),
(8, 1, 'Paula Garnett', 0, 'pgarnett@altru.org', '6855f1c1b763499a173133989551962b', '', '(701) 780-5544', 0, '2015-03-20', '', NULL, '', 0),
(9, 1, 'Hollys Omlid', 1, 'haomlid@bremer.com', '6634b82d956db42b2adab36b298485c7', '', '(701) 739-3342', 0, '2015-04-07', '', NULL, '32113655', 0),
(10, 1, 'Vadim', 1, 'inna.klepko@yandex.ua', '2962f1ce0eeed72eaa4929e45f86b591', '', '(380) 932-7080', 0, '2015-04-25', '', NULL, '32989509', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_order`
--

CREATE TABLE IF NOT EXISTS `tbl_order` (
  `od_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `od_org` varchar(10) NOT NULL COMMENT 'organization name',
  `od_org_id` int(11) NOT NULL,
  `mem_id` int(6) unsigned NOT NULL COMMENT 'tbl_members.id',
  `od_date` datetime DEFAULT NULL,
  `od_last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `od_status` enum('New','Paid','Shipped','Completed','Cancelled','Error') NOT NULL DEFAULT 'New',
  `od_memo` varchar(255) NOT NULL DEFAULT '',
  `od_delivery_date` date NOT NULL COMMENT 'first delivery date for this order',
  `od_five_day` tinyint(1) NOT NULL COMMENT '0=1 day, 1=5 days',
  `od_coupon` tinyint(4) NOT NULL COMMENT '0=no, 1=yes',
  `an_transaction_id` int(11) DEFAULT NULL,
  `payment_profile_id` int(11) NOT NULL,
  `shipping_address_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`od_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1375 ;

--
-- Дамп данных таблицы `tbl_order`
--

INSERT INTO `tbl_order` (`od_id`, `od_org`, `od_org_id`, `mem_id`, `od_date`, `od_last_update`, `od_status`, `od_memo`, `od_delivery_date`, `od_five_day`, `od_coupon`, `an_transaction_id`, `payment_profile_id`, `shipping_address_id`, `token`, `amount`) VALUES
(1374, '', 1, 6, '2015-07-28 16:56:09', '2015-07-28 18:59:46', 'Paid', '', '2015-07-29', 0, 0, NULL, 0, 0, 'EC-09T31931J7226672X', '7.50'),
(1373, '', 1, 6, '2015-07-28 16:41:04', '2015-07-28 18:41:04', 'New', '', '2015-07-29', 0, 0, NULL, 0, 0, 'EC-28J99207NU731273G', '7.50'),
(1372, '', 1, 5, '2015-07-28 15:21:22', '2015-07-28 17:22:11', 'Paid', '', '2015-07-29', 0, 0, NULL, 0, 0, 'EC-6PU959929K7353310', '7.50');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_order_item`
--

CREATE TABLE IF NOT EXISTS `tbl_order_item` (
  `od_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pd_id` int(10) unsigned NOT NULL DEFAULT '0',
  `tea_id` int(10) NOT NULL,
  `od_qty` int(10) unsigned NOT NULL DEFAULT '0',
  `od_item_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`od_item_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=396 ;

--
-- Дамп данных таблицы `tbl_order_item`
--

INSERT INTO `tbl_order_item` (`od_id`, `pd_id`, `tea_id`, `od_qty`, `od_item_id`) VALUES
(1374, 53, 204, 1, 395),
(1373, 53, 204, 1, 394),
(1372, 50, 202, 1, 393);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_organization`
--

CREATE TABLE IF NOT EXISTS `tbl_organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `directory` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_organization`
--

INSERT INTO `tbl_organization` (`id`, `name`, `directory`) VALUES
(1, 'ING', 'ING'),
(2, 'ALTRU', 'ALTRU');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_pendmembers`
--

CREATE TABLE IF NOT EXISTS `tbl_pendmembers` (
  `confirm_code` varchar(65) NOT NULL DEFAULT '',
  `name` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(65) NOT NULL DEFAULT '',
  `password` varchar(41) NOT NULL DEFAULT '',
  `phone` varchar(65) NOT NULL DEFAULT '',
  `customerProfileId` varchar(10) NOT NULL,
  `organization_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `tbl_pendmembers`
--

INSERT INTO `tbl_pendmembers` (`confirm_code`, `name`, `email`, `password`, `phone`, `customerProfileId`, `organization_id`) VALUES
('290a43a58959a2d2afbf89907e0c2b8e', 'Vadim', 'inna.klepko@yandex.ua', '2962f1ce0eeed72eaa4929e45f86b591', '(380) 932-7080', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_product`
--

CREATE TABLE IF NOT EXISTS `tbl_product` (
  `pd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cat_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pd_name` varchar(100) NOT NULL DEFAULT '',
  `pd_description` text NOT NULL,
  `pd_price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `pd_qty` smallint(5) unsigned NOT NULL DEFAULT '0',
  `pd_image` varchar(200) DEFAULT NULL,
  `pd_thumbnail` varchar(200) DEFAULT NULL,
  `pd_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pd_last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `authorize_button` text NOT NULL,
  PRIMARY KEY (`pd_id`),
  KEY `cat_id` (`cat_id`),
  KEY `pd_name` (`pd_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Дамп данных таблицы `tbl_product`
--

INSERT INTO `tbl_product` (`pd_id`, `cat_id`, `pd_name`, `pd_description`, `pd_price`, `pd_qty`, `pd_image`, `pd_thumbnail`, `pd_date`, `pd_last_update`, `authorize_button`) VALUES
(23, 22, 'French Vanilla', 'French vanilla', '7.50', 50, '', '', '2011-11-13 12:27:51', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="66a6392e-0677-4f54-a814-7098ca410c76" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(24, 22, 'Butter Pecan', 'Butter Pecan', '7.50', 50, '', '', '2011-11-13 12:28:24', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="ccf9d7fe-f356-4bf7-9689-30fb87a4e2ca" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(25, 22, 'Banana Nut', 'Banana Nut', '7.50', 50, '', '', '2011-11-13 12:28:51', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="07747d29-6200-4f15-becb-3731f796fb54" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(26, 22, 'Vanilla Cappuccino', 'Vanilla Cappuccino', '7.50', 50, '', '', '2011-11-13 12:29:20', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="8fdd80ff-9bd6-4b71-be29-40017c5baf75" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(27, 22, 'The Elvis (peanut butter and banana)', 'The "Elvis" (peanut butter and banana)', '7.50', 50, '', '', '2011-11-13 12:29:55', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="48b4d944-ad83-42cc-b3b4-cad08268c8b6" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(28, 22, 'Apple Pie', 'Apple Pie', '7.50', 50, '', '', '2011-11-13 12:30:17', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="7a9df83b-24e4-46c7-80fc-8e59c69e6e21" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(29, 22, 'French Toast', 'French Toast', '7.50', 50, '', '', '2011-11-13 12:30:39', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="2a766392-5c42-4828-9a22-a22b3a8be54b" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(30, 22, 'Cake Batter', 'Cake Batter', '7.50', 50, '', '', '2011-11-13 12:31:01', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="79d9ea24-015c-4a5f-a79b-744b39a47d2f" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(31, 22, 'Strawberry Banana', 'Strawberry Banana', '7.50', 50, '', '', '2011-11-13 12:31:38', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="dbd3352f-6133-4abd-a5da-6878affc4180" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(32, 22, 'Blueberry Cheesecake', 'Blueberry Cheesecake', '7.50', 50, '', '', '2011-11-13 12:32:08', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="035519c9-79d5-4739-97e9-b5d54c91334e" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(33, 22, 'Strawberry Cheesecake', 'Strawberry Cheesecake', '7.50', 50, '', '', '2011-11-13 12:32:36', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="2c75fd14-4683-4416-8018-8cfbf0989a08" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(34, 22, 'Lemon Poppy Seed Cake', 'Lemon Poppy Seed Cake', '7.50', 50, '', '', '2011-11-13 12:33:02', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="e170c636-7aab-42f2-afb1-fc6b808556d8" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(35, 22, 'ButterScotch', 'ButterScotch', '7.50', 50, '', '', '2011-11-13 12:36:16', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="3069c423-0581-4264-8816-01fb97e7d010" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(36, 22, 'Banana Cream Pie', 'Banana Cream Pie', '7.50', 50, '', '', '2011-11-13 12:37:17', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="95871543-ed3b-4e13-b086-150f812d37f3" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(37, 22, 'Vanilla Caramel Cappuccino', 'Vanilla Caramel Cappuccino', '7.50', 50, '', '', '2011-11-13 12:38:12', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="d5ce4ab5-cf97-49d7-9fd4-814b6bbbe7e3" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(38, 22, 'Blueberry Muffin', 'Blueberry Muffin', '7.50', 50, '', '', '2011-11-13 12:38:41', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="5d6b68b4-6409-4f47-900e-74218c48d409" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(39, 22, 'Vanilla Caramel Cheesecake', 'Vanilla Caramel Cheesecake', '7.50', 50, '', '', '2011-11-13 12:39:19', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="6d9a20f5-8f47-4058-8d12-214ac2ba1059" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(40, 22, 'White Chocolate', 'White Chocolate', '7.50', 50, '', '', '2011-11-13 12:40:00', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="15c89243-5c6a-4a56-96bc-9da29b5972d1" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(41, 22, 'Vanilla Mint', 'Vanilla Mint', '7.50', 50, '', '', '2011-11-13 12:40:28', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="904a784d-ce01-4d7b-8b0a-fe83bbcfd0d4" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(42, 22, 'Cinnamon Roll', 'Cinnamon Roll', '7.50', 50, '', '', '2011-11-13 12:41:02', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="11c58c34-a18f-480a-b802-22c18be5d2a3" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(43, 22, 'Strawberry Lemonade', 'Strawberry Lemonade', '7.50', 50, '', '', '2011-11-13 12:41:35', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="6fdb215e-6a26-4305-8f0d-c4afcc81a526" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(44, 22, 'Coconut Cream Pie', 'Coconut Cream Pie', '7.50', 50, '', '', '2011-11-13 12:42:03', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="6abe4b7f-56b4-48af-a1b9-a7f1ffa7e6db" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(46, 22, 'Vanilla Peanut Butter', 'Vanilla Peanut Butter', '7.50', 50, '', '', '2011-11-13 12:43:02', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="4cc94e14-45d7-4f40-9679-d48abdfda28b" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(47, 22, 'Peanut Butter & Jelly', 'Peanut Butter & Jelly', '7.50', 50, '', '', '2011-11-13 12:43:39', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="8e208797-032c-4424-beab-306c5c61dfe3" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(48, 18, 'Just Plain Chocolate', 'Just Plain Chocolate', '7.50', 50, '', '', '2011-11-13 12:44:20', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="260e879c-e67b-436e-b6b0-a0f428e8a6ea" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(49, 18, 'Chocolate Banana', 'Chocolate Banana', '7.50', 50, '', '', '2011-11-13 12:45:36', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="b35db906-012d-4e9f-b039-9b76cb3010b0" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(50, 18, 'Chocolate Raspberry', 'Chocolate Raspberry', '7.50', 50, '', '', '2011-11-13 12:46:17', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="48e5643a-f52b-47bf-9168-b68b34f7c4d8" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(51, 18, 'Chocolate Strawberry', 'Chocolate Strawberry', '7.50', 50, '', '', '2011-11-13 12:46:54', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="2852cb67-4951-4ea8-8a85-ed65c46153cd" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(52, 18, 'Chocolate Coconut', 'Chocolate Coconut', '7.50', 50, '', '', '2011-11-13 12:47:22', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="4b723492-04be-4d9c-9343-a91eb251f7cd" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(53, 18, 'Chocolate Caramel Cappuccino', 'Chocolate Caramel Cappuccino', '7.50', 50, '', '', '2011-11-13 12:48:02', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="53a8f998-0020-47a6-afbf-ca437a24b164" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(54, 18, 'Chocolate Caramel Cheesecake', 'Chocolate Caramel Cheesecake', '7.50', 50, '', '', '2011-11-13 12:48:40', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="2fea4d5a-5cfe-4af1-a384-adf09b314da5" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(56, 18, 'Chocolate Mint', 'Chocolate Mint', '7.50', 50, '', '', '2011-11-13 12:49:53', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="f8a2920e-1787-4e96-8abb-482cb4be12d4" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(57, 18, 'Brownie Batter', 'Brownie Batter', '7.50', 50, '', '', '2011-11-13 12:50:21', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="25c91cbf-6958-4fd7-a466-238d05f2a119" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(58, 18, 'Scotcharoo', 'Scotcharoo', '7.50', 50, '', '', '2011-11-13 12:50:48', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="ad52d722-2821-4f4a-ab7e-36fa352785c8" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(59, 23, 'Chocolate Cookies & Cream', 'Chocolate Cookies & Cream', '7.50', 50, '', '', '2011-11-13 12:52:06', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="1018a21b-ce79-42d2-8d5f-d166026a64a1" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(60, 23, 'Banana Split', 'Banana Split', '7.50', 50, '', '', '2011-11-13 12:52:31', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="2bf3e8a8-58e2-4e5a-a08b-15886f4997a1" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(61, 23, 'Peanut Butter Cup', 'Peanut Butter Cup', '7.50', 50, '', '', '2011-11-13 12:53:00', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="d1433da3-8262-47c2-9c74-c72e328e819a" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(62, 23, 'German Chocolate Cake', 'German Chocolate Cake', '7.50', 50, '', '', '2011-11-13 12:53:29', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="563b37f3-34cd-4803-8c35-3ee498fdb1fa" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(63, 23, 'Oatmeal Cookie', 'Oatmeal Cookie', '7.50', 50, '', '', '2011-11-13 12:53:58', '0000-00-00 00:00:00', '<form name="PrePage" method = "post" action = "https://Simplecheckout.authorize.net/payment/CatalogPayment.aspx"> <input type = "hidden" name = "LinkId" value ="4cdfad0c-bb61-44a5-ac04-e1a03dff6e54" /> <input type = "image" src ="//content.authorize.net/images/buy-now-gold.gif" /> </form>'),
(69, 29, 'test p', 'testsetsetsets', '7.50', 1, '', '', '2014-04-14 11:34:31', '0000-00-00 00:00:00', ''),
(70, 30, 'Test Product', 'A Testing product', '7.50', 100, '', '', '2014-04-15 11:23:48', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reseller`
--

CREATE TABLE IF NOT EXISTS `tbl_reseller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_reseller`
--

INSERT INTO `tbl_reseller` (`id`, `name`, `active`) VALUES
(1, 'Mark Hoffman', 1),
(2, 'Don Lewis', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reseller_organization`
--

CREATE TABLE IF NOT EXISTS `tbl_reseller_organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reseller_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `tbl_reseller_organization`
--

INSERT INTO `tbl_reseller_organization` (`id`, `reseller_id`, `organization_id`) VALUES
(1, 1, 1),
(2, 2, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reseller_tea`
--

CREATE TABLE IF NOT EXISTS `tbl_reseller_tea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reseller_id` int(11) NOT NULL,
  `tea_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `tbl_reseller_tea`
--

INSERT INTO `tbl_reseller_tea` (`id`, `reseller_id`, `tea_id`) VALUES
(1, 1, 201),
(2, 1, 202),
(3, 1, 203),
(4, 1, 204),
(9, 1, 208);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reseller_user`
--

CREATE TABLE IF NOT EXISTS `tbl_reseller_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reseller_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `tbl_reseller_user`
--

INSERT INTO `tbl_reseller_user` (`id`, `reseller_id`, `user_id`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_shop_config`
--

CREATE TABLE IF NOT EXISTS `tbl_shop_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reseller_id` int(11) NOT NULL,
  `sc_name` varchar(50) NOT NULL DEFAULT '',
  `sc_address` varchar(100) NOT NULL DEFAULT '',
  `sc_phone` varchar(30) NOT NULL DEFAULT '',
  `sc_email` varchar(30) NOT NULL DEFAULT '',
  `sc_shipping_cost` decimal(5,2) NOT NULL DEFAULT '0.00',
  `sc_currency` int(10) unsigned NOT NULL DEFAULT '1',
  `sc_order_email` enum('y','n') NOT NULL DEFAULT 'n',
  `sc_server_timezone` varchar(100) NOT NULL DEFAULT 'America/Chicago',
  `sc_order_time_limit` time NOT NULL DEFAULT '09:00:00',
  `sc_order_limit` int(255) NOT NULL,
  `orders_left` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_shop_config`
--

INSERT INTO `tbl_shop_config` (`id`, `reseller_id`, `sc_name`, `sc_address`, `sc_phone`, `sc_email`, `sc_shipping_cost`, `sc_currency`, `sc_order_email`, `sc_server_timezone`, `sc_order_time_limit`, `sc_order_limit`, `orders_left`) VALUES
(1, 1, 'www.HealthyMealDelivered.com', 'PO Box 3 - Bowstring, MN 56631', '(218) 832-3540', 'info@healthymealdelivered.com', '0.00', 4, 'y', 'America/Chicago', '09:00:00', 37, 37);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_teas`
--

CREATE TABLE IF NOT EXISTS `tbl_teas` (
  `tea_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tea_name` varchar(20) NOT NULL,
  PRIMARY KEY (`tea_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=209 ;

--
-- Дамп данных таблицы `tbl_teas`
--

INSERT INTO `tbl_teas` (`tea_id`, `tea_name`) VALUES
(201, 'Arnold Palmer'),
(202, 'Raspberry'),
(203, 'Lemon'),
(204, 'Peach'),
(208, 'Raspberry Lemonade');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(20) NOT NULL DEFAULT '',
  `user_password` varchar(41) NOT NULL DEFAULT '',
  `user_regdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_password`, `user_regdate`, `user_last_login`, `admin`) VALUES
(1, 'admin', '*C780EB2F2B2C68A5953AAD409F4D57ACCE95C843', '2005-02-20 17:35:44', '2015-07-29 13:12:25', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
