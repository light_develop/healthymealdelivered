Event.observe(document, 'dom:loaded', function(){
// Create two variable with the names of the months and days in an array


// Create a newDate() object
    var newDate = new Date();
// Extract the current date from Date object
    newDate.setDate(newDate.getDate());
// Output the day, date, month and year


    /*setInterval(function () {
        // Create a newDate() object and extract the seconds of the current time on the visitor's
        var seconds = new Date().getSeconds();
        // Add a leading zero to seconds value
        document.getElementById('sec').innerHTML = (( seconds < 10 ? "0" : "" ) + seconds);
    }, 1000);*/

    setInterval(function () {
        // Create a newDate() object and extract the minutes of the current time on the visitor's
        var minutes = new Date().getMinutes();
        // Add a leading zero to the minutes value
        document.getElementById('min').innerHTML = (( minutes < 10 ? "0" : "" ) + minutes);
    }, 1000);

    setInterval(function () {
        // Create a newDate() object and extract the hours of the current time on the visitor's
        var hours = new Date().getHours();
        // Add a leading zero to the hours value

        if(hours>12){
            document.getElementById('hours').innerHTML = (( hours < 12 ? "" : "" ) + hours-12);
            document.getElementById('half').innerHTML = 'PM';
            document.getElementById('half').style.fontSize = '1em';
        }
        else{
            document.getElementById('hours').innerHTML = (( hours == '0' ? "0" : '' ) + hours);
            document.getElementById('half').style.fontSize = '1em';
            document.getElementById('half').innerHTML = 'AM';
        }


    }, 1000);

});