<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$adminUserTable = $installer->getTable("admin_user");
$storeTable     = $installer->getTable("core_store");

$installer->startSetup();
$installer->run("ALTER TABLE {$adminUserTable}
                    ADD COLUMN `store_id` SMALLINT(5) UNSIGNED NOT NULL,
                    ADD CONSTRAINT `FK_MULTIADMIN_STORE_ID_ADMIN_USER` FOREIGN KEY (`store_id`) REFERENCES {$storeTable}(`store_id`) ON DELETE CASCADE ON UPDATE CASCADE
");
$installer->endSetup();
