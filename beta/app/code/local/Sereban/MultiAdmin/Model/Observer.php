<?php
class Sereban_MultiAdmin_Model_Observer
{
    /* a big question to use this observer in future or no  */
    public function preDispatch() { //need to use store and websites codes
        if(Mage::app()->getRequest()->getControllerName() == "system_config") {
           // Mage::app()->getRequest()->setParam("store", Mage::helper("sereban_ma")->getAdminStoreId());
          //  Mage::app()->getRequest()->setParam("website", 5); //Mage::helper("sereban_ma")->getAdminStoreId());
        }
    }

    public function preDispatchFrontend() {
        if(!Mage::registry("is_frontend")) {
            Mage::register("is_frontend", 1);
        }
    }
    /* set current store from admin to altru
     * allow to access to admin store id variable from Zend/Db/Select
     */
    public function bindStore() {
return; 
        $user = Mage::getSingleton("admin/session")->getUser();
        if($user) {
            Mage::getSingleton("core/session")->setAdminStoreId($user->getStoreId());
        }

        Mage::app()->setCurrentStore(Mage::helper("sereban_ma")->getAdminStoreId());
    }
}