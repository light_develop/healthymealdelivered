<?php
class Sereban_MultiAdmin_Model_Resource_Entity_Attribute extends Mage_Catalog_Model_Resource_Attribute
{
    /**
     * Rewrite frontend label from 0 to current store_id
     * @param Mage_Core_Model_Abstract $object
     * @return Mage_Eav_Model_Resource_Entity_Attribute|void
     */
    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
        $applyTo = $object->getApplyTo();
        if (is_array($applyTo)) {
            $object->setApplyTo(implode(',', $applyTo));
        }
        /* @var int $storeId - current store id */
        $storeId = Mage::app()->getStore()->getId();

        $frontendLabel = $object->getFrontendLabel();
        if (is_array($frontendLabel)) {
            if (!isset($frontendLabel[$storeId]) || is_null($frontendLabel[$storeId]) || $frontendLabel[$storeId] == '') {
                Mage::throwException(Mage::helper('eav')->__('Frontend label is not defined'));
            }
            $object->setFrontendLabel($frontendLabel[$storeId])
                ->setStoreLabels($frontendLabel);
        }

        /**
         * @todo need use default source model of entity type !!!
         */
        if (!$object->getId()) {
            if ($object->getFrontendInput() == 'select') {
                $object->setSourceModel('eav/entity_attribute_source_table');
            }
        }

        return $this;
    }
}