<?php

class Sereban_MultiAdmin_Helper_Data
{
    /* @var string */
    const ADMIN_JOINS_CONFIG_PATH = "adminjoins";

    /* @var $adminStoreId -> specific multiadmin store id */
    public $adminStoreId = 0;

    /* @return array | boolean
     * @params string $initialTable
     */
    public function getTableDataToJoin($initialTable)
    {
        $_config = $this->_getAdminJoinsConfig();

        return (is_string($initialTable) && isset($_config[$initialTable])) ? (array)$_config[$initialTable] : false;
    }

    /* @return array
     */
    protected function _getAdminJoinsConfig()
    {
        return (array)Mage::getConfig()->getNode(self::ADMIN_JOINS_CONFIG_PATH);
    }

    /** This function redefine magento Constants
     *  it`s the easiest way to change store to any we want
     *  ! IMPORTANT: Function must be called before class with your constant will be called
     * @param Varien_Object|string $object
     * @param string $constantName
     * @param void $changeTo
     * @return boolean|void
     * THROWABLE
     * @throws Exception
     */
    public function redefineConstant($filePath, $constantName, $changeTo)
    {

        $redefined = 0;
        $execString = "";
        if (file_exists($filePath)) {
            if ($res = fopen($filePath, "r")) {
                while ($line = fgets($res)) {
                    if (strpos($line, "?php")) continue;
                    if (preg_match("/const\s+" . $constantName . "/", $line)) {
                        $line = "const " . $constantName . " = " . $changeTo . ";";
                        $redefined = 1;
                    }
                    $execString .= $line;
                }

                if ($execString && $redefined) {
                    eval($execString); //bad function! very bad
                }
            } else {
                throw new Exception("Cannot open file : " . $filePath);
            }
        } else {
            throw new Exception("Cannot find path : {$filePath}");
        }
    }

    public function getAdminStoreId()
    {
	return 1; 
        switch($_SERVER["MAGE_RUN_CODE"]) {
            case "altru":
                return 6;
            case "ing":
                return 7;
            default:
                return 0;
        }









        /*if (isset($_SESSION) || $_SESSION == null) {
            $user = $this->getAdminSessionWithourMage();

            if (is_object($user)) {
                $this->adminStoreId = $user->getStoreId();
            }
        } else {

            $this->adminStoreId = Mage::getSingleton("core/session")->getAdminStoreId();
        }

        return $this->adminStoreId;*/

    }

    public function getAdminSessionWithourMage()
    {
        if ($sess = Mage::registry("admin_user") || Mage::registry("session_already_dispatched")) return $sess;

        session_save_path($_SERVER["DOCUMENT_ROOT"] . "/var/session");
        session_name();
        session_module_name("files");
        session_start();
        $sess = isset($_SESSION["admin"]["user"]) ? $_SESSION["admin"]["user"] : array();
        Mage::register("admin_user", $sess);
        Mage::register("session_already_dispatched", 1);
        return $sess;


        //session_destroy(); // destroy session

    }
}