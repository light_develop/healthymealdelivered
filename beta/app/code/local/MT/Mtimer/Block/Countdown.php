<?php

class MT_Mtimer_Block_Countdown extends Mage_Core_Block_Template
{
    protected $_template = "mtimer/countdown.phtml";
    protected $_holidays = array();
    /* @var $_time DateTime */
    protected $_time;
    /* date should be in format dd.mm.yyyy */

    public function __construct() {
        $this->_prepareHolidays();
    }

    protected function _prepareHolidays()
    {
        $this->_holidays = array();
        $_config = Mage::getStoreConfig('mtsystems/customers/holiday');

        if (empty($_config)) return;

        if ($config = unserialize($_config)) {
            foreach ($config as $holiday) {
                if (empty($holiday)) continue;

                list($day, $month, $year) = explode(".", $holiday["customer_group_id"]);
                $_time = new DateTime();
                $this->_holidays[] = $_time->setDate($year, $month, $day);
            }
        }
        $this->_initCurrentDate();
        $this->_setIncrementToDate();
    }

    protected function _initCurrentDate() {
        $this->_time = new DateTime();
        $_timeZone = Mage::getStoreConfig("mtsystems/customers/timezone");
        $this->_time->setTimezone(
            new DateTimeZone($_timeZone)
        );
    }

    protected function _getCurrentHours()
    {
        return $this->_time->format("H"); //in 24 format
    }
    /* increment date */
    protected function _setIncrementToDate() {
        $hours = $this->_getCurrentHours();
        $_startTime = Mage::getStoreConfig('mtsystems/customers/start_time');
        ($hours > $_startTime) ? $this->_time->add(new DateInterval("P2D")) : $this->addOneDay();
    }

    public function getDeliveryDate() {
        if($this->_checkForWeekend()) {
            $this->addOneDay();
            $this->getDeliveryDate();
        }

        /* @var $holiday DateTime */
        foreach($this->_holidays as $holiday) {
            if($holiday->getTimeStamp() == $this->_time->getTimestamp()) { //if we have holiday
                $this->addOneDay();
                $this->getDeliveryDate(); //recursive
            }
        }

        return $this->_time;
    }

    protected function _checkForWeekend() {
        $weekends = Mage::getStoreConfig("mtsystems/customers/days_of_the_week");
        if(strlen($weekends) > 0) {
            return in_array($this->_time->format('w'), explode(",", $weekends));
        } else {
            return false;
        }
    }

    public function addOneDay() {
        return $this->_time->add(new DateInterval("P1D"));
    }

}