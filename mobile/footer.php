<?php

$pageURL = 'http';
 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";

$server = $pageURL.$_SERVER["SERVER_NAME"]."/dev/omnao/mobile/policy.php";

$main = $pageURL.$_SERVER["SERVER_NAME"]."/dev/omnao/ing/index.php";
?>
<div data-theme="d" data-role="footer" class="footer" id="footer">
    <div class="footerLinks">
        <a data-ajax="false" href="<?php echo $server; ?>#privacy">Privacy Policy</a>
        <a data-ajax="false" href="<?php echo $server; ?>#security">Security Policy</a>
        <a data-ajax="false" href="<?php echo $server; ?>#refunds">Refunds</a>
        <a data-ajax="false" href="<?php echo $server; ?>#delivery">Delivery</a>
    </div>

    <div class="copyright">&copy; <?php echo date('Y'); ?> <?php echo $shopConfig['name']; ?></div>
    &mdash;
    <div class="contact">
        <?php echo $shopConfig['address']; ?><br>
        <strong><?php echo $shopConfig['phone']; ?></strong><br>
        <a href="mailto:<?php echo $shopConfig['email']; ?>"><?php echo $shopConfig['email']; ?></a>
    </div>
    
    <div class="fullSiteLink">
        <a href="#" onclick="document.getElementById('fullForm').submit()">Full Site</a>
    </div>
    
    <form style="visibility: hidden;" id="fullForm" name="fullForm" action="<?php echo $main; ?>" method="post" data-ajax="false">
        <input type="hidden" name="size" id="size" value="full" />
    </form>

</div><!--  end footer -->