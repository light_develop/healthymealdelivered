<?php
require_once '../shared/library/config.php';
?>
<!DOCTYPE html> 
<html> 
	<head> 
	<title>Minot Nutrition Addiction</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

        <?php require 'head.php'; ?>
        
        <style type="text/css">
            h2 {
                margin-top: 0;
                padding-top: 1em;
            }
            
            p {
                margin-bottom: 2em;
            }
        </style>
</head> 

<body> 

    <!-- Home -->
<div data-role="page" id="policy">
    <div data-theme="b" data-role="header">
        <h3>
            &nbsp;
        </h3>
        <a data-role="button" data-rel="back" data-transition="slide" href="index.php"
        data-icon="arrow-l" data-iconpos="left" class="ui-btn-left" data-theme="a">
            Back
        </a>
    </div>
    <div data-role="content">
        <h2 id="privacy">Privacy Policy</h2>
        <p>We respect and are committed to protecting your privacy. We may collect personally identifiable information when you visit our site. We also automatically receive and record information on our server logs from your browser including your IP address, cookie information and the page(s) you visited. We will not sell, rent or distribute your personally identifiable information to anyone EVER </p>
        
        <h2 id="security">Security Policy</h2>
        <p>Your payment and personal information is always safe. Our Secure Sockets Layer (SSL) software is the industry leader and the best software available today for secure commerce transactions. It encrypts all of your personal information, including credit card number, name, and address, so that it cannot be read over the Internet.</p>
        
        <h2 id="refund">Refund Policy</h2>
        <p>We offer a 30 day Money Back Guarantee on every product we sell. Please Contact us thru the website within 30 days of purchase date. All refunds will be provided as a credit to the credit card used at the time of purchase within five (5) business days upon receipt of the returned merchandise. </p>
        
        <h2 id="delivery">Delivery Policy</h2>
        <p>Please be assured that your items will ship out within seven days of purchase. We determine the most efficient shipping carrier for your order. The carriers that may be used are: United Parcel Service (UPS) or FedEx. Sorry but we cannot ship to P.O. Boxes. If you're trying to estimate when a package will be delivered, please note the following: Credit card authorization and verification must be received prior to processing. Federal Express and or UPS deliveries occur Monday through Friday, excluding holidays. All orders will be charged appropriate sales tax for the zip code the product is delivered to. </p>
        
    </div>
    <?php include_once "footer.php"; ?>
</body>
</html>