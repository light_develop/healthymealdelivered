<?php
require_once '../shared/library/config.php';
require_once '../shared/library/category-functions.php';
require_once '../shared/library/product-functions.php';
require_once '../shared/library/cart-functions.php';
require_once '../shared/library/common.php';

$instruct = "";
$logout = (isset($_GET['action']) && $_GET['action'] == '2') ? $_GET['action'] : 0;
if ($logout) {
    unset($_SESSION['mname']);
    unset($_SESSION['mid']);
    
    $instruct = "You have been logged out.";
    printf("<script>location.href='index.php'</script>");
}


if((isset($_POST['email']) && $_POST['email'] != '') && (isset($_POST['password']) && $_POST['password'] != ''))
{
    $uemail = $_POST['email'];
    $upass  = $_POST['password'];
    
    $sql = "SELECT id, name, email, password, status FROM tbl_members WHERE email = '".$uemail."' AND password = '".$upass."'";
	$result = dbQuery($sql);
        
        
	if (!dbNumRows($result)) {
		$instruct = "Incorrect Email Address and/or Password. Please try again.";
	} else {
		$row = dbFetchAssoc($result);
                if($row['status'] == 1)
                {
                    $_SESSION['mname'] = $row['name'];
                    $_SESSION['mid'] = $row['id'];
                    
                    printf("<script>location.href='index.php'</script>");
                }
                else
                {
                    $instruct = "You are unable to log in because your account has been suspended. We apologize for any inconvenience.";
                }
	}
}


?>
<!DOCTYPE html> 
<html> 
	<head> 
	<title>Minot Nutrition Addiction</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

	<?php require 'head.php'; ?>
</head> 

<body> 
<div data-role="page" id="loginPage">
    <div data-theme="b" data-role="header">
        <h3>
            &nbsp;
        </h3>
        <a data-role="button" data-direction="reverse" data-transition="slide" href="index.php"
        data-icon="arrow-l" data-iconpos="left" class="ui-btn-left" data-theme="a">
            Main
        </a>
    </div>
    <div data-role="content">
        <h3><?php echo $instruct; ?></h3>
        <form action="login.php" method="post" data-ajax="false">
            <div data-role="fieldcontain">
                <fieldset data-role="controlgroup">
                    <label for="email">
                        Email
                    </label>
                    <input name="email" id="email" placeholder="" value="" type="email">
                    <br />
                    <label for="password">
                        Password
                    </label>
                    <input name="password" id="password" placeholder="" value="" type="password">
                </fieldset>
            </div>
            <input data-theme="b" value="Log In" type="submit">
        </form>
    </div>
    <?php include_once "footer.php"; ?>
</div>
    
</body>
</html>