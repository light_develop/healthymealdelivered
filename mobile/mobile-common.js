function submitShake()
{
    if ((!($("#shakes option:selected").val() > 0)) || (!($("#teas option:selected").val() > 0))) {
        if($("#error").html().length > 0){
            $("#error").css("color", "red");
        }
        $("#error").html("Please select both a Shake and a Tea");
        return false;
    }
    else {
        return true;
    }
}

function addSTToCart() {
    var pd_id = document.getElementById("shakes").value;
    var tea_id = document.getElementById("teas").value;
    //var cart_url = "../shared/cart.php?action=add&p=" + pd_id + "&t=" + tea_id;
    //window.location.href = cart_url;
    alert('its working.');
    $.ajax({
        url: "../shared/cart.php",
        data: {"action":"add", "p":pd_id, "t":tea_id},
        success: function(){alert('it worked.');},
        error: function(jqXHR, textStatus, errorThrown){alert(errorThrown);}
    });
}

function validate() {
    
    if(!$('input[name=payment]:checked').length > 0){        
        alert('Please select a form of payment');
        return false;
    }
    
    if(!$('input[name=shipping]:checked').length > 0){
        alert('Please select a shipping address');
        return false;
    }
    
    return true;
}

function fiveDayFlip() {
    //alert('hi');
    /*if($("#repeat").hasClass('repeatSmall')) {
        $("#repeat").removeClass("repeatSmall").addClass("repeat");
        $("#repeatPrice").hide(0);
        $("#wRepeat").hide(0);
        $("#wORepeat").show(0);
    } else {
        $("#repeat").removeClass("repeat").addClass("repeatSmall");
        $("#repeatPrice").show(0);
        $("#wRepeat").show(0);
        $("#wORepeat").hide(0);
    }*/
}

$(document).bind('pageinit', function(){
    
    $("#repeat").change(function(){
        //$("#repeat.repeatSmall").removeClass("repeatSmall").addClass("repeat");
        //$("#repeat.repeat").removeClass("repeat").addClass("repeatSmall");
        if($(this).hasClass('repeatSmall')) {
            $(this).removeClass("repeatSmall").addClass("repeat");
            $("#repeatPrice").removeClass("show").addClass("hide");
            $("#wRepeat").removeClass("show").addClass("hide");
            $("#jimmy").removeClass("hide").addClass("show");
        } else {
            $(this).removeClass("repeat").addClass("repeatSmall");
            $("#repeatPrice").removeClass("hide").addClass("show");
            $("#wRepeat").removeClass("hide").addClass("show");
            $("#jimmy").removeClass("show").addClass("hide");
        }

    });
    
    
});