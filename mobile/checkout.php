<?php
require_once '../shared/library/config.php';
require_once '../shared/library/cart-functions.php';
require_once '../shared/library/checkout-functions.php';
include_once ("../shared/authorize/php_cim/XML/vars.php");
include_once ("../shared/authorize/php_cim/XML/util.php");

if (isCartEmpty()) 
{
	// the shopping cart is still empty
	// so checkout is not allowed
	header('Location: cart.php');
} 
else if (isset($_GET['step']) && (int)$_GET['step'] > 0 && (int)$_GET['step'] <= 3)
{
    
    $step = (int)$_GET['step'];

    $includeFile = '';
    $prevLoc = '';
    $prevTitle = '';
    if ($step == 1) {
        $includeFile = 'payment.php';
        $prevLoc = "cart.php";
        $prevTitle = "Cart";
    } else if ($step == 2) {
        $includeFile = 'confirm.php';
        $prevLoc = "checkout.php?step=1";
        $prevTitle = "Payment";
    } else if ($step == 3) {
        
        $custProfID = $_SESSION['custProfID'];
        $memberID = $_SESSION['mid'];
        $customerPaymentProfileId = $_SESSION['customerPaymentProfileId'];
        $customerShippingAddressId = $_SESSION['customerShippingAddressId'];
        
        // adding Ids to POST for saveorder() //
        $_POST["customerPaymentProfileId"] = $customerPaymentProfileId;
        $_POST["customerShippingAddressId"] = $customerShippingAddressId;
        
        $ccv = $_POST['ccv'];
        $amount = $_SESSION["subTotal"];
        $saveinfo = 0;
        
        if(isset($_SESSION["saveinfo"]) && $_SESSION["saveinfo"] == 1)
        {
            $saveinfo = $_SESSION["saveinfo"];
            $sql = "UPDATE tbl_members SET saveInfo = 1 WHERE id = $memberID";
            $result = dbQuery($sql);
        }
        else
        {
            $sql = "UPDATE tbl_members SET saveInfo = 0";
            $result = dbQuery($sql);
        }

        $cartContent = getCartContent();
        $numItem     = count($cartContent);
        extract($cartContent);

        // save this order (and order for each day in 5day order)
        $orderId  = saveOrder($customerPaymentProfileId, $customerShippingAddressId, "FiveDayOrder");

        $request = '<?xml version="1.0" encoding="utf-8"?>
        <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                '.MerchantAuthenticationBlock().'
                <transaction>
                        <profileTransAuthOnly>
                                <amount>'.$amount.'</amount>';

                foreach($cartContent as $item)
                {
                    $name = substr(htmlspecialchars ($item["pd_name"].' '.$item["tea_name"]),0,30);
                    $ct_id = $item['ct_id'];
                    $ct_qty = $item['ct_qty'];
                    $pd_price = $item['pd_price'];

                    $request .= "<lineItems>
                                        <itemId>$ct_id</itemId>
                                        <name>$name</name>
                                        <quantity>$ct_qty</quantity>
                                        <unitPrice>$pd_price</unitPrice>
                                </lineItems>";
                }

                    $request .= "<customerProfileId>$custProfID</customerProfileId>
                                <customerPaymentProfileId>$customerPaymentProfileId</customerPaymentProfileId>
                                <customerShippingAddressId>$customerShippingAddressId</customerShippingAddressId>
                                <order>
                                        <invoiceNumber>$orderId</invoiceNumber>
                                </order>
                                <recurringBilling>false</recurringBilling>";

                    if(isset($ccv) && strlen($ccv) > 0)
                    {
                            $request .= "<cardCode>$ccv</cardCode>";
                    }


                    $request .= "</profileTransAuthOnly>
                </transaction>
        </createCustomerProfileTransactionRequest>";


        //submit transaction for this order 
        $response = send_xml_request($request);

        $parsedresponse = parse_api_response($response);
        if ("Ok" == $parsedresponse->messages->resultCode) {
            $creditResult = explode(',',$parsedresponse->directResponse);

            //summary of result parts can be found at http://www.authorize.net/support/AIM_guide.pdf on page 38

            if($creditResult[0] > 1)
            {
                //there was an issue processing the card
                
                deleteOrder($orderId);
                
                $includeFile = 'payment.php';
                $prevLoc = "cart.php";
                $prevTitle = "Cart";
                
                $cardError = 1;
            }
            else
            {
                updateOrderId($orderId, $creditResult[6]);
                
                $includeFile = "complete.php";
                $prevLoc = "";
                $prevTitle = "";
                
                // then remove the ordered items from cart
                for ($i = 0; $i < $numItem; $i++) {
                        $sql = "DELETE FROM tbl_cart
                                WHERE ct_id = {$cartContent[$i]['ct_id']}";
                        $result = dbQuery($sql);					
                }
            }
        }
    }
}
else
{
	// missing or invalid step number, just redirect
	header('Location: index.php');
}
?>
<!DOCTYPE html> 
<html> 
	<head> 
	<title>Minot Nutrition Addiction</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

	<?php require 'head.php'; ?>
</head>

<body> 
<div data-role="page" id="checkoutPage">
    <div data-theme="b" data-role="header">
        <h3>
            &nbsp;
        </h3>
        
        <?php
        if(strlen($prevLoc) > 0)
        {
            echo "<a data-role=\"button\" data-direction=\"reverse\" data-transition=\"slide\" href=\"$prevLoc\"
                data-icon=\"arrow-l\" data-iconpos=\"left\" class=\"ui-btn-left\">
                    $prevTitle
                </a>";
        }
        ?>
    </div>
    <div data-role="content">
        
        <?php require_once "$includeFile"; ?>
        
    </div>
    
    <?php include_once "footer.php"; ?>
</div>
    
</body>
</html>