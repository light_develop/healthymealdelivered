<?php

require_once '../shared/library/config.php';
require_once '../shared/library/category-functions.php';
require_once '../shared/library/cart-functions.php';

$message = '';

if((isset($_GET['shakes']) && $_GET['shakes'] != 'Shake') && (isset($_GET['teas']) && $_GET['teas'] != 'Tea'))
{
    $productId = (int)$_GET['shakes'];
    $teaId = (int)$_GET['teas'];
    
    
    // does the product exist ?
    $sql = "SELECT pd_id, pd_qty
            FROM tbl_product
                    WHERE pd_id = $productId";
    $result = dbQuery($sql);

    if (dbNumRows($result) != 1) {
            // the product doesn't exist
            $message = "This product does not exist. ".$_GET['teas'];
    }
    else
    {

        // current session id
        $sid = session_id();

        // check if the Shake/Tea combo is already
        // in cart table for this session
        $sql = "SELECT pd_id
                FROM tbl_cart
                        WHERE pd_id = $productId AND tea_id = $teaId AND ct_session_id = '$sid'";
        $result = dbQuery($sql);

        if (dbNumRows($result) == 0) {
                // put the Shake in cart table
                $sql = "INSERT INTO tbl_cart (pd_id, tea_id, ct_qty, ct_session_id, ct_date)
                                VALUES ($productId, $teaId, 1, '$sid', NOW())";
                $result = dbQuery($sql);
        } else {
                // update Shake quantity in cart table
                $sql = "UPDATE tbl_cart 
                        SET ct_qty = ct_qty + 1
                                WHERE ct_session_id = '$sid' AND pd_id = $productId AND tea_id = $teaId";		

                $result = dbQuery($sql);		
        }

        deleteAbandonedCart();

        $message = "Added to cart.";
    }
}
else if(isset($_GET['shakes']) && isset($_GET['teas']))
{
    $message = "Please select both a Shake and a Tea";
}

$catId = (isset($_GET['cat']) && $_GET['cat'] != '') ? $_GET['cat'] : 0;

$sql = "SELECT p.cat_id, c.cat_id, cat_description, pd_id, pd_description
		FROM tbl_product p, tbl_category c
		WHERE p.cat_id = $catId AND c.cat_id = $catId 
		ORDER BY pd_description";
$result     = dbQuery($sql);
$row = array();
$slist = "<option>Shake</option>";
while ($row = dbFetchAssoc($result)) {
	$pd_id = $row['pd_id'];
	$pd_description = $row['pd_description'];
	$shake = $row['cat_description'];
	$slist .= "<option value=\"$pd_id\">$pd_description</option>";
}
$sql = "SELECT tea_id, tea_name
		FROM tbl_teas
		ORDER BY tea_name";
$result     = dbQuery($sql);
$row = array();
$tlist = "<option>Tea</option>";
while ($row = dbFetchAssoc($result)) {
	$tea_id = $row['tea_id'];
	$tea_name = $row['tea_name'];
	$tlist .= "<option value=\"$tea_id\">$tea_name</option>";
}

$cartContent = getCartContent();
$numItem = count($cartContent);
?>
<!DOCTYPE html> 
<html> 
    <head> 
    <title>Minot Nutrition Addiction</title> 

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php require 'head.php'; ?>
</head> 

<body> 

    <!-- Home -->
<div data-role="page" id="category">
    <div data-theme="b" data-role="header">
        <h3>
            &nbsp;
        </h3>
        <a data-role="button" data-direction="reverse" data-transition="slide" href="index.php"
        data-icon="arrow-l" data-iconpos="left" class="ui-btn-left" data-theme="a">
            Main
        </a>
        <a data-role="button" data-transition="slide" href="cart.php" data-icon="arrow-r"
        data-iconpos="right" class="ui-btn-right" data-theme="a">
            Cart (<?php echo $numItem; ?>)
        </a>
    </div>
    <div data-role="content" style="padding: 15px">
        <h1>
            <?php echo $shake; ?>
        </h1>
        <span id="error" name="error"><?php echo $message; ?></span>
        <form id="addShakeForm" name="addShakeForm" action="category.php" method="get" data-ajax="false">
            <div data-role="fieldcontain">
                <label for="shakes">
                    Select Shake:
                </label>
                <select data-native-menu="false" name="shakes" id="shakes">
                    <?php echo $slist; ?>
                </select>
                <br />
                <label for="teas">
                    Select Tea:
                </label>
                <select data-native-menu="false" name="teas" id="teas">
                    <?php echo $tlist; ?>
                </select>
            </div>
            <input data-theme="b" value="Add to Cart" type="submit">
            <input type="hidden" id="cat" name="cat" value="<?php echo $catId; ?>"/>
        </form>
    </div>
    <?php include_once "footer.php"; ?>
</div>
</body>
</html>