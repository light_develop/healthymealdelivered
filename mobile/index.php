<?php
require_once '../shared/library/config.php';
require_once '../shared/library/category-functions.php';
require_once '../shared/library/cart-functions.php';

$instruct='';
$logout = (isset($_GET['action']) && $_GET['action'] == '2') ? $_GET['action'] : 0;
if ($logout && isset($_SESSION['mname'])) {
    unset($_SESSION['mname']);
    unset($_SESSION['mid']);
    
    $instruct = "You have been logged out.";
}

$categoryList    = getCategoryList();
$numCategory     = count($categoryList);

$cartContent = getCartContent();
$numItem = count($cartContent);


?>
<!DOCTYPE html> 
<html> 
	<head> 
	<title>Minot Nutrition Addiction</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

        <?php require 'head.php'; ?>
</head> 

<body> 

    <!-- Home -->
<div data-role="page" id="page1">
    <div data-theme="b" data-role="header">
        <h3>
            &nbsp;
        </h3>
        <a data-role="button" data-transition="slide" href="cart.php" data-icon="arrow-r"
        data-iconpos="right" class="ui-btn-right" data-theme="a" >
            Cart (<?php echo $numItem; ?>)
        </a>
    </div>
    <div data-role="content" style="padding: 15px">
        <h1>
            Minot Nutrition Addiction Online
        </h1>
        <span><?php echo $instruct; ?></span>
        
        <?php
    
        $link = '';
        if (isset($_SESSION['mname']) && $_SESSION['mname'] != "") {
            $who = $_SESSION['mname'];
            $label = "Logout";
            $mode = 2;
            $action = "index.php";
            $ajax = "data-ajax=\"false\"";
            
            $link = "<div class=\"loggedInAs\" style=\"width: 100%;text-align: center;\">Logged in as ".$_SESSION['mname']."</div>
                    <form action=\"$action\" method=\"get\" data-transition=\"slide\" $ajax>
                        <input data-theme=\"b\" value=\"$label\" data-mini=\"true\" type=\"submit\"/>
                        <input type=\"hidden\" name=\"action\" id=\"action\" value=\"$mode\"/>
                    </form>";
            
        } else {
            $who = "";
            $label = "Login";
            $mode = 1;
            $action = "login.php";
            $ajax = "";
            
            echo "<form action=\"$action\" method=\"get\" data-transition=\"slide\" $ajax>
                        <input data-theme=\"b\" value=\"$label\" type=\"submit\"/>
                        <input type=\"hidden\" name=\"action\" id=\"action\" value=\"$mode\"/>
                    </form>";
            
        }
        ?>        
        
        <div style="margin-top: 1em;">
            Select a category below to order a shake.
        </div>
        
        <ul data-role="listview" data-divider-theme="b" data-inset="true">
            <li data-role="list-divider" role="heading">
                Categories
            </li>
            <?php
                if ($numCategory > 0) {
                    $i = 0;
                    for ($i; $i < $numCategory; $i++) {
                        // we have $url, $image, $name, $price, $cat
                        extract ($categoryList[$i]);
                        
                    echo "<li data-theme=\"c\">
                                <a href=\"category.php?cat=$cat\" data-transition=\"slide\">
                                    $name
                                </a>
                            </li>";
                    }
                }else {
                ?>
                        No categories yet
                <?php	
                }
            ?>
            
        </ul>
        
        <?php echo $link; ?>
    </div>
    <?php include_once "footer.php"; ?>
</body>
</html>