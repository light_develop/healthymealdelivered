<?php
require_once '../shared/library/config.php';
require_once '../shared/library/category-functions.php';
require_once '../shared/library/product-functions.php';
require_once '../shared/library/cart-functions.php';

$_SESSION['shop_return_url'] = $_SERVER['REQUEST_URI'];

$catId  = (isset($_GET['c']) && $_GET['c'] != '1') ? $_GET['c'] : 0;
$pdId   = (isset($_GET['p']) && $_GET['p'] != '') ? $_GET['p'] : 0;

require_once '../shared/include/header.php';
?><style type="text/css">
<!--
body {
	background-image: url(../shared/images/online-shop-background.jpg);
	background-repeat: repeat-x;
}
-->
</style>
<link href="../shoppingcartCSS.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
 <tr> 
  <td colspan="3" bgcolor="#FFFFFF">
  <?php require_once '../shared/include/top.php'; ?>
  </td>
 </tr>
 <tr valign="top"> 
  <td width="150" height="400" bgcolor="#FFFFFF" id="leftnav"><?php
require_once '../shared/include/leftNav.php';
?></td>
  <td bgcolor="#FFFFFF" class="itemdescription2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20">&nbsp;</td>
      <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="13" height="13" valign="top"><img src="../shared/images/top-round-left.gif" alt="" width="13" height="13" /></td>
              <td height="13"><img src="../shared/images/fill-box.gif" alt="" width="100%" height="13" /></td>
              <td width="13" height="13" valign="top"><img src="../shared/images/top-round-right.gif" alt="" width="13" height="13" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top" bgcolor="#EADCB9"><p>
            <?php
if ($pdId) {
	require_once '../shared/include/productDetail.php';
} else if ($catId) {
	require_once '../shared/include/productList.php';
} else {
	require_once '../shared/include/categoryList.php';
}
?>
          </p></td>
        </tr>
        <tr>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="13" height="14" valign="top"><img src="../shared/images/bottom-round-left.gif" width="13" height="13" /></td>
              <td height="13" valign="top"><img src="../shared/images/fill-box.gif" width="100%" height="14" /></td>
              <td width="13" height="14" valign="top"><img src="../shared/images/bottom-round-right.gif" width="13" height="13" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top">&nbsp;</td>
        </tr>
      </table></td>
      <td width="20">&nbsp;</td>
    </tr>
  </table></td>
  <td width="130" align="center" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
      <td width="15">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php require_once '../shared/include/miniCart.php'; ?></td>
      <td>&nbsp;</td>
    </tr>
  </table></td>
 </tr>
 <tr valign="top">
   <td height="72" colspan="3" id="leftnav2"><img src="../shared/cart/images/online-shop-footer.png" width="780" height="72" /></td>
  </tr>
</table>
<?php
require_once '../shared/include/footer.php';
?>
