<?php
require_once './../utils/functions.php';
require_once './../shared/library/config.php';
	//The token of the cancelled payment typically used to cancel the payment within your application
	//Знак отменена оплата, как правило, используется для отмены оплаты в вашем приложении
	$token = noSQLInjections(trim($_GET['token']));
	if (!$token){
		header("Location: ../index.php");
	}
	$dataEdit = date ("Y-m-d H:i:s");
	$sqlError = "UPDATE `tbl_order` SET `od_status`='Cancelled',`od_last_update`='{$dataEdit}' WHERE `token`='{$token}'";
	$resultError = dbQuery($sqlError);
	if ($resultError){
        if (!$_SESSION) session_start();
        if (isset($_SESSION['last_cart_url'])) {
            $url = $_SESSION['last_cart_url'];
        } else {
            $url = '/index.php';
        }
        header("Location: ".$url);
		/*echo "<p>Payment is cancelled</p>";
		echo '<a href="'.$url.'">To Cart</a>';*/
	}
/*
* Защита от SQL инекций protection=1 -замена символов 2- екранировать
*/
function noSQLInjections($sql, $protection = 1)
{
	if ($protection == 1) {
		$sql = str_replace(';', "&#59;", $sql);
		$sql = str_replace('"', "&#34;", $sql);
		$sql = str_replace("'", "&#39;", $sql);
		$sql = str_replace("*", "&#42;", $sql);
		$sql = str_replace("´", "&acute;", $sql);
		$sql = str_replace('`', "&#96;", $sql);
	}
	$sql = mysql_real_escape_string($sql);
	return $sql;
}?>