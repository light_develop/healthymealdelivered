<?php
require_once './../shared/library/config.php';
require_once './../shared/library/cart-functions.php';
require_once 'Paypal.php';
require_once './../utils/functions.php';

$payPal = new PayPal($configPayPal);
$cartContents = getCartContent();
//var_dump($cartContents); die();


if (!isset($_SESSION)) session_start();
if (isset($_SESSION['last_cart_url'])) {
    $back_url = $_SESSION['last_cart_url'];
} elseif (isset($_SESSION['shop_return_url'])) {
    $back_url = $_SESSION['shop_return_url'];
} else {
    $back_url = '../index.php';
}


	if (!isset($_COOKIE['userID']) || !intval($_COOKIE['userID'])) {
		header("Location: ".$back_url);
	}
	if (isset($_POST['amount']) && intval($_POST['amount']) &&
		isset($_POST['item_name']) && $_POST['item_name'] != "" && $cartContents) {

		$orderID = false;
		$memberId = intval($_COOKIE['userID']);
		$chkFive = ($_POST['chkFive'] ? 1 : 0);
		$chkdCoupon = ($_POST['chkdCoupon'] ? 1 : 0);
		$deliveryDate = strtotime(getDeliveryDate());
		$deliveryDate = date ("Y-m-d H:i:s", $deliveryDate);
		$paymentId = 0;
		$shippingId = 0;
		$amount = number_format($_POST['amount'], 2, '.', '');
		$sql = "INSERT INTO `tbl_order`
								(`od_date`,
								`od_last_update`,
								`od_org`,
								`od_org_id`,
								`mem_id`,
								`od_five_day`,
								`od_coupon`,
								`od_delivery_date`,
								`payment_profile_id`,
								`shipping_address_id`,
								`amount`)
                            VALUES
                            	(NOW(),
                            	NOW(),
                            	'',
                            	1,
                            	'{$memberId}',
                            	'{$chkFive}',
                            	'{$chkCoupon}',
                            	'{$deliveryDate}',
                            	{$paymentId},
                            	{$shippingId},
                            	'$amount')";
		$result = dbQuery($sql);
		if($result){
			$orderID = intval(dbInsertId());
			if(!intval($orderID)){
				echo "Error insert_id tab tbl_order";
				exit();
			}
			foreach($cartContents as $cartContent){
				$productId = $cartContent['pd_id'];
				$teaId = $cartContent['tea_id'];
				$odQty = $cartContent['ct_qty'];
				$sql = "INSERT INTO `tbl_order_item` (`od_id`,`pd_id`,`tea_id`,`od_qty`) VALUES
							({$orderID},{$productId},{$teaId},{$odQty})";
				$result = dbQuery($sql);
				if(!$result){
					echo "Error INSERT tbl_order_item";
					break;
				}
			}
		}else{
			echo "Error Order";
			exit();
		}

		// set
		$paymentInfo['Order']['theTotal'] = $amount;
		$paymentInfo['Order']['description'] = strip_tags($_POST['item_name']);
		$paymentInfo['Order']['quantity'] = 1;
		$paymentInfo['OrderParams']['landingPage'] = $_POST['landingPage'];

		// call paypal
		$result = $payPal->SetExpressCheckout($paymentInfo);
		//Detect Errors
		if (!$payPal->isCallSucceeded($result)) {
			if ($payPal->apiLive === true) {
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
			} else {
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
			echo $error;
			$dataEdit = date ("Y-m-d H:i:s");
			$sqlError = "UPDATE `tbl_order` SET `od_status`='Error',`od_last_update`='{$dataEdit}' WHERE `od_id` = '{$orderID}'";
			$resultError = dbQuery($sqlError);
			exit();

		} else {
			// send user to paypal
			$token = noSQLInjections(urldecode($result["TOKEN"]));

			$dataEdit = date ("Y-m-d H:i:s");
			$sql = "UPDATE `tbl_order` SET `token` = '{$token}', `od_last_update`='{$dataEdit}' WHERE `od_id` = '{$orderID}'";
			$result = dbQuery($sql);
			if ($result)
				$payPalURL = $payPal->paypalUrl . $token;
			else
				$payPalURL = "/paypal/choice/";
			header("Location: $payPalURL");
		}
	} else
		header("Location: ".$back_url);

/*
* Защита от SQL инекций protection=1 -замена символов 2- екранировать
*/
function noSQLInjections($sql, $protection = 1)
{
	if ($protection == 1) {
		$sql = str_replace(';', "&#59;", $sql);
		$sql = str_replace('"', "&#34;", $sql);
		$sql = str_replace("'", "&#39;", $sql);
		$sql = str_replace("*", "&#42;", $sql);
		$sql = str_replace("´", "&acute;", $sql);
		$sql = str_replace('`', "&#96;", $sql);
	}
	$sql = mysql_real_escape_string($sql);
	return $sql;
}
