<?php
require_once './../shared/library/config.php';
require_once './../shared/library/cart-functions.php';
require_once 'Paypal.php';
require_once './../utils/functions.php';
$payPal = new PayPal($configPayPal);

if (!isset($_COOKIE['userID']) || !intval($_COOKIE['userID'])) {
	header("Location: ../index.php");
}

$token = noSQLInjections(trim($_GET['token']));
$payerId = trim($_GET['PayerID']);
$memberId = intval($_COOKIE['userID']);

$sql = "SELECT * FROM `tbl_order` WHERE `token`='{$token}' AND `od_status`='New' AND `mem_id`={$memberId}";
$result = dbQuery($sql);
$order = dbFetchAssoc($result);

if ($order) {
	$orderID = $order['od_id'];
	$result = $payPal->GetExpressCheckoutDetails($token);

	$result['PAYERID'] = $payerId;
	$result['TOKEN'] = $token;
	$result['ORDERTOTAL'] = $order['amount'];

	//Detect errors
	if (!$payPal->isCallSucceeded($result)) {
		if ($payPal->apiLive === true) {
			//Live mode basic error message
			$error = 'We were unable to process your request. Please try again later';
		} else {
			//Sandbox output the actual error message to dive in.
			$error = $result['L_LONGMESSAGE0'];
		}
		$dataEdit = date ("Y-m-d H:i:s");
		$sqlError = "UPDATE `tbl_order` SET `od_status`='Error',`od_last_update`='{$dataEdit}' WHERE `od_id` = '{$orderID}'";
		$resultError = dbQuery($sqlError);
		echo $error;
		exit();
	} else {

		$paymentResult = $payPal->DoExpressCheckoutPayment($result);
		//Detect errors
		if (!$payPal->isCallSucceeded($paymentResult)) {
			if ($payPal->apiLive === true) {
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
			} else {
				//Sandbox output the actual error message to dive in.
				$error = $paymentResult['L_LONGMESSAGE0'];
			}
			$dataEdit = date ("Y-m-d H:i:s");
			$sqlError = "UPDATE `tbl_order` SET `od_status`='Error',`od_last_update`='{$dataEdit}' WHERE `od_id` = '{$orderID}'";
			$resultError = dbQuery($sqlError);
			echo $error;
			exit();
		} else {
			//payment was completed successfully
			//оплата была успешно завершена
			$dataEdit = date ("Y-m-d H:i:s");
			$sql = "UPDATE `tbl_order` SET `od_status`='Paid',`od_last_update`='{$dataEdit}' WHERE `token`='{$token}' AND `od_status`='New' AND `mem_id`={$memberId}";
			$result = dbQuery($sql);

			if ($result) {
                emptyCart();
                /*if (!$_SESSION) session_start();
                if (isset($_SESSION['shop_return_url'])) {
                    $url = $_SESSION['shop_return_url'].'?payment=success';
                } else {
                    $url = '/index.php?payment=success';
                }*/
                $_SESSION['THANKS_FOR_ORDER'] = true;
                header("Location: /shared/cart.php?action=view");
                /*printMessage("<p>Payment is successful</p>".
                            '<a href="'.$url.'">To Shop</a>');*/
			}
		}

	}
} else {
    printMessage('<p>Error token</p>');
	exit();
}

/*
* Защита от SQL инекций protection=1 -замена символов 2- екранировать
*/
function noSQLInjections($sql, $protection = 1)
{
	if ($protection == 1) {
		$sql = str_replace(';', "&#59;", $sql);
		$sql = str_replace('"', "&#34;", $sql);
		$sql = str_replace("'", "&#39;", $sql);
		$sql = str_replace("*", "&#42;", $sql);
		$sql = str_replace("´", "&acute;", $sql);
		$sql = str_replace('`', "&#96;", $sql);
	}
	$sql = mysql_real_escape_string($sql);
	return $sql;
}

function printMessage($msg) {
    include $_SERVER['DOCUMENT_ROOT'].'/shared/include/header.php';
    ?>
    
    <link href="../shoppingcartCSS.css" rel="stylesheet" type="text/css" />

    <table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
            <td colspan="3">
              <?php require_once '../shared/include/top.php'; ?>
            </td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#FFFFFF" class="style11" align="center">
                &nbsp;
            </td>
        </tr>
        <tr valign="top"> 
            <td><?= $msg ?></td>
        </tr>
        <tr valign="top">
            <td height="72" colspan="3" id="leftnav2"><img src="../shared/images/cartfooter.png" width="780" height="72" /></td>
        </tr>
    </table>
    <script language="javascript">
        timeTimer = setTimeout("updateDeliveryInfo()", 1000);
    </script>
    <?
    include $_SERVER['DOCUMENT_ROOT'].'/shared/include/footer.php';
}