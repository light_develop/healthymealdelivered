<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/shared/library/config.php');
$old_tz = date_default_timezone_get();
date_default_timezone_set(getTimeZone());

//Если сегодня выходной или праздник
function ifWeekendOrHolidays($timestamp, &$holiday = false)
{
    while (true) {

        $day_of_week = date('w', $timestamp);
        if (0 == $day_of_week) {
            $holiday = true;
            $timestamp += 24 * 60 * 60;
        }
        if (6 == $day_of_week) {
            $holiday = true;
            $timestamp += 2 * 24 * 60 * 60;
        }

        $getHoliday = dbGetRows('SELECT `date` FROM tbl_holiday WHERE date = "' . date('Y-m-d', $timestamp) . '" AND active = 1 LIMIT 1');
        if (count($getHoliday) > 0) {
            $timestamp += 24 * 60 * 60;
            $holiday = true;
        } else
            break;
    }

    return $timestamp;
}

function getOrderLimit($cut_off_stamp)
{

    $now_date = date('r');
    $cut_off_stamp_date = date('r', $cut_off_stamp);
    $getHoliday = dbGetRows('SELECT `date` FROM tbl_holiday WHERE date = "' . date('Y-m-d', $cut_off_stamp) . '" AND active = 1 LIMIT 1');

    //После девяти
    if ($now_date >= $cut_off_stamp_date) {
        if (count($getHoliday) > 0) {
            $cut_off_stamp += 24 * 60 * 60;
        } elseif (date('w') + 1 == 6) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp + 3 * 24 * 60 * 60);
        } elseif (date('w') == 1) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp + 24 * 60 * 60);
        } elseif (date('w') == 6) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 3 * 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        } elseif (date('w') == 0) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 3 * 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        } else {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp + 24 * 60 * 60);
        }
    } //До девяти
    else {
        if (date('w') + 1 == 6) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        } elseif (date('w') == 1) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 3 * 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        } elseif (date('w') == 6) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 3 * 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        } elseif (date('w') == 0) {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 3 * 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        } else {
            $timeRange1 = date('Y-m-d H:m:s', $cut_off_stamp - 24 * 60 * 60);
            $timeRange2 = date('Y-m-d H:m:s', $cut_off_stamp);
        }
    }

    $sql = dbQuery('SELECT COUNT(od_date) FROM tbl_order WHERE od_date >="' . $timeRange1 . '" AND od_date <="' . $timeRange2 . '" AND od_status = "Paid"');
    $countOrders = dbFetchRow($sql);
    $sql = dbQuery('SELECT sc_order_limit FROM tbl_shop_config');
    $orderLimit = dbFetchRow($sql);
    $ordersLeft = $orderLimit[0] - $countOrders[0];
    $ordersLeft = $ordersLeft <= 0 ? 0 : $ordersLeft;
    $sql = dbQuery('UPDATE tbl_shop_config SET orders_left=' . $ordersLeft);

    $ordersLeft = str_split($ordersLeft);
    $orders = '';

    foreach ($ordersLeft as $order) {
        $orders .= '<span class="digit">' . $order . '</span>';
    }

    echo $orders . '<br>';
//    echo $timeRange1 . '<br>';
//    echo $timeRange2 . '<br>';
//    echo date('Y-m-d',$cut_off_stamp) . '<br>';
}

list($cut_off['H'], $cut_off['i'], $cut_off['s']) = explode(':', getCutOffTime());
$cut_off_stamp = mktime($cut_off['H'], $cut_off['i'], $cut_off['s']);
//$cut_off_stamp = mktime( $cut_off['H'], $cut_off['i'], $cut_off['s'], 2, 27 );

//Анализируем $cut_off_stamp
$current_holiday = false;
$cut_off_stamp = ifWeekendOrHolidays($cut_off_stamp, $current_holiday);
$cut_off_day = date('r', $cut_off_stamp);

$current = array(
    'H' => date('H'),
    'i' => date('i'),
    's' => date('s'),
    'stamp' => date('U')
);

$current_day = date('r', $current['stamp']);

$deliverAfterHoliday = false;
$result_stamp = $cut_off_stamp + 24 * 60 * 60;
$temp_day = date('r', $result_stamp);
$result_stamp = ifWeekendOrHolidays($result_stamp, $deliverAfterHoliday);
$result_day = date('r', $result_stamp);

//Если сегодня праздник или выходной

date_default_timezone_set($old_tz);

//Выводим ответ
?>
<p><strong>Remaining orders: <span><?php getOrderLimit($cut_off_stamp); ?></span></strong></p>