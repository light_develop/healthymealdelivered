<?
include_once($_SERVER['DOCUMENT_ROOT'].'/shared/library/config.php');
$old_tz = date_default_timezone_get();
date_default_timezone_set( getTimeZone() );

//Если сегодня выходной или праздник
function ifWeekendOrHolidays( $timestamp ) {
    while(true) {

        $day_of_week = date('w', $timestamp);
        if (0==$day_of_week) $timestamp += 24*60*60;
        if (6==$day_of_week) $timestamp += 2*24*60*60;

        $getHoliday = dbGetRows('SELECT `date` FROM tbl_holiday WHERE date = "' . date('Y-m-d', $timestamp) . '" LIMIT 1');
        if ( count($getHoliday) > 0 )
            $timestamp += 24*60*60;
        else
            break;
    }

    return $timestamp;
}

list($cut_off['H'], $cut_off['i'], $cut_off['s']) = explode( ':', getCutOffTime() );
$cut_off_stamp = mktime( $cut_off['H'], $cut_off['i'], $cut_off['s'] );

//Анализируем $cut_off_stamp
$cut_off_stamp = ifWeekendOrHolidays( $cut_off_stamp );

$current = array(
    'H'      => date('H'),
    'i'      => date('i'),
    's'      => date('s'),
    'stamp'  => date('U')
);

if ($current['stamp']<$cut_off_stamp) {
    $before_or_after = 'before';
    $cut_off_display_time = date('H:i', $cut_off_stamp);
    if ( date('dmY') == date('dmY', $cut_off_stamp) )
        $cut_off_display_time .= ' today';
    else
        $cut_off_display_time = $cut_off_display_time . ' ' . date('m/d/Y', $cut_off_stamp);

    $result_date = date('m/d/Y', $cut_off_stamp);
} else {
    $before_or_after = 'after';
    $cut_off_display_time = date('H:i', $cut_off_stamp);
    $cut_off_display_time .= ' today';

    $result_stamp = $cut_off_stamp = ifWeekendOrHolidays( $cut_off_stamp+24*60*60 );
    $result_date = date('m/d/Y', $result_stamp);
}

?>
<style>
    #countdown_container { margin-bottom: 10px; }
    #countdown_container p {
        margin: 0;
    }

    #countdown_container p:first-child {
        font-weight: bold;
        color: #008000;
    }
    #countdown_container p + {
        margin-top: 5px;
    }
</style>
<div id="countdown_container">
    <!-- Current date: <?php echo date('H:i m/d/Y'); ?> -->
    <p>Orders placed <?= $before_or_after ?> <?= $cut_off_display_time ?> will be delivered <?= $result_date ?></p>
    <p>Remaining time: <span class="time"></span></p>
    <script type="text/javascript">
        var limit_stamp = '<?= date('r', $cut_off_stamp); ?>';
        var limit_date = new Date( limit_stamp );
        console.log( limit_stamp );
        function formateDatesDiff(date) {
            var hours   = date.getUTCHours();
            var minutes = date.getUTCMinutes(); if (minutes < 10) minutes = '0' + minutes;
            var seconds = date.getUTCSeconds(); if (seconds < 10) seconds = '0' + seconds;
            var str = hours + ':' + minutes + ':' + seconds;
            if (date.getUTCDate() > 1) {
                str = (date.getUTCDate()-1)+' days, '+str;
            }
            return str;
        }
        function updateRemainingTime() {
            var remaining = new Date(limit_date - new Date());
            var rem_str = formateDatesDiff(remaining);
            $('#countdown_container span.time').html(rem_str);
            
            if (rem_str == '0:00:00') {
                reloadTimer();
            }
        }
        function reloadTimer() {
            clearInterval(cron_id);
            $.ajax('/shared/widgets/cut_off_countdown/index.php', {
                success : function(response) {
                    var $self = $('#countdown_container');
                    var $prev = $self.prev();
                    var $parent = $self.parent();
                    $self.remove();
                    if ($prev) {
                        $prev.after(response);
                    } else {
                        $parent.html(response);
                    }
                }
            });
        }
        var cron_id = setInterval(updateRemainingTime, 1000);
    </script>
</div>
<?php date_default_timezone_set( $old_tz ); ?>