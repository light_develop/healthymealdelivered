<?
include_once($_SERVER['DOCUMENT_ROOT'].'/shared/library/config.php');
$old_tz = date_default_timezone_get();
date_default_timezone_set( getTimeZone() );

//Если сегодня выходной или праздник
function ifWeekendOrHolidays( $timestamp, &$holiday = false ) {
    while(true) {

        $day_of_week = date('w', $timestamp);
        if (0==$day_of_week) {
            $holiday = true;
            $timestamp += 24*60*60;
        }
        if (6==$day_of_week) {
            $holiday = true;
            $timestamp += 2*24*60*60;
        }

        $getHoliday = dbGetRows('SELECT `date` FROM tbl_holiday WHERE date = "' . date('Y-m-d', $timestamp) . '" AND active = 1 LIMIT 1');
        if ( count($getHoliday) > 0 ) {
            $timestamp += 24*60*60;
            $holiday = true;
        } else
            break;
    }

    return $timestamp;
}

list($cut_off['H'], $cut_off['i'], $cut_off['s']) = explode( ':', getCutOffTime() );
$cut_off_stamp = mktime( $cut_off['H'], $cut_off['i'], $cut_off['s'] );
//$cut_off_stamp = mktime( $cut_off['H'], $cut_off['i'], $cut_off['s'], 2, 27 );

//Анализируем $cut_off_stamp
$current_holiday = false;
$cut_off_stamp = ifWeekendOrHolidays( $cut_off_stamp, $current_holiday );
$cut_off_day = date('r', $cut_off_stamp);

$current = array(
    'H'      => date('H'),
    'i'      => date('i'),
    's'      => date('s'),
    'stamp'  => date('U')
);

$current_day = date('r', $current['stamp']);

$deliverAfterHoliday = false;
$result_stamp = $cut_off_stamp+24*60*60;
$temp_day = date('r', $result_stamp);
$result_stamp = ifWeekendOrHolidays( $result_stamp, $deliverAfterHoliday );
$result_day = date('r', $result_stamp);

//Если сегодня праздник или выходной
if ( $current_holiday ) {
    //то Доставка в следующий рабочий день. Цвет - красный
    $result_stamp = $cut_off_stamp;
    $orderPlaced = '<p class="red"><strong>Orders placed now will be delivered on ' . date('l', $result_stamp) . '.</strong></p>';
} elseif ( $current['stamp']>=$cut_off_stamp ) {
    //Если сегодня не празник и день будний, но время после cut_off_time
    //Если завтра выходной или праздник
    if ( $deliverAfterHoliday ) {
        //то Доставка в следующий рабочий день. Цвет - красный
        $orderPlaced = '<p class="red"><strong>Orders placed now will be delivered on ' . date('l', $result_stamp) . '.</strong></p>';
    } else {
        //то Доставка будет завтра. Цвет - красный
        $orderPlaced = '<p class="red"><strong>Orders placed now will be delivered tomorrow.</strong></p>';
    }
} else {
    //Если сегодня не празник и день будний, а также время до cut_off_time
    //то доставка сегодня и цвет - зеленый
    $orderPlaced = '<p class="green"><strong>Orders placed by ' . date('g:i A', $cut_off_stamp) . ' will be delivered today!</strong></p>';
    $result_stamp = $cut_off_stamp;
}

/*if ( ($current['stamp']<$cut_off_stamp) && (date('w')!=0) && ( date('w')!=6 ) ) {
    $orderPlaced = '<p class="green"><strong>Orders placed by ' . date('g:i A', $cut_off_stamp) . ' will be delivered today!</strong></p>';
    $result_stamp = $cut_off_stamp;
} elseif ( ($current['stamp']>=$cut_off_stamp) && (date('w')!=0) && ( date('w')!=6 ) && ( date('w')!=5 ) ) {
    $orderPlaced = '<p class="red"><strong>Orders placed now will be delivered tomorrow.</strong></p>';
} else {
    $orderPlaced = '<p class="red"><strong>Orders placed now will be delivered on Monday.</strong></p>';

}*/

/*if ($current['stamp']<$cut_off_stamp) {
    $before_or_after = 'before';
    $cut_off_display_time = date('g:i A', $cut_off_stamp);
    if ( date('dmY') == date('dmY', $cut_off_stamp) )
        $cut_off_display_time .= ' today';
    else
        $cut_off_display_time = $cut_off_display_time . ' ' . date('m/d/Y', $cut_off_stamp);

    $result_stamp = $cut_off_stamp;
    $result_date = date('n/j/y', $cut_off_stamp);
    $class = "green";
} else {
    $before_or_after = 'after';
    $cut_off_display_time = date('g:i A', $cut_off_stamp);
    $cut_off_display_time .= ' today';

    $result_stamp = ifWeekendOrHolidays( $cut_off_stamp+24*60*60 );
//    $result_date = date('n/j/y', $result_stamp);
    $class = "red";

    if ( $result_stamp > ($cut_off_stamp+24*60*60) ) {
        $result_date = date('l', $result_stamp);
    } else {
        $result_date = 'tomorrow';
    }
}*/
date_default_timezone_set( $old_tz );

//Выводим ответ
?>
<!-- Current date: <?php echo date('H:i m/d/Y'); ?> -->
<?php echo $orderPlaced; ?>
<p><strong>Remaining time: <span class="time" data-cut-off-time="<?= date('r', $result_stamp); ?>"></span></strong></p>