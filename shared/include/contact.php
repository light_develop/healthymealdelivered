<?php

if (!defined('WEB_ROOT')) {
	exit;
}

if( isset($_POST["submitButton"]) ){
    
    $name = $_POST["uname"];
    $email = $_POST["uemail"];
    $phone = $_POST["uphone"];
    
    $email = preg_replace('=((<CR>|<LF>|0x0A/%0A|0x0D/%0D|\\n|\\r)\S).*=i',
                         null, $email);
    

    // Your subject
    $comp = $shopConfig["name"];
    $comp_email = $shopConfig["email"];
    
    $subject = "Someone is requesting info from $comp";
    
    // From
    $header = "from: System <$comp_email>";
    
    // send e-mail to ...
    $to=$comp_email;

    // Your message
    $message = "This person has requested information about making their own shakes. \r\n";
    $message .= "Name: $name \r\n";
    $message .= "Email: $email \r\n";
    $message .= "Phone: $phone \r\n";

    // send email
    $sentmail = mail($to,$subject,$message,$header);
    
    if( $sentmail ):
    ?>
        <div style="width: 90%; margin-bottom: 10px;">
            Your information was submitted successfully. We will contact you shortly with more information.
        </div>
    <?php
            else:
    ?>
        <div style="width: 90%; margin-bottom: 10px;">
            Something went wrong. Please go back and try again.
        </div>        
    <?php
    endif;
    
} else {
?>

<form id="contactForm" name="contactForm" method="post" action="index.php?contact=1">
    
    <div>
        Enter your contact information below and we'll get back to you with more information.
    </div>
    
    <table>
        <tr>
            <td>
                Name
            </td>
            <td>
                <input type="text" id="uname" name="uname" />
            </td>
        </tr>
        <tr>
            <td>
                Email
            </td>
            <td>
                <input type="email" id="uemail" name="uemail" />
            </td>
        </tr>
        <tr>
            <td>
                Phone
            </td>
            <td>
                <input type="tel" id="uphone" name="uphone" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" id="submitButton" name="submitButton" value="Send us your Info" /></td>
        </tr>
    </table>
    
</form>

<script>

    $(document).ready(function(){
        
        $("#contactForm").submit(function(e){
            e.preventDefault();
            
            if($("#uname").val().length < 1){
                alert("Please enter your name");
                return;
            }
            
            if($("#uphone").val().length < 1 && $("#uemail").val().length < 1){
                alert("Please enter either an email address or phone number so we can contact you.");
                return;
            }
            
            this.submit();
        });
        
    });

</script>

<?php
}
?>


        
