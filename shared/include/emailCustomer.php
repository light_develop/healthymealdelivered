<?php
require_once '../library/repeatConfig.php';

// get all New orders with delivery date of tomorrow
$sql = "SELECT * FROM tbl_order 
        JOIN tbl_members ON mem_id = tbl_members.id
        WHERE --`od_five_day` = 1 AND
              --`an_transaction_id` IS NULL AND
              od_status = 'New' AND
              DATE(od_delivery_date) = DATE(NOW()) ";
$result = dbQuery($sql);

while($rows = mysql_fetch_array($result))
{
    $custProfID = $rows['customerProfileId'];
    $customerPaymentProfileId = $rows['payment_profile_id'];
    $customerShippingAddressId = $rows['shipping_address_id'];
    $orderId = $rows['od_id'];
    $saveInfo = $rows['saveInfo'];
    $orderDate = $rows['od_date'];
    $transactionID = $rows['an_transaction_id'];
    $memberID = $rows['mem_id'];
    
    $sql = "SELECT oi.pd_id, pd_name, pd_price, od_qty, tea_name
	    FROM tbl_order_item oi, tbl_product p, tbl_teas t
		WHERE oi.pd_id = p.pd_id and oi.tea_id = t.tea_id and oi.od_id = $orderId
		ORDER BY od_id ASC";
    $result = dbQuery($sql);
    $orderedItem = array();
    $amount = 0;
    while ($row = mysql_fetch_assoc($result)) {
            $orderedItem[] = $row;
            $amount += $row['pd_price'] * $row['od_qty'];
    }
    
    // only authorize payment if not already done
    if(strlen($transactionID)<1)
    {
        // Authorize payment
        $request = '<?xml version="1.0" encoding="utf-8"?>
        <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                '.MerchantAuthenticationBlock().'
                <transaction>
                        <profileTransAuthOnly>
                                <amount>'.$amount.'</amount>';

                foreach($orderedItem as $item)
                {
                    $name = substr(htmlspecialchars ($item["pd_name"].' '.$item["tea_name"]),0,30);
                    $ct_id = $item['pd_id'];
                    $ct_qty = $item['od_qty'];
                    $pd_price = $item['pd_price'];

                    $request .= "<lineItems>
                                        <itemId>$ct_id</itemId>
                                        <name>$name</name>
                                        <quantity>$ct_qty</quantity>
                                        <unitPrice>$pd_price</unitPrice>
                                </lineItems>";
                }

                    $request .= "<customerProfileId>$custProfID</customerProfileId>
                                <customerPaymentProfileId>$customerPaymentProfileId</customerPaymentProfileId>
                                <customerShippingAddressId>$customerShippingAddressId</customerShippingAddressId>
                                <order>
                                        <invoiceNumber>$orderId</invoiceNumber>
                                </order>
                                <recurringBilling>false</recurringBilling>";
    //
    //                if(isset($ccv) && strlen($ccv) > 0)
    //                {
    //                        $request .= "<cardCode>$ccv</cardCode>";
    //                }


                    $request .= "</profileTransAuthOnly>
                </transaction>
        </createCustomerProfileTransactionRequest>";


        //submit transaction for this order
        $response = send_xml_request($request);
        
        // if successful, insert transaction id
        $parsedresponse = parse_api_response($response);
        if ("Ok" == $parsedresponse->messages->resultCode) 
        {
            $creditResult = explode(',',$parsedresponse->directResponse);

            //summary of result parts can be found at http://www.authorize.net/support/AIM_guide.pdf on page 38

            switch($creditResult[0])
            {
                case 1: $responseCode = "Approved";
                    break;
                case 2: $responseCode = "Declined";
                    break;
                case 3: $responseCode = "Error";
                    break;
                case 4: $responseCode = "Held for Review";
                    break;
            }

            $responseReasonCode = $creditResult[2];
            $responseReasonText = $creditResult[3];
            $transactionID      = $creditResult[6];
            $amount             = $creditResult[9];
            $paymentMethod      = $creditResult[10];
            $customerID         = $creditResult[12];
            $billingFirst       = $creditResult[13];
            $billingLast        = $creditResult[14];
            $emailAddress       = $creditResult[23];
            $shippingFirst      = $creditResult[24];
            $shippingLast       = $creditResult[25];
            $cardNumber         = $creditResult[50];
            
            if($creditResult[0] > 1)
            {
                //there was an issue processing the card
                
                $sql = "REPLACE INTO tbl_invalid_cards 
                        VALUES ($customerPaymentProfileId, $memberID)";
                
                $result = mysql_query($sql);


                // SEND EMAIL
                $to = $emailAddress;
                $subject = "There was a problem with your order";

                $message = "<html>
                        <head>
                        <title>There was a problem with your Minot Nutrition Addiction Online order</title>
                        </head>
                        <body>
                            <p>Unfortunately, there was an issue with your card so we will not be able to complete your order for tomorrow.</p>
                            <p>Please log in and update your payment information. You can do so by clicking <a href='http://".BASE_URL.$_SESSION['organization']."/index.php?id=$memberID'>here</a>.</p>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        Error:
                                    </td>
                                    <td>
                                        $responseCode
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Reason
                                    </td>
                                    <td>
                                        $responseReasonText
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total
                                    </td>
                                    <td>
                                        $amount
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Payment Method
                                    </td>
                                    <td>
                                        $paymentMethod
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Card or Account #
                                    </td>
                                    <td>
                                        $cardNumber
                                    </td>
                                </tr>
                            </table>
                        </body>
                        </html>";

                // To send HTML mail, the Content-type header must be set
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                mail ( $to , $subject , $message , $headers );
            }
            else
            {
                //add Auth.Net transactionID to order table for this order and update status
                $sql = "UPDATE tbl_order 
                        SET an_transaction_id = ".$creditResult[6]."
                        WHERE od_id = $orderId";
                $result = dbQuery($sql);

                // SEND EMAIL
                $to = $emailAddress;
                $subject = "A reminder about your order to be delivered tomorrow";

                foreach($orderedItem as $item)
                {
                    $name = substr(htmlspecialchars ($item["pd_name"].' '.$item["tea_name"]),0,30);
                    $ct_id = $item['pd_id'];
                    $ct_qty = $item['od_qty'];
                    $pd_price = $item['pd_price'];

                    $items .= "<tr>
                                    <td>$ct_id</td>
                                    <td>$name</td>
                                    <td>$ct_qty</td>
                                    <td>$pd_price</td>
                                </tr>";
                }
                
                $message = "<html>
                        <head>
                        <title>Your Minot Nutrition Addiction order will be delivered tomorrow</title>
                        </head>
                        <body>
                            <p>Here is a summary of the order you will receive tomorrow.</p>
                            <p>If you would like to change or cancel this order, please log in on MinotNutritionAddiction.com and click \"Manage Orders.\"
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>Item ID</td>
                                                <td>Name</td>
                                                <td>Quantity</td>
                                                <td>Price</td>
                                            </tr>
                                            $items
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Total
                                    </td>
                                    <td>
                                        $amount
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Payment Method
                                    </td>
                                    <td>
                                        $paymentMethod
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Card or Account #
                                    </td>
                                    <td>
                                        $cardNumber
                                    </td>
                                </tr>
                            </table>
                        </body>
                        </html>";

                // To send HTML mail, the Content-type header must be set
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                mail ( $to , $subject , $message , $headers );
            }
        }
    }
}
?>
