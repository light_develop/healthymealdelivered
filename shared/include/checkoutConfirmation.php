<?php
/*
Line 1 : Make sure this file is included instead of requested directly
Line 2 : Check if step is defined and the value is two
Line 3 : The POST request must come from this page but the value of step is one
*/
if (!defined('WEB_ROOT')
    || !isset($_GET['step']) || (int)$_GET['step'] != 2
	|| ($_SERVER['HTTP_REFERER'] != 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?step=1' && $_SERVER['HTTP_REFERER'] != 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?step=1&info=1') ) {
    
        printf("<script>location.href='../".$_SESSION['organization']."/index.php';</script>");
	exit;
}
$errorMessage = '&nbsp;';

/*
 Make sure all the required field exist is $_POST and the value is not empty
 Note: txtShippingAddress2 and txtPaymentAddress2 are optional
*/
//$requiredField = array('txtBillingName', 'txtBillingAddress', 'txtBillingCity', 'txtBillingState', 'txtBillingZipCode',  'txtCardType', 'txtShowNumber',
                       //'txtCardMonth', 'txtCardYear', 'txtCardCVV');
$requiredField = array('payment', 'btnProceed', 'customerPaymentProfileId');


if (!checkRequiredPost($requiredField)) {
	$errorMessage = 'Input not complete';
}

$cartContent = getCartContent();


$customerPaymentProfileId = $_POST['payment'];
$customerPaymentProfileId = explode(',', $customerPaymentProfileId);

    
//if((strlen($_POST['ccv']) < 1) && $customerPaymentProfileId[2] == "Card")
//{
//    
//    printf("<script>location.href='checkout.php?step=1&error=CCV value is missing.';</script>");
//    exit;
//}
?>
<table width="550" border="0" align="center" cellpadding="10" cellspacing="0">
    <tr> 
        <td>Step 2 of 3 : Confirm Order </td>
    </tr>
</table>
<p id="errorMessage"><?php echo $errorMessage; ?></p>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>?step=3" method="post" name="frmCheckout" id="frmCheckout">
    <table width="550" border="0" align="center" cellpadding="5" cellspacing="1" class="infoTable">
        <tr class="infoTableHeader"> 
            <td colspan="4">Ordered Item(s)</td>
        </tr>
        <tr class="label">
        	<td>Qty</td> 
            <td>Item</td>
            <td>Unit Price</td>
            <td>Total</td>
        </tr>
        <?php
        $numItem  = count($cartContent);
        $subTotal = 0;
        for ($i = 0; $i < $numItem; $i++) {
            extract($cartContent[$i]);
            $subTotal += $pd_price * $ct_qty;
        ?>
        <tr class="content">
        	<td><?php echo $ct_qty; ?></td> 
            <td class="content"><?php echo "$pd_name Shake & $tea_name Tea"; ?></td>
            <td align="right"><?php echo displayAmount($pd_price); ?></td>
            <td align="right"><?php echo displayAmount($ct_qty * $pd_price); ?></td>
        </tr>
        <?php
            echo "<input type='hidden' id='itemName_$i' value='$pd_name Shake &amp; $tea_name Tea' />";
            echo "<input type='hidden' id='itemPrice_$i' value='$pd_price' />";
            echo "<input type='hidden' id='itemQty_$i' value='$ct_qty' />";
        }
        $five = '';
        
        if(isset($_SESSION['days']))
        {
            $five = $subTotal * $_SESSION['days'];
            $subTotal += $five;
        echo '<tr class="content"> 
                <td colspan="3" align="right" style="height: 35px">
                    5-day order option
                </td>
                <td align="right" style="height: 35px">'.displayAmount($five).'</td>
            </tr>';
        }
        ?>
        <tr class="content"> 
            <td colspan="3" align="right" style="height: 35px">Coupon discount
            </td>
            <td align="right" style="height: 35px">&nbsp;</td>
        </tr>
        
        <tr class="content"> 
            <td colspan="3" align="right">Total to be charged to <?php echo $customerPaymentProfileId[2]?> ending in <strong><?php echo substr($customerPaymentProfileId[0], -4); ?></strong></td>
            <td align="right"><?php echo displayAmount($subTotal); ?></td>
        </tr>
        
        <tr>
            <td colspan="4" align="center">
                <strong>This order will be delivered on <?php $date = getDeliveryDate(); echo date("l", strtotime($date)).", " . $date; ?></strong>
            </td>
        </tr>
    </table>
<!--    <p><div id="deliveryInfo" style="text-align:center">&nbsp;</div></p>-->
    <p align="center"> 
        <input name="btnBack" type="button" id="btnBack" value="&lt;&lt; Modify Payment Info" onClick="window.location.href='checkout.php?step=1';" class="box">
        &nbsp;&nbsp; 
        <input name="btnConfirm" type="submit" id="btnConfirm" value="Place Order &gt;&gt;" class="box">
        
    <?php 
    
    $customerShippingAddressId = $_POST['shipping'];
    $ccv = null;
    if(isset($_POST['ccv'])){$ccv = $_POST['ccv'];}
    $saveinfo = 0;
    if(isset($_POST['saveinfo']))$saveinfo = $_POST['saveinfo'];
    echo "<input type='hidden' id='amount' name='amount' value='$subTotal' />";
    echo "<input type='hidden' id='customerPaymentProfileId' name='customerPaymentProfileId' value='$customerPaymentProfileId[1]' />";
    //echo "<input type='hidden' id='customerShippingAddressId' name='customerShippingAddressId' value='$customerShippingAddressId' />";
    echo "<input type='hidden' id='ccv' name='ccv' value='$ccv' />";
    echo "<input type='hidden' id='saveinfo' name='saveinfo' value='$saveinfo' />";
    ?>
</form>
<p class="footerBar">&nbsp;</p>
