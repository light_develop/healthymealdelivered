<?php

$rowsPerPage = 10;
$who = $_SESSION['mname'];
$id = $_SESSION['mid'];

$org_id = $_SESSION["organization_id"];

$sql = "SELECT o.od_id, 
            o.mem_id,
            m.id, 
            name, 
            od_date,
            od_status,
            SUM(pd_price * od_qty) AS od_amount,
            od_five_day,
            od_delivery_date
            FROM tbl_order o, tbl_order_item oi, tbl_product p, tbl_members m
                WHERE oi.pd_id = p.pd_id and
                    o.od_id = oi.od_id and 
                    o.mem_id = m.id and
                    m.id = $id and
                    o.od_status in ('New', 'Paid') and
                    o.od_org_id = $org_id
                GROUP BY od_id
                ORDER BY od_id DESC";
$result     = dbQuery(getPagingQueryForTable($sql, $rowsPerPage, 'pending'));
$pagingLink = getPagingLinkForTable($sql, $rowsPerPage, 'manage=1', 'pending');

?>

<h1>Orders</h1>

<div id="tabs" style="width: 95%;">
    <ul>
        <li><a href="#tabs-1">Pending</a></li>
        <li><a href="#tabs-2">Previous</a></li>
    </ul>
    
    <div id="tabs-1" style="padding: 5px;">
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="text">
        <tr align="center" id="listTableHeader"> 
        <td>Order #</td>
        <td align="right">Amount</td>
        <td>Ordered</td>
        <td>Deliver</td>
        <td>Status</td>
        </tr>
        <?php
        $parentId = 0;
        if (dbNumRows($result) > 0) {
                $i = 0;

                while($row = dbFetchAssoc($result)) {
                        extract($row);
                        $name = $name;

                        if ($i%2) {
                                $class = 'row1';
                        } else {
                                $class = 'row2';
                        }

                        $i += 1;
        ?>
        <tr class="<?php echo $class; ?>"> 
        <td align="center"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?manage=1&oid=<?php echo $od_id; ?>"><?php echo $od_id; ?></a></td>
        <td align="right"><?php echo displayAmount($od_amount); ?></td>
        <td align="center"><?php echo $od_date; ?></td>
        <td align="center"><?php echo $od_delivery_date; ?></td>
        <td align="center"><?php echo $od_status; ?></td>
        </tr>
        <?php
                } // end while

        ?>
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr> 
        <td colspan="5" align="center">
        <?php 
        echo $pagingLink;
        ?></td>
        </tr>
        <?php
        } else {
        ?>
        <tr> 
        <td colspan="5" align="center">No Orders Found </td>
        </tr>
        <?php
        }
        ?>

        </table>
    </div>
    <div id="tabs-2" style="padding: 5px;">
        <?php
        $sql2 = "SELECT o.od_id, 
            o.mem_id,
            m.id, 
            name, 
            od_date,
            od_status,
            SUM(pd_price * od_qty) AS od_amount,
            od_five_day,
            od_delivery_date
            FROM tbl_order o, tbl_order_item oi, tbl_product p, tbl_members m
                WHERE oi.pd_id = p.pd_id and
                    o.od_id = oi.od_id and 
                    o.mem_id = m.id and
                    m.id = $id and
                    o.od_status NOT in ('New', 'Paid') and
                    o.od_org_id = $org_id
                GROUP BY od_id
                ORDER BY od_id DESC";
        $result2     = dbQuery(getPagingQueryForTable($sql2, $rowsPerPage, 'previous'));
        $pagingLink = getPagingLinkForTable($sql2, $rowsPerPage, 'manage=1', 'previous');
        ?>
        
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="0" class="text">
        <tr align="center" id="listTableHeader"> 
            <td>Order #</td>
            <td align="right">Amount</td>
            <td>Ordered</td>
            <td>Deliver</td>
            <td>Status</td>
        </tr>
        <?php
        $parentId = 0;
        if (dbNumRows($result2) > 0) {
                $i = 0;

                while($row = dbFetchAssoc($result2)) {
                        extract($row);
                        $name = $name;

                        if ($i%2) {
                                $class = 'row1';
                        } else {
                                $class = 'row2';
                        }

                        $i += 1;
        ?>
        <tr class="<?php echo $class; ?>"> 
            <td align="center"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?manage=1&oid=<?php echo $od_id; ?>"><?php echo $od_id; ?></a></td>
            <td align="right"><?php echo displayAmount($od_amount); ?></td>
            <td align="center"><?php echo $od_date; ?></td>
            <td align="center"><?php echo $od_delivery_date; ?></td>
            <td align="center"><?php echo $od_status; ?></td>
        </tr>
        <?php
                } // end while

        ?>
        <tr>
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr> 
            <td colspan="5" align="center">
            <?php 
            echo $pagingLink;
            ?></td>
        </tr>
        <?php
        } else {
        ?>
        <tr> 
        <td colspan="5" align="center">No Orders Found </td>
        </tr>
        <?php
        }
        ?>

        </table>
    </div>
</div>