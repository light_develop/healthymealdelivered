<div class="policies">
    <h2 id="privacy">Privacy Policy</h2>
    <div class="policyText">
        We respect and are committed to protecting your privacy.
        We may collect personally identifiable information when you visit our site.
        We also automatically receive and record information on our server logs from your browser including your IP address, cookie information and the page(s) you visited.
        We will NOT sell, rent or distribute your personally identifiable information to anyone EVER.
    </div>

    <h2 id="security">Security Policy</h2>
    <div class="policyText">
        Your payment and personal information is always safe.
        Our Secure Sockets Layer (SSL) software is the industry leader and the best software available today for secure commerce transactions.
        It encrypts all of your personal information, including credit card number, name, and address, so that it cannot be read over the Internet.
    </div>

    <h2 id="refunds">Refund Policy</h2>
    <div class="policyText">
        We offer a 30 day Money Back Guarantee on every product we sell.
        Please Contact us thru the website within 30 days of purchase date.
        All refunds will be provided as a credit to the credit card used at the time of purchase within five (5) business days of receipt of your request for a refund.
    </div>

    <h2 id="delivery">Delivery Policy</h2>
    <div class="policyText">
        Please be assured that your shake and tea will be delivered today if ordered before 9 AM.
        If ordered after 9 AM Monday - Thursday it will be delivered the following day. If ordered on a Friday 
				after 9 AM it will be delivered on Monday. For any bulk orders of product that you use to make them 
				yourself at home at a discount we determine the most efficient shipping carrier for your order.
        The carriers that may be used are: United Parcel Service (UPS) or FedEx.
        Sorry but we cannot ship to P.O. boxes.
        If you're trying to estimate when a package will be delivered, please note the following: Credit card authorization and verification must be received prior to 
				the order being shipped and shipping may take up to 7 days.
        Federal Express and or UPS deliveries occur Monday through Friday, excluding holidays.
        All orders will be charged the appropriate sales tax for the zip code the product is delivered to.
    </div>
</div>