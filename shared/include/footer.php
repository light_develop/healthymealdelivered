<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$org = $_SESSION['organization'];

$pageURL = 'http';
 if (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";

$server = $pageURL.$_SERVER["SERVER_NAME"]."/dev/omnao/$org/index.php?";

$main = $pageURL.$_SERVER["SERVER_NAME"]."/dev/omnao/$org/index.php";
?>
<div class="footer">
    
   <div class="footerLinks">
       <a href="../ing/index.php?policy=privacy#privacy">Privacy Policy</a>
       <a href="../ing/index.php?policy=security#security">Security Policy</a>
       <a href="../ing/index.php?policy=refunds#refunds">Refunds</a>
       <a href="../ing/index.php?policy=delivery#delivery">Delivery</a>
   </div>
    
    <div class="footerLinks" style="margin-bottom: 5px;">
        <a href="../ing/index.php?shakeInfo=1">What's in our Shakes?</a>
    </div>
    
    <div class="footerLinks" style="margin-top: 0px;">
        <a href="../ing/index.php?contact=1">Would you like to make these yourself at home at a discount?</a>
    </div>
    
    <div class="copyright">&copy; <?php echo date('Y'); ?> <?php echo $shopConfig['name']; ?></div>
    &mdash;
    <div class="contact">
        <?php echo $shopConfig['address']; ?><br>
        <strong><?php echo $shopConfig['phone']; ?></strong><br>
        <a href="mailto:<?php echo $shopConfig['email']; ?>"><?php echo $shopConfig['email']; ?></a>
    </div>
    
    <?php if(isMobile()) { ?>
        <div style="margin-top: 2em; font-size: 1.3em;">
            <a href="#" onclick="document.getElementById('fullForm').submit()">Mobile Site</a>
        </div>

        <form style="visibility: hidden;" id="fullForm" name="fullForm" action="<?php echo "http://".BASE_URL.$_SESSION['organization']; ?>/mobile/" method="post" data-ajax="false">
            <input type="hidden" name="size" id="size" value="small" />
        </form>
    <?php } ?>
</div>  
</body>
</html>