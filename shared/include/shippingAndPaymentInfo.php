<?php
include_once ("../shared/authorize/php_cim/XML/vars.php");
include_once ("../shared/authorize/php_cim/XML/util.php");

$custProfID = null;
if(!isset($_SESSION['custProfID'])) 
{
    //header( 'Location: '.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/cart.php' ) ;
    printf("<script>location.href='http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/cart.php'</script>");
}

$custProfID = $_SESSION['custProfID'];
$request = '<?xml version="1.0" encoding="utf-8"?>
                    <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                        '.MerchantAuthenticationBlock().'
                        <customerProfileId>'.$custProfID.'</customerProfileId>
                        <hostedProfileSettings>
                            <setting>
                                <settingName>hostedProfileReturnUrl</settingName>
                                <settingValue>http://'.BASE_URL.'shared/checkout.php?step=1</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfileReturnUrlText</settingName>
                                <settingValue>Continue to checkout</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfilePageBorderVisible</settingName>
                                <settingValue>true</settingValue>
                            </setting>
                        </hostedProfileSettings>
                    </getHostedProfilePageRequest>';
        
        $response = send_xml_request($request);
                        
        $parsedresponse = parse_api_response($response);
        if ("Ok" == $parsedresponse->messages->resultCode) {
            $token = $parsedresponse->token;
            
        }


$request = '<?xml version="1.0" encoding="utf-8"?>
            <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                '.MerchantAuthenticationBlock().'
                <customerProfileId>'.$custProfID.'</customerProfileId>
            </getCustomerProfileRequest>';

$response = send_xml_request($request);

$parsedresponse = parse_api_response($response);
if ("Ok" == $parsedresponse->messages->resultCode) {
    $paymentProfiles = $parsedresponse->profile;
    $shipTo = $parsedresponse->profile->shipToList;//array of all available shipping addresses
    
}

    echo'
            <form method="post" action="https://test.authorize.net/profile/manage"
            id="formAuthorizeNetPage" style="display:none;">
            <input type="hidden" name="Token" value="'.$token.'"/>
            </form>';?>
<table width="550" border="0" align="center" cellpadding="10" cellspacing="0">
    <?php 
        if(isset($_GET['error']))
        {
            $msg = $_GET['error'];
            echo "<tr>
                    <td><span style='color:red;'>There was an error processing your payment information: $msg </br> Please check your payment info and try again.</span></td>
                </tr>";
        }
    ?>
    <tr> 
        <td>Step 1 of 3 : Select Payment and Shipping Information </td>
    </tr>
</table>

    <table width="550" border="0" align="center" cellpadding="5" cellspacing="1">
        <tr>
            <td align="right">
                <input name="btnEdit" type="button" id="btnEdit" onClick="document.getElementById('formAuthorizeNetPage').submit();" value="Edit Payment Information &gt;&gt;" class="box">
            </td>
        </tr>
    </table>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>?step=2" method="post" onsubmit="return validate();" name="frmCheckout" id="frmCheckout">

    <table width="550" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
        <tr class="entryTableHeader"> 
            <td colspan="2">Choose Payment Method</td>
        </tr>
        <?php 
            foreach($paymentProfiles->paymentProfiles as $profile)
            {
                $type;
                $label;
                $number;
                $paymentProfileID = $profile->customerPaymentProfileId;
                if($profile->payment->creditCard)
                {
                    $type = 'Credit Card';
                    $label = 'Card';
                    $number = $profile->payment->creditCard->cardNumber;
                    
                }
                else
                {
                    $type = 'Bank';
                    $label = 'Account';
                    $number = $profile->payment->bankAccount->accountNumber;
                }
                echo "<tr>

                    <td colspan='2'>
                        <input type='hidden' id='customerPaymentProfileId' name='customerPaymentProfileId' value='$paymentProfileID' />
                        <input type='radio' name='payment' id='$paymentProfileID' value='$number,$paymentProfileID,$label' class='radio $type' onclick='ccv_view()'/>
                        $type
                    </td>
                </tr>
                <tr> 
                    <td width='150' class='label'>$label Number</td>
                    <td class='content'>
                        $number
                        
                </td>
                </tr>";
        
        
            }
            
            $info = 'checked="checked"';
            if(!(isset($_SESSION['saveinfo']) && $_SESSION['saveinfo'] == 1))
            {
                $info = '';
            }
        ?>

        <tr>
            <td colspan="2" align="right">
                Save this information for next time? <input type="checkbox" id="saveinfo" name="saveinfo" <?php echo $info ?> /><br/>
                <span style="font-size: 10px;">If this is a 5 day order, your information will be saved until all orders have been shipped.</span>
            </td>
        </tr>
    </table>
    
    
    <p><div id="deliveryInfo" style="text-align:center">&nbsp;</div></p>
    <p align="center" class="footerBar"> 
        <input name="btnBack" type="button" id="btnBack" value="&lt;&lt; Back to Cart" onClick="window.location.href='cart.php?action=view';" class="box">
        &nbsp;&nbsp; 
        <input class="box" name="btnProceed" type="submit" id="btnProceed" value="Proceed &gt;&gt;">
    </p>
    
</form>
<script src="library/jquery-1.7.2.min.js" type="text/javascript"></script>
<script language="javascript">
    
    $(document).ready(function(){
        ccv_view();
    });
    
function ccv_view() {
    
    if($('input[name=payment]:checked').hasClass('Bank')) {
        $('#ccvBlock').slideUp(500);
    }
    else {
        $('#ccvBlock').slideDown(500);
    }
    
}
    
    function validate() {
    
    
    if(!$('input[name=payment]:checked').length > 0){
       
        
        alert('Please select a form of payment');
        return false;
    }
    
//    if(!$('input[name=payment]:checked').hasClass('Bank') && (!$('#ccv').val().length > 0)){
//
//        alert('Please enter your card\'s CCV number');
//        return false;
//
//    }
//    if(!$('input[name=shipping]:checked').length > 0){
//        alert('Please select a shipping address');
//        return false;
//    }
}

    
//checkCCPaymentInfo();



</script>