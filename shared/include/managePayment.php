<?php
include_once ("../shared/authorize/php_cim/XML/vars.php");
include_once ("../shared/authorize/php_cim/XML/util.php");

$edit = (isset($_GET['edit']) && $_GET['edit'] != '') ? $_GET['edit'] : 0;
$order_id = (isset($_GET['oid']) && $_GET['oid'] != '') ? $_GET['oid'] : 0;

$error="";

if(isset($_SESSION['mid']))
{
    $id = $_SESSION['mid'];
}
else
{
    printf("<script>location.href='index.php'</script>");
}

$sql = "SELECT customerProfileId FROM tbl_members WHERE id =$id";

$result = dbQuery($sql);

$rows = mysql_fetch_array($result);
$custProfID = $rows['customerProfileId'];


//////////////////// UPDATE (from edit screen) CODE HERE ////////////////////
if(isset($_POST["payment"]) && isset($_SESSION['mid']))
{
    $pmntPrflId = $_POST["payment"][0];
    
    // update tbl_order.payment_profile_id where od_id = $oid
    $sql = "UPDATE tbl_order
            SET payment_profile_id = $pmntPrflId
            WHERE od_id = $order_id AND
            mem_id = $id";
    dbQuery($sql);
    
    // submit createCustomerProfileTransactionRequest
    $sql = "SELECT * FROM tbl_order 
            JOIN tbl_members ON mem_id = tbl_members.id
            WHERE tbl_order.od_id = $order_id";
    $result = dbQuery($sql);
    
    $row = mysql_fetch_array($result);
    
    ///potential duplicate code
    $custProfID = $row['customerProfileId'];
    $customerPaymentProfileId = $row['payment_profile_id'];
    $customerShippingAddressId = $row['shipping_address_id'];
    $orderId = $row['od_id'];

    $sql = "SELECT oi.pd_id, pd_name, pd_price, od_qty, tea_name
            FROM tbl_order_item oi, tbl_product p, tbl_teas t
                WHERE oi.pd_id = p.pd_id and oi.tea_id = t.tea_id and oi.od_id = $orderId
                ORDER BY od_id ASC";
    $result = dbQuery($sql);
    $orderedItem = array();
    $amount = 0;
    while ($row = mysql_fetch_assoc($result)) {
            $orderedItem[] = $row;
            $amount += $row['pd_price'] * $row['od_qty'];
    }

    $request = '<?xml version="1.0" encoding="utf-8"?>
    <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
            '.MerchantAuthenticationBlock().'
            <transaction>
                    <profileTransAuthOnly>
                            <amount>'.$amount.'</amount>';

            foreach($orderedItem as $item)
            {
                $name = substr(htmlspecialchars ($item["pd_name"].' '.$item["tea_name"]),0,30);
                $ct_id = $item['pd_id'];
                $ct_qty = $item['od_qty'];
                $pd_price = $item['pd_price'];

                $request .= "<lineItems>
                                    <itemId>$ct_id</itemId>
                                    <name>$name</name>
                                    <quantity>$ct_qty</quantity>
                                    <unitPrice>$pd_price</unitPrice>
                            </lineItems>";
            }

                $request .= "<customerProfileId>$custProfID</customerProfileId>
                            <customerPaymentProfileId>$customerPaymentProfileId</customerPaymentProfileId>
                            <customerShippingAddressId>$customerShippingAddressId</customerShippingAddressId>
                            <order>
                                    <invoiceNumber>$orderId</invoiceNumber>
                            </order>
                            <recurringBilling>false</recurringBilling>";

                $request .= "</profileTransAuthOnly>
            </transaction>
    </createCustomerProfileTransactionRequest>";


    //submit transaction 
    $response = send_xml_request($request);
    ///potential duplicate code
    
    
    // update tbl_order.an_transaction_id
    $parsedresponse = parse_api_response($response);
    if ("Ok" == $parsedresponse->messages->resultCode) 
    {
        $creditResult = explode(',',$parsedresponse->directResponse);
        $sql = "UPDATE tbl_order 
                SET an_transaction_id = ".$creditResult[6]."
                WHERE od_id = $orderId";
        $result = dbQuery($sql);
        
        printf("<script>location.href='?id=$id'</script>");
    }
    else
    {
        $error = "This payment method is also invalid. Please update it, or select a different one.";
        
        $sql = "INSERT INTO tbl_invalid_cards
                VALUES ($custProfID,$id)";
        dbQuery($sql);
    }
}
//////////////////////////////////////////////////////////

if(isset($_GET["update"]))
{
    // Select all invalid cards for this user
    $sql = "SELECT *
            FROM tbl_invalid_cards
            WHERE mem_id = $id";
    $result = dbQuery($sql);
    
    $rows = array();
    while($rows[] = mysql_fetch_array($result));
    array_pop($rows);

    // Now, try to authorize each invalid card for the next order using it
    
    foreach($rows as $card)
    {
        $card = $card["payment_profile_id"];
        
        $sql = "SELECT * FROM tbl_order 
                JOIN tbl_members ON mem_id = tbl_members.id
                WHERE payment_profile_id = $card AND
                      od_status = 'New'
                ORDER BY od_date ASC
                LIMIT 1";
        $result = dbQuery($sql);

        while($rows = mysql_fetch_array($result))
        {
            $custProfID = $rows['customerProfileId'];
            $customerPaymentProfileId = $rows['payment_profile_id'];
            $customerShippingAddressId = $rows['shipping_address_id'];
            $orderId = $rows['od_id'];

            $sql = "SELECT oi.pd_id, pd_name, pd_price, od_qty, tea_name
                    FROM tbl_order_item oi, tbl_product p, tbl_teas t
                        WHERE oi.pd_id = p.pd_id and oi.tea_id = t.tea_id and oi.od_id = $orderId
                        ORDER BY od_id ASC";
            $result = dbQuery($sql);
            $orderedItem = array();
            $amount = 0;
            while ($row = mysql_fetch_assoc($result)) {
                    $orderedItem[] = $row;
                    $amount += $row['pd_price'] * $row['od_qty'];
            }

            // Authorize payment
            $request = '<?xml version="1.0" encoding="utf-8"?>
            <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                    '.MerchantAuthenticationBlock().'
                    <transaction>
                            <profileTransAuthOnly>
                                    <amount>'.$amount.'</amount>';

                    foreach($orderedItem as $item)
                    {
                        $name = substr(htmlspecialchars ($item["pd_name"].' '.$item["tea_name"]),0,30);
                        $ct_id = $item['pd_id'];
                        $ct_qty = $item['od_qty'];
                        $pd_price = $item['pd_price'];

                        $request .= "<lineItems>
                                            <itemId>$ct_id</itemId>
                                            <name>$name</name>
                                            <quantity>$ct_qty</quantity>
                                            <unitPrice>$pd_price</unitPrice>
                                    </lineItems>";
                    }

                        $request .= "<customerProfileId>$custProfID</customerProfileId>
                                    <customerPaymentProfileId>$customerPaymentProfileId</customerPaymentProfileId>
                                    <customerShippingAddressId>$customerShippingAddressId</customerShippingAddressId>
                                    <order>
                                            <invoiceNumber>$orderId</invoiceNumber>
                                    </order>
                                    <recurringBilling>false</recurringBilling>";

                        $request .= "</profileTransAuthOnly>
                    </transaction>
            </createCustomerProfileTransactionRequest>";


            //submit transaction 
            $response = send_xml_request($request);

            // if the card validates, remove it from the invalid cards table
            $parsedresponse = parse_api_response($response);
            if ("Ok" == $parsedresponse->messages->resultCode) 
            {
                $sql = "DELETE FROM tbl_invalid_cards
                        WHERE payment_profile_id = $card";
                dbQuery($sql);
            }
            //if the card doesn't validate, alert the user
            else
            {
                $error = "An invalid payment method still exists and is being used. Please Update.";
            }
        }
    }
}

if($edit)
{
    $return_url = "http://".BASE_URL.$_SESSION['organization']."/index.php?edit=1&amp;oid=$order_id&amp;id=$id";
}
else
{
    $return_url = "http://".BASE_URL.$_SESSION['organization']."/index.php?update=1&amp;id=$id";
}

$request = '<?xml version="1.0" encoding="utf-8"?>
                    <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                        '.MerchantAuthenticationBlock().'
                        <customerProfileId>'.$custProfID.'</customerProfileId>
                        <hostedProfileSettings>
                            <setting>
                                <settingName>hostedProfileReturnUrl</settingName>
                                <settingValue>'.$return_url.'</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfileReturnUrlText</settingName>
                                <settingValue>Finished Editing</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfilePageBorderVisible</settingName>
                                <settingValue>true</settingValue>
                            </setting>
                        </hostedProfileSettings>
                    </getHostedProfilePageRequest>';
        
        $response = send_xml_request($request);
                        
        $parsedresponse = parse_api_response($response);
        if ("Ok" == $parsedresponse->messages->resultCode) {
            $token = $parsedresponse->token;
            
        }


$request = '<?xml version="1.0" encoding="utf-8"?>
            <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                '.MerchantAuthenticationBlock().'
                <customerProfileId>'.$custProfID.'</customerProfileId>
            </getCustomerProfileRequest>';

$response = send_xml_request($request);

$parsedresponse = parse_api_response($response);
if ("Ok" == $parsedresponse->messages->resultCode) {
    $paymentProfiles = $parsedresponse->profile;
    $shipTo = $parsedresponse->profile->shipToList;//array of all available shipping addresses
    
}

if($error)
    echo "<span style='color:red'>$error</span>";

if(!$edit)
{

    echo'
        <form method="post" action="https://test.authorize.net/profile/manage"
        id="formAuthorizeNetPage" name="formAuthorizeNetPage">
        <input type="hidden" name="Token" value="'.$token.'"/>
        </form>';
    echo '<input name="btnEdit" type="button" id="btnEdit" onClick="clearInvalidCards(\''.$id.'\'); document.getElementById(\'formAuthorizeNetPage\').submit();" 
            value="Edit Payment Information &gt;&gt;" class="box">';
}

if($edit)
{
    echo "<h2>Choose a payment method for <br />order #$order_id</h2>
          <form action='' method='post' name='update_form' id='update_form' onsubmit='return validate();'>";
}


//////// select all invalid cards for this user ////////
    $paymentProfileIDs = "";
    $i = 0;
    foreach($paymentProfiles->paymentProfiles as $profile)
    {
        $paymentProfileIDs .= $profile->customerPaymentProfileId;
        if(++$i < count($paymentProfiles->paymentProfiles))
        {
            $paymentProfileIDs .= ",";
        }
    }
    $sql = "SELECT *
            FROM tbl_invalid_cards
            WHERE payment_profile_id IN ($paymentProfileIDs)";
    $result = mysql_query($sql);

    if( $result !== false ){
        //////// select all orders using any invalid cards ////////
        $invalCards = array();
        while($row = mysql_fetch_array($result))
        {
            $invalCards[]=$row["payment_profile_id"];
        }
    }
    
?>
<table width="400" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
        <tr class="entryTableHeader">
            <td colspan="2">Payment Methods</td>
        </tr>
        <?php 
            
            
            foreach($paymentProfiles->paymentProfiles as $profile)
            {
                
                $warn = "";
                $radioB = "";
                $type;
                $label;
                $number;
                $paymentProfileID = $profile->customerPaymentProfileId;
                if($profile->payment->creditCard)
                {
                    $type = 'Credit Card';
                    $label = 'Card';
                    $number = $profile->payment->creditCard->cardNumber;
                    
                }
                else
                {
                    $type = 'Bank';
                    $label = 'Account';
                    $number = $profile->payment->bankAccount->accountNumber;
                }
                
                if(in_array($paymentProfileID,$invalCards))
                {
                    $warn="Please Update";
                }
                
                if($edit)
                {
                    $radioB = "<input type='radio' name='payment[]' id='$paymentProfileID' value='$paymentProfileID' class='radio $type' onclick='ccv_view()'/>";
                }
                
                if(!$edit || strlen($warn) == 0) //only display this option if it doesn't have a warning
                {
                    echo "<tr>

                        <td colspan='2'>
                            <input type='hidden' id='customerPaymentProfileId' name='customerPaymentProfileId' value='$paymentProfileID' />
                            $radioB
                            $type
                        </td>
                    </tr>
                    <tr> 
                        <td width='150' class='label'>$label Number</td>
                        <td class='content'>
                            $number
                            <span style='color:red; margin-left: 15px;'>$warn</span>
                        </td>
                    </tr>";
                }
        
            }
        ?>
    </table>
    
<!--   <table width="400" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
        
        <tr class="entryTableHeader"> 
            <td colspan="2">Shipping Addresses</td>
        </tr>-->
        
        <?php 
        
            /*$index = 0;
            foreach($paymentProfiles->shipToList as $ships)
            {
                $number = $index + 1;
                $customerShippingAddressId = $ships->customerAddressId;
                
                echo "<tr>

                    <td>
                        <input type='hidden' id='customerShippingAddressId' name='customerShippingAddressId' value='$customerShippingAddressId' />
                        
                        Address $number
                    </td>
                </tr>
                <tr>
                    <td class='content'>

                        $ships->firstName 
                        $ships->lastName</br>
                        $ships->address</br>
                        $ships->city</br>
                        $ships->state</br>
                        $ships->zip</br>

                </td>
                </tr>";
        
                $index++;
            }
            
            $info = 'checked="checked"';
            if(!(isset($_SESSION['saveinfo']) && $_SESSION['saveinfo'] == 1))
            {
                $info = '';
            }*/
        ?>
<!--    </table>-->

    
<?php 

    
    
    $cardString = "";
    if( is_array($invalCards) ){
        foreach($invalCards as $card)
        {
            $cardString .= $card;
            if(++$i < count($invalCards))
            {
                $cardString .= ",";
            }
        }
    
        $sql = "SELECT o.od_id, 
                o.mem_id,
                m.id, 
                name, 
                od_date,
                od_status,
                SUM(pd_price * od_qty) AS od_amount,
                od_five_day,
                od_delivery_date
                FROM tbl_order o, tbl_order_item oi, tbl_product p, tbl_members m
                    WHERE oi.pd_id = p.pd_id and
                        o.od_id = oi.od_id and 
                        o.mem_id = m.id and
                        m.id = $id and
                        payment_profile_id IN ($cardString) and
                        o.od_status = 'New'
                    GROUP BY od_id
                    ORDER BY od_id DESC";
        $result = mysql_query($sql);
    }
    
    //////// display all orders which use an invalid card ////////
    if ($result && dbNumRows($result) > 0 && !$edit) {
?>
<p style="font-family: Arial, Helvetica, sans-serif; font-size: .7em; width: 95%;">The following orders are using an invalid payment method (Expired, Declined, etc.). 
    <br /> Click each order to select a new method of payment, or update the payment method using the button above.</p>
<table width="90%" border="0" align="center" cellpadding="2" cellspacing="0" class="text">
    <tr align="center" id="listTableHeader"> 
        <td>Order #</td>
        <td align="right">Amount</td>
        <td>Ordered</td>
        <td>Deliver</td>
        <td>Status</td>
    </tr>
    <?php
    $parentId = 0;
    
            $i = 0;

            while($row = dbFetchAssoc($result)) {
                    extract($row);
                    $name = $name;

                    if ($i%2) {
                            $class = 'row1';
                    } else {
                            $class = 'row2';
                    }

                    $i += 1;
    ?>
    <tr class="<?php echo $class; ?>"> 
        <td align="center"><a href="<?php echo $_SERVER['PHP_SELF']; ?>?edit=1&oid=<?php echo $od_id; ?>&id=<?php echo $id; ?>"><?php echo $od_id; ?></a></td>
        <td align="right"><?php echo displayAmount($od_amount); ?></td>
        <td align="center"><?php echo $od_date; ?></td>
        <td align="center"><?php echo $od_delivery_date; ?></td>
        <td align="center"><?php echo $od_status; ?></td>
    </tr>
    <?php
            } // end while
    
    ?>
</table>
<?php
        }
        else if($edit)
        {
            ?>
            <input type="submit" id="submit_B" name="submit_B" value="Update" />
            <input type="button" id="cancel_B" name="cancel_B" value="Cancel" onclick="javascript:location.href='index.php?id=<?php echo $id ?>'" />
            </form>
            <?php
        }
?>