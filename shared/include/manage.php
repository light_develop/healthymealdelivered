
<?php
if (isset($_SESSION['mname']) && $_SESSION['mname'] != "") {
    $orderId = (isset($_GET['oid']) && $_GET['oid'] != '' ? $_GET['oid'] : false);
    if ($orderId) 
    {
            require_once 'orderDetail.php';
    }
    else
    {
            require_once 'orderList.php';
    }
}
else
{
    echo 'Please log in to view this page.';
}
?>