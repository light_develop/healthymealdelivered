<?php
//session_start();
include_once '../library/config.php';

if (isset($_SESSION['mid']) && $_SESSION['mid'] != "")
{
    $mName = $_SESSION['mname'];
    $mId = $_SESSION['mid'];
    $orderId = $_POST['orderID'];
    $itemCount = $_POST['itemCount'];
    
    $reseller_id = $_SESSION["reseller_id"];
    $org_id = $_SESSION["organization_id"];
    
    // check if this order is ok to edit
    $sql = "SELECT od_id FROM tbl_order WHERE od_org_id = $org_id AND od_id = $orderId";
    $r = dbQuery($sql);
    if( mysql_num_rows($r) < 1 ){
        echo "Can not edit this record";
        exit;
    }
    
    $items = array();
    if(isset($_POST['updateOrderButton']))
    {//update
        
        $deleteSql = "DELETE FROM tbl_order_item
                WHERE od_id = $orderId";
        
        
        $itemsExist = false;
        $sql = "INSERT INTO tbl_order_item (od_id, pd_id, tea_id, od_qty) VALUES";
        for($x = 0; $x < $itemCount; $x ++)
        {
            $qty = $_POST['qty_'.$x];
            $shake = $_POST['shake_'.$x];
            $tea = $_POST['tea_'.$x];
            
            
            if((int)$qty > 0)
            {
                $sql .= "($orderId, $shake, $tea, $qty)";
                $itemsExist = true;
                
                if($x+1 < $itemCount) //add a comma after each row except the last
                {
                    $sql .= ", ";
                }
            }
        }
        
        if(!$itemsExist) // if all the quantities are 0, just cancel the order
        {
            $sql = "UPDATE tbl_order
                    SET od_status = 'Cancelled'
                    WHERE od_id = $orderId";
        }
        else
        {
            $result = dbQuery($deleteSql);
        }
        $result = dbQuery($sql);
        //echo $sql;
    } 
    else if (isset($_POST['cancelOrderButton'])) 
    {//cancel
        $sql = "UPDATE tbl_order
                SET od_status = 'Cancelled'
                WHERE od_id = $orderId";
        
        $result = dbQuery($sql);
    }
    else if(isset($_POST['deleteSelectedButton']))
    {
        $item = $_POST['select'];
        $count = count($item);
        
        $sql = "DELETE FROM tbl_order_item
                WHERE od_item_id IN (";
        
        for($x = 0; $x < $count; $x++)
        {
            $sql .= $item[$x];
            if ($x+1 < $count)
                $sql .= ", ";
            else
                $sql .= ")";
        }
        
        $result = dbQuery($sql);
    }
}
?>
