<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$categoryList    = getCategoryList();
$categoriesPerRow = 2;
$numCategory     = count($categoryList);
$columnWidth    = (int)(100 / $categoriesPerRow);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
    <tr>
        <td>
            <img height="300" style="margin-left: 20px;" src="../shared/images/MH900013321.jpg" />
        </td>
        <td>
            <table border="0" >
<?php 
if ($numCategory > 0) {
	$i = 0;
	for ($i; $i < $numCategory; $i++) {
		
		echo '<tr>';
		
		
		// we have $url, $image, $name, $price
		extract ($categoryList[$i]);
		
		echo "<td class='list' align=\"left\"><a href=\"$url\">$name</a></td>\r\n";
	

		
		echo '</tr>';
		
		
	}
	
	
} else {
?>
	<tr><td width="100%" align="center" valign="center">No categories yet</td></tr>
<?php	
}	
?>
            </table>
        </td>
    </tr>
</table>

<div id="countdown_container">
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.ajax('/shared/widgets/cut_off_countdown/index.php', {
            success : function(response) {
                $countdown_container = $('#countdown_container');
                $countdown_container.html(response);
                limit_stamp = $countdown_container.find('.time').attr('data-cut-off-time');
                limit_date = new Date( limit_stamp );
                cron_id = setInterval(updateRemainingTime, 1000);
            }
        });
    });

    var limit_stamp;
    var limit_date;

    function formateDatesDiff(date) {
        var hours   = date.getUTCHours();
        var minutes = date.getUTCMinutes(); if (minutes < 10) minutes = '0' + minutes;
        var seconds = date.getUTCSeconds(); if (seconds < 10) seconds = '0' + seconds;
        var str = hours + ':' + minutes + ':' + seconds;
        if (date.getUTCDate() > 1) {
            str = (date.getUTCDate()-1)+' days, '+str;
        }
        return str;
    }

    function updateRemainingTime() {
        var remaining = new Date(limit_date - new Date());
        var rem_str = formateDatesDiff(remaining);
        $('#countdown_container span.time').html(rem_str);

        if (rem_str == '0:00:00') {
            reloadTimer();
        }
    }

    function reloadTimer() {
        clearInterval(cron_id);
        $.ajax('/shared/widgets/cut_off_countdown/index.php', {
            success : function(response) {
                $countdown_container = $('#countdown_container');
                $countdown_container.html(response);
                limit_stamp = $countdown_container.find('.time').attr('data-cut-off-time');
                limit_date = new Date( limit_stamp );
                cron_id = setInterval(updateRemainingTime, 1000);
            }
        });
    }

    var cron_id;
</script>