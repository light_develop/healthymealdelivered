<?php
if (!defined('WEB_ROOT')) {
	exit;
}

// This section controls various elements related to login/logout status
// if $login==6 (logout has been selected) then clear the member id and name from session vars
$logstat6 = (isset($_POST['login']) && $_POST['login'] == '6') ? $_POST['login'] : 0;
if ($logstat6) {
	unset($_SESSION['mname']);
	unset($_SESSION['mid']);
}

$instruct = "Enter your email address and password to log in. You can also register at this point, or have your password reset.";
$uemail = (isset($_POST['uemail']) && $_POST['uemail'] != '') ? $_POST['uemail'] : 0;
$upass = (isset($_POST['upass']) && $_POST['upass'] != '') ? $_POST['upass'] : 0;
$upass = md5($upass);
$uremember = (isset($_POST['chkRemember']) && $_POST['chkRemember'] != '') ? $_POST['chkRemember'] : 0;
// echo "email:".$uemail."/pass: ".$upass;
if ($uemail && $upass) {
	$sql = "SELECT id, name, email, password, status FROM tbl_members WHERE email = '".$uemail."' AND password = '".$upass."' AND organization_id = ".$_SESSION["organization_id"];
	$result = dbQuery($sql);
	if (!dbNumRows($result)) {
		$instruct = "Incorrect Email Address and/or Password. Please try again, or register.";
	} else {
		$row = dbFetchAssoc($result);
        if($row['status'] == 1) {
            $_SESSION['mname'] = $row['name'];
            $_SESSION['mid'] = $row['id'];
            setcookie("userID", $row['id'], time()+21600, "/", ".".$_SERVER['HTTP_HOST']."");
            if ($uremember) {
                if (((isset($_COOKIE['autologin'])) && ($_COOKIE['autologin'] != $_SESSION['mid'])) || (!isset($_COOKIE['autologin']))) {
                        setcookie("autologin", $_SESSION['mid'], time()+3600, "/", ".$_domain", 0);
                }
            } else {
                setcookie("autologin", $_SESSION['mid'], time()-3600, "/", ".$_domain", 0);
                if (isset($_COOKIE['autologin'])) {
                        unset($_COOKIE['autologin']);
                }
            }
            header('location: index.php');
        } else {
            $instruct = "You are unable to log in because your account has been suspended. We apologize for any inconvenience.";
        }
	}
}

// set the default page title
$pageTitle = 'HealthyMealDelivered.com'; 

// if a product id is set add the product name
// to the page title but if the product id is not
// present check if a category id exist in the query string
// and add the category name to the page title
if (isset($_GET['p']) && (int)$_GET['p'] > 0) {
	$pdId = (int)$_GET['p'];
	$sql = "SELECT pd_name
			FROM tbl_product
			WHERE pd_id = $pdId";
	
	$result    = dbQuery($sql);
	$row       = dbFetchAssoc($result);
	$pageTitle = $row['pd_name'];
	
} else if (isset($_GET['c']) && (int)$_GET['c'] > 0) {
	$catId = (int)$_GET['c'];
	$sql = "SELECT cat_name
	        FROM tbl_category
			WHERE cat_id = $catId";

    $result    = dbQuery($sql);
	$row       = dbFetchAssoc($result);
	$pageTitle = $row['cat_name'];
}
// check for existence of $_SESSION['autologin']
// echo a line to setcookie, or clear cookie
//if (isset($_SESSION['autologin'])) {
//	if ($_SESSION['autologin'] == "1") {	
//		setcookie("autologin", $_SESSION['mid'], time()+3600, "/", ".larsoncomputerservices.com", 0);
//	} else {
//		setcookie("autologin", $_COOKIE['autologin'], time()-3600, "/", ".larsoncomputerservices.com", 0);
//	}
//	echo $preheader;
//}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title><?php echo $pageTitle; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../shared/include/shop.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="../shared/library/jquery-1.7.2.min.js"></script>

    <link type="text/css" href="../shared/library/jquery-ui/css/custom-theme/jquery-ui-1.8.23.custom.css" rel="Stylesheet" />	
    <script type="text/javascript" src="../shared/library/jquery-ui/js/jquery-1.8.0.min.js"></script>
    <script type="text/javascript" src="../shared/library/jquery-ui/js/jquery-ui-1.8.23.custom.min.js"></script>

    <script type="text/javascript" src="../shared/library/jquery.cookie.js"></script>

    <script src="../shared/library/jquery.tipTip.minified.js"></script>
    <link href="../shared/library/tipTip.css" rel="stylesheet" type="text/css">

    <script language="JavaScript" type="text/javascript" src="../shared/library/common.js"></script>
    <script language="JavaScript" type="text/javascript" src="../shared/library/popup.js"></script>

    <script>
        $(document).ready(function(){
           $("#tabs").tabs({ cookie: {} });
           $(".question").tipTip();
        });
    </script>
</head>
<body>