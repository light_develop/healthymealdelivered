<?php
include_once ("../shared/authorize/php_cim/XML/vars.php");
include_once ("../shared/authorize/php_cim/XML/util.php");

$id = $_SESSION['mid'];
$sql = "SELECT customerProfileId FROM tbl_members WHERE id =$id";

$result = dbQuery($sql);

$rows = mysql_fetch_array($result);
$custProfID = $rows['customerProfileId'];

$request = '<?xml version="1.0" encoding="utf-8"?>
                    <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                        '.MerchantAuthenticationBlock().'
                        <customerProfileId>'.$custProfID.'</customerProfileId>
                        <hostedProfileSettings>
                            <setting>
                                <settingName>hostedProfileReturnUrl</settingName>
                                <settingValue>http://'.BASE_URL.$_SESSION['organization'].'/index.php?id='.$id.'</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfileReturnUrlText</settingName>
                                <settingValue>Finished Editing</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfilePageBorderVisible</settingName>
                                <settingValue>true</settingValue>
                            </setting>
                        </hostedProfileSettings>
                    </getHostedProfilePageRequest>';
        
        $response = send_xml_request($request);
                        
        $parsedresponse = parse_api_response($response);
        if ("Ok" == $parsedresponse->messages->resultCode) {
            $token = $parsedresponse->token;
            
        }


$request = '<?xml version="1.0" encoding="utf-8"?>
            <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                '.MerchantAuthenticationBlock().'
                <customerProfileId>'.$custProfID.'</customerProfileId>
            </getCustomerProfileRequest>';

$response = send_xml_request($request);

$parsedresponse = parse_api_response($response);
if ("Ok" == $parsedresponse->messages->resultCode) {
    $paymentProfiles = $parsedresponse->profile;
    $shipTo = $parsedresponse->profile->shipToList;//array of all available shipping addresses
    
}



?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" onsubmit="return validate();" name="frmCheckout" id="frmCheckout">

    <table width="550" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
        <tr class="entryTableHeader"> 
            <td colspan="2">Choose Payment Method</td>
        </tr>
        <?php
            foreach($paymentProfiles->paymentProfiles as $profile)
            {
                $type;
                $label;
                $number;
                $paymentProfileID = $profile->customerPaymentProfileId;
                if($profile->payment->creditCard)
                {
                    $type = 'Credit Card';
                    $label = 'Card';
                    $number = $profile->payment->creditCard->cardNumber;
                    
                }
                else
                {
                    $type = 'Bank';
                    $label = 'Account';
                    $number = $profile->payment->bankAccount->accountNumber;
                }
                echo "<tr>

                    <td colspan='2'>
                        <input type='hidden' id='customerPaymentProfileId' name='customerPaymentProfileId' value='$paymentProfileID' />
                        <input type='radio' name='payment' id='$paymentProfileID' value='$number,$paymentProfileID,$label' class='radio $type' onclick='ccv_view()'/>
                        $type
                    </td>
                </tr>
                <tr> 
                    <td width='150' class='label'>$label Number</td>
                    <td class='content'>
                        $number
                        
                </td>
                </tr>";
        
        
            }
        ?>
        
    </table>
    
</form>