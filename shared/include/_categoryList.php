<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$categoryList    = getCategoryList();
$categoriesPerRow = 2;
$numCategory     = count($categoryList);
$columnWidth    = (int)(100 / $categoriesPerRow);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
    <tr>
        <td>
            <img height="300" style="margin-left: 20px;" src="../shared/images/MH900013321.jpg" />
        </td>
        <td>
            <table border="0" >
<?php 
if ($numCategory > 0) {
	$i = 0;
	for ($i; $i < $numCategory; $i++) {
		
		echo '<tr>';
		
		
		// we have $url, $image, $name, $price
		extract ($categoryList[$i]);
		
		echo "<td class='list' align=\"left\"><a href=\"$url\">$name</a></td>\r\n";
	
	
		
		echo '</tr>';
		
		
	}
	
	
} else {
?>
	<tr><td width="100%" align="center" valign="center">No categories yet</td></tr>
<?php	
}	
?>
            </table>
        </td>
    </tr>
</table>
