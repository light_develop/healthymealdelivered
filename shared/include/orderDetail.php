<?php

$org_id = $_SESSION["organization_id"];
$reseller_id = $_SESSION["reseller_id"];

$orderId = (isset($_GET['oid']) && $_GET['oid'] != '' ? $_GET['oid'] : false);
if($orderId)
{
    //////////////// Get the details for the order ////////////////
    $sql = "SELECT o.od_date, 
                   o.od_last_update, 
                   o.od_status, 
                   od_qty, pd_name, 
                   tea_name,
                   pd_price, 
                   oi.pd_id, 
                   oi.tea_id,
                   oi.od_item_id,
                   an_transaction_id,
                   od_delivery_date
	    FROM tbl_order_item oi, tbl_product p, tbl_teas t, tbl_order o
            WHERE oi.pd_id = p.pd_id and 
                  oi.tea_id = t.tea_id and 
                  oi.od_id = $orderId and 
                  o.od_id = $orderId and
                    o.od_org_id = $org_id
            ORDER BY o.od_id ASC";
    
    $result = dbQuery($sql);
    
    if(mysql_num_rows($result) < 1){
        echo "You do not have access to this order.";
        exit;
    }
    
    $orderedItems = array();
    while ($row = dbFetchAssoc($result)) {
            $orderedItems[] = $row;
    }
    
    
    $isPending = ($orderedItems[0]['od_status'] == 'New');
    if($isPending)
    {
        //////////////// Get all categories ////////////////
        $sql = "SELECT cat_id, cat_name
                FROM tbl_category
                WHERE reseller_id = $reseller_id
                ORDER BY cat_name ASC";
        $result = dbQuery($sql);
        $cats = array();
        $cat_ids = array();
        while ($row = dbFetchAssoc($result)) {
                $cats[] = $row;
                $cat_ids[] = $row["cat_id"];
        }

        $cat_ids = implode(",", $cat_ids);
        //////////////// Get all products ////////////////
        $sql = "SELECT pd_id, pd_name, cat_id
                FROM tbl_product
                WHERE pd_qty > 0 AND cat_id IN ($cat_ids)
                ORDER BY pd_name ASC";
        $result = dbQuery($sql);
        $products = array();
        while ($row = dbFetchAssoc($result)) {
                $products[] = $row;
        }

        //////////////// Get all teas ////////////////
        $sql = "SELECT t.tea_id, t.tea_name
                FROM tbl_teas t, tbl_reseller_tea rt
                WHERE rt.tea_id = t.tea_id AND rt.reseller_id = $reseller_id
                ORDER BY tea_name ASC";
        $result = dbQuery($sql);
        $teas = array();
        while ($row = dbFetchAssoc($result)) {
                $teas[] = $row;
        }
    }
    //////////////// Construct product select box ////////////////
    function buildProducts($name, $selected)
    {
        global $cats, $products;
        $productSelect = "<select id='$name' name='$name' >";
        foreach ($cats as $cat)
        {
            $catName = $cat['cat_name'];
            $productSelect .= "<optgroup label='$catName'>";
            
            foreach ($products as $product)
            {
                if($product['cat_id'] == $cat['cat_id'])
                {
                    $prodName = $product['pd_name'];
                    $prodId   = $product['pd_id'];
                    $selectText = '';
                    if($selected == $prodId)
                    {
                        $selectText = "selected = 'selected'";
                    }
                    $productSelect .= "<option value='$prodId' $selectText >$prodName</option>";
                }
            }
            
            $productSelect .= "</optgroup>";
        }
        $productSelect .= "</select>";
        
        return $productSelect;
    }
    
    function buildTeas($name, $selected)
    {
        global $teas;
        $teaSelect = "<select id='$name' name='$name' >";
        foreach ($teas as $tea)
        {
            $teaName = $tea['tea_name'];
            $teaId   = $tea['tea_id'];
            $selectText = '';
            if($selected == $teaId)
            {
                $selectText = "selected = 'selected'";
            }
            $teaSelect .= "<option value='$teaId' $selectText >$teaName</option>";
        }
        $teaSelect .= "</select>";
        
        return $teaSelect;
    }
?>
<script>
$(document).ready(function(){
    
    $("#updateOrderButton").click(function(event){
        event.preventDefault();
        var ok = true;
        var sum = 0;
        
        $(".qtyText").each(function() {
 
            //add only if the value is number
            if(!isNaN(this.value) && this.value.length!=0) {
                sum += parseFloat(this.value);
            }
 
        });
        
        if(sum < 1){
            if(confirm("You have set all quantities at 0 for this order. The order will be cancelled. Are you sure you want to cancel this order? (This can not be undone)")){
               ok = false; 
            }
        }
            
        if(ok && sum > 0){
            $.ajax({
                type: "POST",
                url: "../shared/include/updateOrder.php",
                data: $("#orderForm").serialize() + '&updateOrderButton=1',
                success: function(response){
                    $("#resultsDiv").html(response);
                    //location.reload();
                    $("#updated").fadeIn(200).delay(2000).fadeOut(500);
                }
            });
        }
        else if(!ok){
            $.ajax({
                    type: "POST",
                    url: "../shared/include/updateOrder.php",
                    data: $("#orderForm").serialize() + '&cancelOrderButton=1',
                    success: function(response){
                        //$("#resultsDiv").html(response);
                        location.reload();
                    }
                });
        }
    });
    
    $("#cancelOrderButton").click(function(event){
        event.preventDefault();
        if(confirm("Are you sure you want to cancel this order? (This can not be undone)")){

            $.ajax({
                type: "POST",
                url: "../shared/include/updateOrder.php",
                data: $("#orderForm").serialize() + '&cancelOrderButton=1',
                success: function(response){
                    //$("#resultsDiv").html(response);
                    location.reload();
                }
            });
        }
    });
    
    $("#deleteSelectedButton").click(function(event){
        event.preventDefault();
        var n = $("input:checked").length;
        var t = $(":checkbox").length;
        if(n < t){
        
            if(confirm("Are you sure you want to delete the selected item(s) from this order? (This can not be undone)")){

                $.ajax({
                    type: "POST",
                    url: "../shared/include/updateOrder.php",
                    data: $("#orderForm").serialize() + '&deleteSelectedButton=1',
                    success: function(response){
                        //$("#resultsDiv").html(response);
                        location.reload();
                    }
                });
            }
        }
        else{
            if(confirm("You have selected to delete all items from this order. The order will be cancelled. Are you sure you want to cancel this order? (This can not be undone)")){

                $.ajax({
                    type: "POST",
                    url: "../shared/include/updateOrder.php",
                    data: $("#orderForm").serialize() + '&cancelOrderButton=1',
                    success: function(response){
                        //$("#resultsDiv").html(response);
                        location.reload();
                    }
                });
            }
        }
    });
    
});
</script>
<form id="orderForm" name="orderForm" method="post" action=""><!-- ../shared/include/updateOrder.php -->
<table border="0" width="90%" align="center" cellpadding="5" cellspacing="0" class="detailTable">
    <tr> 
        <td colspan="2" align="center" id="listTableHeader">Order Detail</td>
    </tr>
    <tr> 
        <td width="150" class="label">Order Number</td>
        <td class="content"><?php echo $orderId; ?></td>
    </tr>
    <tr> 
        <td width="150" class="label">Order Date</td>
        <td class="content"><?php echo $orderedItems[0]['od_date']; ?></td>
    </tr>
    <tr> 
        <td width="150" class="label">Delivery Date</td>
        <td class="content"><?php echo $orderedItems[0]['od_delivery_date']; ?></td>
    </tr>
    <tr> 
        <td class="label">Status</td>
        <td class="content">
            <?php 
                echo $orderedItems[0]['od_status'];
                if($isPending && $orderedItems[0]['od_status'] == 'New')
                    echo ", Transaction Pending";
                else if($orderedItems[0]['od_status'] == 'Completed')
                    echo ", Payment method charged";
            ?>
        </td>
    </tr>
</table>
<br />
<table width="98%" border="0"  align="center" cellpadding="5" cellspacing="0" class="detailTable">
    <tr id="listTableHeader"> 
        <td colspan="<?php if($isPending){echo '5';}else{echo '4';} ?>">Ordered Items</td>
    </tr>
    <tr align="center" class="label">
        <?php
            if($isPending)
            {
                echo '<td>Select</td>';
            }
        ?>
    	<td width="15">Qty</td> 
        <td style="width: 356px">Item</td>
        <td style="width: 69px">Unit Price</td>
        <td>Total</td>
    </tr>
    <?php
    $numItem  = count($orderedItems);
    $subTotal = 0;
    for ($i = 0; $i < $numItem; $i++) {
            extract($orderedItems[$i]);
            $subTotal += $pd_price * $od_qty;
    ?>
        <tr class="content">
            <?php
                if($isPending)
                {
                    echo "<td>
                            <input type='checkbox' id='select_$i' name='select[]' value='$od_item_id' />
                        </td>";
                }
            ?>
            <td>
                <?php
                    if($isPending)
                    {
                        echo "<input class='qtyText' type='text' id='qty_$i' name='qty_$i' value='$od_qty' style='width: 20px;' />";
                    }
                    else
                    {
                        echo $od_qty;
                    }
                ?>
            </td> 
            <td style="width: 356px">
                <?php 
                if($isPending)
                {
                    echo buildProducts("shake_".$i, $pd_id);
                    echo " & ";
                    echo buildTeas("tea_".$i, $tea_id);
                }
                else
                {
                    echo "$pd_name Shake & $tea_name Tea"; 
                }
                ?>
            </td>
            <td align="right" style="width: 69px"><?php echo displayAmount($pd_price); ?></td>
            <td align="right"><?php echo displayAmount($od_qty * $pd_price); ?></td>
        </tr>
        <?php
    }
    
    ?>
    <tr class="content"> 
        <td colspan="<?php if($isPending){echo '4';}else{echo '3';} ?>" align="right">Total</td>
        <td align="right" style="width: 69px"><?php echo displayAmount($subTotal); ?></td>
    </tr>
</table>
<div style="text-align: right;">
<?php
    if($isPending)
    {
        echo '<div id="updated" style="float:left; margin-left: 20px; display:none;">Order Updated.</div>
            
              <div style="width:400px; margin: 10px auto 0;">
                <input type="submit" id="deleteSelectedButton" name="deleteSelectedButton" value="Delete Selected" style="float:left; margin-bottom: 10px;" />
                <input type="submit" id="updateOrderButton" name="updateOrderButton" value="Update Order" style="float:right;" />
                <input type="submit" id="cancelOrderButton" name="cancelOrderButton" value="Cancel Order" style="clear:left;float:left;" />
              </div>

              <input type="hidden" id="itemCount" name="itemCount" value="'.$numItem.'" />
              <input type="hidden" id="orderID" name="orderID" value="'.$orderId.'" />';
    }
}
else
{
    echo "Could not find order.";
};
?>
</div>
</form>
<div id="resultsDiv"></div>