<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$cartContent = getCartContent();
/*
This is what getCartContent() does
SELECT ct_id, ct.pd_id, ct_qty, pd_name, pd_price, pd_thumbnail, pd.cat_id
FROM tbl_cart ct, tbl_product pd, tbl_category cat
WHERE ct_session_id = '$sid' AND ct.pd_id = pd.pd_id AND cat.cat_id = pd.cat_id"; 
*/

$numItem = count($cartContent);	
?>
<table width="100%" border="1" cellspacing="0" cellpadding="2" id="minicart">
 <?php
if ($numItem > 0) {
?>
 <tr>
  <td colspan="2">Cart Content</td>
 </tr>
<?php
	$subTotal = 0;
	for ($i = 0; $i < $numItem; $i++) {
		extract($cartContent[$i]);
		$pd_name = "$ct_qty x $pd_name Shake & $tea_name Tea";
		$url = "index.php?c=$cat_id&p=$pd_id";
		
		$subTotal += $pd_price * $ct_qty;
?>
 <tr>
   <td><?php echo $pd_name; ?></td>
   
  <td width="30%" align="right"><?php echo displayAmount($ct_qty * $pd_price); ?></td>
 </tr>
<?php
	} // end while
?>
<!--  <tr><td align="right">Sub-total</td>
  <td width="30%" align="right"><?php echo displayAmount($subTotal); ?></td>
 </tr> -->
<!--  <tr><td align="right">Shipping</td>
  <td width="30%" align="right"><?php echo displayAmount($shopConfig['shippingCost']); ?></td>
 </tr> -->
  <tr><td align="right">Total</td>
  <td width="30%" align="right"><?php echo displayAmount($subTotal); ?></td>
<!--  <td width="30%" align="right"><?php echo displayAmount($subTotal + $shopConfig['shippingCost']); ?></td> -->
 </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
  <tr>
  <td colspan="2" align="center">
  <?php
	if (isset($_SESSION['mname']) && $_SESSION['mname'] != "") {
		echo "<a href=\"../shared/cart.php?action=view\"> Go To Shopping Cart</a>";
	} else {
		echo "<font color=\"red\">You must be logged in to go to the Shopping Cart.</font>";
	}
   ?>
   </td>
 </tr>  
<?php	
} else {
?>
  <tr><td colspan="2" align="center" valign="middle">Shopping Cart Is Empty</td></tr>
<?php
}
?> 
</table>
