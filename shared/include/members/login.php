<head>
<style type="text/css">
.style1 {
	text-align: left;
}
.style2 {
	text-align: left;
}
.style3 {
	text-align: center;
}
</style>

<?php if( $login == 1 ): ?>
<script>
    window.onload = function(){
        window.document.body.onload = toggleLogIn(); 
    };
document.onload = window.setTimeout(toggleLogIn, 500);
</script>
<?php endif; ?>
</head>
<!-- $login var holds 1 for Login, 2 for Register, 3 for the confirmation page, 4 for reset password
<!-- <form name="form1" method="post" action="signup_ac.php">-->
<?php
if (!defined('WEB_ROOT')) {
	exit;
}

	switch ($login) {
		case 1: // Login
			$chkChecked = "";
			$autoEmail = "";
			$autoPass = "";
			$btnState = "disabled='disabled'";
			if (isset($_COOKIE['autologin'])) {
				$chkChecked = "checked='checked'";
				$sql = "SELECT email, password FROM tbl_members WHERE id = '" . $_COOKIE['autologin']. "'";
				$result = dbQuery($sql);
				$row = dbFetchAssoc($result);
				$autoEmail = $row['email'];
				$autoPass = $row['password'];
				$btnState = "";
			}
			$display = "<table width='100%' cellspacing='2' cellpadding='2' border='0' style='padding-left: 15px;' >
                                    <tr>
                                        <td style='text-align: center; width: 45%; border-right: 1px solid #999; padding-right: 10px;' valign='middle'>

                                            A member account is required to place orders.<br/>
                                            
                                            <br/>
                                            Not a member yet? <br/>
                                            <form action='index.php' method='post' name='frmRR' id='frmRR'>
                                              <a href='javascript: registerReset(2)'>Click Here to Register</a>

                                              <br/><br/>
                                              Forgot your password?<br>
                                              <a href='javascript: registerReset(4)'>Reset Password</a><input name='login' id='login' type='hidden' />
                                            </form>

                                        </td>
                                        <td style='text-align: center; padding-left: 15px; padding-top: 25px;' valign='middle'>

                                            Already have an account?

                                            <div style='font-weight: bold; font-size: 18px;margin-bottom: 15px;'>Log in below</div>
                                            <form action='index.php' method='post' name='frmLogin' id='frmLogin' style='margin-bottom: 0;'>
                                              <table width='80%' align='center' cellspacing='5' cellpadding='2' border='0' bordercolor='gray'>
                                                  <tr>
                                                      <td class='style2'>
                                                          <strong>Email</strong>
                                                          <div style='font-size:xx-small'>Enter the email address you registered with</div>
                                                          <input name='uemail' id='uemail' type='text' value='$autoEmail' onkeyup='toggleLogIn()' onchange='toggleLogIn()' />
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td class='style2'>
                                                          <strong>Password</strong>
                                                          <div style='font-size:xx-small'>Enter your password</div>
                                                          <input name='upass' id='upass' type='password' value='$autoPass' onkeyup='toggleLogIn()' onchange='toggleLogIn()' />
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td align='center' class='style3'>
                                                          <input type='submit' name='btnLogIn' id='btnLogIn' value='Log In' $btnState>
                                                          <input name='login' type='hidden' value='1' />
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td colspan='2' align='center'>
                                                          <div style='font-family:Arial, Helvetica, sans-serif;font-size:x-small'>
                                                              <input name='chkRemember' type='checkbox' $chkChecked>(Remember me on this computer)</div>
                                                      </td>
                                                  </tr>
                                              </table>
                                            </form>
                                        </td>
                                    </tr>
                                </table>";
		break;
		case 2: // Register
			$display =  "<form action='index.php' method='post' name='frmLogin' id='frmLogin'>
                  <table width='90%' cellspacing='5' cellpadding='2' border='0' bordercolor='gray'>
                      <tr><td colspan='2'>Register as a new HMD Online customer. You must register to place an order.</td></tr>
                      <tr><td colspan='2'>Check '<b>Confirm Here</b>' next to your preferred confirmation method. We will send you a message to verify your account.</td></tr>
                      <tr><td colspan='2'>
                              <table width='80%' align='center' cellspacing='1' cellpadding='2' border='0' bordercolor='gray'>
                                  <tr>
                                      <td class='style1'><strong>Name</strong></td>
                                      <td class='style2'>
                                          <div style='font-size:xx-small'>Enter First and Last name</div><input name='uname' id='uname' type='text' onkeyup='toggleLogIn()' />
                                      </td>
                                  </tr> 
                                  <tr>
                                      <td class='style1'><strong>Email</strong></td>
                                      <td class='style2' style='white-space: nowrap;'>
                                          <div style='font-size:xx-small'>Confirmation will be sent to this address</div>
                                          <input name='uemail' type='text' onkeyup='toggleLogIn()' />
                                          <input type='radio' name='confirm' id='confirmEmail' value='1' checked='checked' /><label for='confirmEmail'>Confirm Here</label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class='style1'><strong>Phone *</strong></td>
                                      <td class='style2'>
                                          <div style='font-size:xx-small'>Cell Phone required for confirmation</div>
                                          <input name='uphone' type='text' onkeyup='toggleLogIn()' />
                                          <input type='radio' name='confirm' id='confirmPhone' value='2' /><label for='confirmPhone'>Confirm Here</label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td class='style1'><strong>Password</strong></td>
                                      <td class='style2'><div style='font-size:xx-small'>Enter a password to create account</div><input name='upass1' id='upass1' type='text' onkeyup='toggleLogIn()' />
                                  </tr>
                                  <tr>
                                      <td class='style1'><strong>&nbsp;</strong></td>
                                      <td class='style2'>
                                          <div style='font-size:xx-small'>Re-enter password (must match)</div><input name='upass2' id='upass2' type='text' onkeyup='toggleLogIn()' />
                                          <input name='hidDuplicateEmail' type='hidden' />
                                      </td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr><td>&nbsp;<input name='login' type='hidden' value='5' /></td>
                          <td class='style3'><input type='button' name='btnLogIn' id='btnLogIn'  value='Register Now' disabled='disabled' onclick='checkForDuplicateEmail()'></td>
                      </tr>
                      <tr>
                        <td colspan='2'>* If we are delayed due to accident or vehicle breakdown you will receive a text message so make sure the phone number you supply is accurate</td>
                      </tr>
                  </table>
              </form>";
		break;
		case 3: // Confirmation
			// confirm that got from link
			$confirm= $_GET['confirm'];
			$tbl_name1 = "tbl_pendmembers";
			
			// Retrieve data from table where row that match this passkey
			$sql1 = "SELECT * FROM tbl_pendmembers WHERE confirm_code ='$confirm'";
			$result1=dbQuery($sql1);
			
			// If successfully queried

			if($result1){
			
				// Count how many row have this passkey, should be only one
				$count = mysql_num_rows($result1);
				
				// if found this passkey in our database, retrieve data from table "temp_members_db"
				if($count==1){
					$rows = mysql_fetch_array($result1);
					$name = $rows['name'];
					$email = $rows['email'];
					$phone = $rows['phone'];
					$password = $rows['password'];
                                        $custProfID = $rows['customerProfileId'];
					$tbl_name2 = "tbl_members";
                                        
                                        $org_id = $_SESSION["organization_id"];
					
					// Insert data that retrieves from "temp_members_db" into table "registered_members"
					$sql2 = "INSERT INTO $tbl_name2(organization_id, name, email, phone, password, sdate, customerProfileId) VALUES($org_id, '$name', '$email', '$phone', '$password', NOW(), '$custProfID')";
					$result2 = dbQuery($sql2);
				} else {
					// if not found passkey, display message "Wrong Confirmation code"
					$message = "Wrong Confirmation code";
				}
			
			// if successfully moved data from table"temp_members_db" to table "registered_members" displays message
			// "Your account has been activated" and don't forget to delete confirmation code from table "temp_members_db"
			if($result2){
				$message = "Your account has been activated";
			
				// Delete information of this user from table "temp_members_db" that has this passkey
				$sql3 = "DELETE FROM $tbl_name1 WHERE confirm_code = '$confirm'";
				$result3 = dbQuery($sql3);
			}
			
			}
			$display = "<form action=\"index.php\" method=\"post\" name=\"frmLogin\" id=\"frmLogin\">
				<table width=\"90%\" cellspacing=\"10\" cellpadding=\"10\" border=\"0\" bordercolor=\"gray\">
					<tr><td colspan=\"2\">&nbsp;</td></tr>
					<tr><td colspan=\"2\">
						<table width=\"80%\" align=\"center\" cellspacing=\"5\" cellpadding=\"2\" border=\"0\" bordercolor=\"gray\">
							<tr><td colspan=\"2\" class=\"style1\"><strong>$message</strong></td>
							</tr>
						</table>
					</td>
					</tr>
					<tr><td>&nbsp;<input name=\"login\" type=\"hidden\" value=\"1\" /></td>
						<td class=\"style3\"><input type=\"submit\" name=\"btnLogIn\" id=\"btnLogIn\" value=\"Log In\"></td></tr>
				</table>
				</form>";
		break;
		case 4: // Reset password
				// if $rpass is set at this point, it means the user has clicked the reset password link in their email
			$changed = (isset($_POST['changed']) && $_POST['changed'] != '') ? $_POST['changed'] : 0;
			if ($changed) {
				$newpass = md5($_POST['newpass']);
				$rcode = $_POST['rcode'];
 				$sql = "UPDATE tbl_members SET password = '$newpass', passreset = '' WHERE passreset = '$rcode'";
	 			$result = dbQuery($sql);
				$instruct = "Your login password has been changed. Use any Login button to log in to your account.";
				$entryFields = "";
				$action = "<td colspan=\"2\" align=\"center\" class=\"style3\"><input type=\"submit\" name=\"btnLogIn\" id=\"btnLogIn\"  value=\"Login\"><br>
						<input name=\"login\" type=\"hidden\" value=\"1\" /></td></tr>";
			} else if ($rpass) {
				$sql = "SELECT * FROM tbl_members WHERE passreset = '$rpass'";
				$result = dbQuery($sql);
				$rows = dbNumRows($result);
				if ($rows) {
					$instruct = "Enter the new password you would like to use in the box below, then press the Save button.<br><br>";
					$instruct .= "If you wish to cancel the password change, simply make another selection on the left to do something else.";
					$entryFields = "<tr id=\"passRow\" name=\"passRow\"><td class=\"style1\"><strong>Password</strong></td>
						<td class=\"style2\"><div style=\"font-size:xx-small\">Enter your new password</div><input name=\"newpass\" type=\"password\" /></td>
					</tr>";
					$action = "<td colspan=\"2\" align=\"center\" class=\"style3\"><input type=\"submit\" name=\"btnLogIn\" id=\"btnLogIn\"  value=\"Save New Password\"><br>
							<input name=\"login\" type=\"hidden\" value=\"4\" /><input name=\"rcode\" type=\"hidden\" value=\"$rpass\" /><input name=\"changed\" type=\"hidden\" value=\"1\" /></td></tr>";
				} else {
					$instruct = "Invalid or expired password reset code";
					$entryFields = "";
					$action = "<td colspan=\"2\" align=\"center\" class=\"style3\"><input type=\"submit\" name=\"btnLogIn\" id=\"btnLogIn\"  value=\"Login\"><br>
						<input name=\"login\" type=\"hidden\" value=\"1\" /></td></tr>";
				}
			} else {
				$instruct = "Enter the email address your account is registered with, then press the Submit button.<br><br>";
				$instruct .= "An Password Reset link will be sent to your email account, and you will need to click on that link to return here, and complete the reset process.";
				$entryFields = "<tr id=\"emailRow\" name=\"emailRow\"><td class=\"style1\"><strong>Email</strong></td>
					<td class=\"style2\"><div style=\"font-size:xx-small\">Enter the email address you registered with</div><input name=\"uemail\" id=\"uemail\" type=\"text\" /></td>
					</tr>";
				$action = "<td colspan=\"2\" align=\"center\" class=\"style3\"><input type=\"button\" name=\"btnLogIn\" id=\"btnLogIn\"  value=\"Submit\" onclick=\"checkForValidEmail()\"><br>
						<input name=\"login\" type=\"hidden\" value=\"7\" /><input name=\"hidDuplicateEmail\" type=\"hidden\" /></td></tr>";
			}
			$display = "<form action=\"index.php\" method=\"post\" name=\"frmLogin\" id=\"frmLogin\">
			<table width=\"90%\" cellspacing=\"2\" cellpadding=\"2\" border=\"0\" bordercolor=\"gray\">
				<tr><td colspan=\"2\">".$instruct."</td></tr>
				<tr><td colspan=\"2\">
					<table width=\"80%\" align=\"center\" cellspacing=\"5\" cellpadding=\"2\" border=\"0\" bordercolor=\"gray\">";
						$display .= $entryFields;
			$display .= "</table>
				</td>
				</tr>
				<tr>" . $action . "</table></form>";
		break;
		case 5: 
                        
                        // send email to new member
			// table name
			$tbl_name = "tbl_pendmembers";
			
			// Random confirmation code
			$confirm_code = md5(uniqid(rand()));
			 
			// values sent from form
			$name = $_POST['uname'];
			$email = strtolower($_POST['uemail']);
			$phone = $_POST['uphone'];
			$userpass = md5($_POST['upass1']);
                        
                        $confirmMethod = intval($_POST["confirm"]);
			
                        // check phone number
                        $phone_check = preg_match("/\([0-9]{3}\) [0-9]{3}-[0-9]{4}/", $phone);
                        
                        if( $phone_check === 0 ){
                            $response = "Please enter a valid 10 digit phone number. (Also make sure javascript is enabled in your browser before trying again)";
                        } else if( $phone_check === false ){
                            $response = "An error occurred. Please try again.";
                        } else {
                        
                            // check blacklist
                            $sql = "SELECT email FROM tbl_blacklist WHERE email = '$email'";
                            $result = mysql_query($sql);

                            if(!mysql_num_rows($result) > 0)
                            {
                                //create Authorize.Net customer profile
                                ///////////////////////////////////////////////////////////////////

                                $description       = $name;
                                $merchantCustomerId= getMerchantCustomerId($email);

                                include_once ("../shared/authorize/php_cim/XML/vars.php");
                                include_once ("../shared/authorize/php_cim/XML/util.php");

                                //build xml to post
                                $content =
                                        "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
                                        "<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
                                        MerchantAuthenticationBlock().
                                        "<profile>".
                                        "<merchantCustomerId>$merchantCustomerId</merchantCustomerId>". // Your own identifier for the customer.
                                        "<description>$description</description>".
                                        "<email>" . $email . "</email>".
                                        "</profile>".
                                        "</createCustomerProfileRequest>";


                                $response = send_xml_request($content);

                                $parsedresponse = parse_api_response($response);
                                if ("Ok" == $parsedresponse->messages->resultCode) {
                                    $custProfID = $parsedresponse->customerProfileId;
                                }

                                //$parsedresponse = parse_api_response($response);
                                //if ("Ok" == $parsedresponse->messages->resultCode) {
                                //        echo "customerProfileId <b>"
                                //                . htmlspecialchars($parsedresponse->customerProfileId)
                                //                . "</b> was successfully created.<br><br>";
                                //}

                                ///////////////////////////////////////////////////////////////////

                                $org_id = $_SESSION["organization_id"];
                                // Insert data into database
                                $sql = "INSERT INTO $tbl_name(confirm_code, name, email, phone, password, customerProfileId, organization_id) VALUES('$confirm_code', '$name', '$email', '$phone', '$userpass', '$custProfID', $org_id)";
                                $result = dbQuery($sql);




                                // if successfully inserted data into database, send confirmation link to email
                                if($result){
                                    
                                    if( $confirmMethod === 1 ){
                                    
                                        // ---------------- SEND MAIL FORM ----------------

                                        // send e-mail to ...
                                        $to=$email;

                                        // Your subject
                                        $comp = $shopConfig["name"];
                                        $comp_email = $shopConfig["email"];
                                        $subject = "Your $comp confirmation link here";

                                        // From
                                        $header = "from: $comp <$comp_email>";

                                        // Your message
                                        $message = "Your Comfirmation link \r\n";
                                        $message .= "Click on this link to activate your account \r\n";
                                        $message .= "http://".BASE_URL.$_SESSION['organization']."/?confirm=$confirm_code";

                                        // send email
                                        $sentmail = mail($to,$subject,$message,$header);
                                        
                                    } else if( $confirmMethod === 2 ){
                                        
                                        // send SMS confirmation
                                        
                                        // remove anything but numbers from phone
                                        $phone = str_replace(array("(",")"," ","-"), "", $phone );
                                        
                                        $phone = "+1".$phone;
                                        
                                        $store = $shopConfig["name"];
                                        
                                        try {
                                            $client = new SoapClient('http://sms2.cdyne.com/sms.svc?wsdl');
                                            $postback = "http://".BASE_URL."shared/library/receiveMessages.php";
                                            $license = getCDYNELicense();
                                            $param = array(
                                                    'PhoneNumber' => $phone,
                                                    'LicenseKey' => "$license",
                                                    'Message' => "Reply YES to verify your $store account. If you didn't request an account, ignore this message.",
                                                    'StatusPostBackURL' => "$postback"
                                            );
                            
                                            $message = urlencode("Reply YES to verify your $store account. If you didn't request an account, ignore this message.");
                                            $senttext = true;
//                                            $result = file_get_contents("http://sms2.cdyne.com/sms.svc/SimpleSMSsendWithPostback?PhoneNumber=$phone&Message=$message&LicenseKey=$license&StatusPostBackURL=$postback");
                                            $result = $client->SimpleSMSsendWithPostback($param);

                                            $parsed = new SimpleXMLElement($result);

                                            if( $parsed->SMSError != "NoError" ){
                                                $senttext = true;
                                            } else {
                                                $senttext = true;
                                            }
                                        } catch (Exception $e){
                                            $senttext = true;
                                        }
                                    }

                                } else {
                                        $response = "Your email address was not found in our database";
                                }

                                // if your email successfully sent
                                if($sentmail){
                                        $response = "A Confirmation link Has Been Sent To Your Email Address. \r\n";
                                        $response .= "You must click on that link to complete the registration process. \r\n";
                                        $response .= "After completing the registration, you may proceed to log in.";
                                } else if($senttext) {
                                    $response = "A Confirmation Text Has Been Sent To Your Phone Number. \r\n";
                                    $response .= "You must reply to that message to complete the registration process. \r\n";
                                    $response .= "After completing the registration, you may proceed to log in.";
                                } else {
                                        $response = "Cannot send Confirmation link to your e-mail address or phone number";
                                }
                            }
                            else
                            {
                                $response = "Unable to add an account. (BL)";
                            }
                        }
			$display = "<form action=\"index.php\" method=\"post\" name=\"frmLogin\" id=\"frmLogin\">
				<table width=\"90%\" cellspacing=\"10\" cellpadding=\"10\" border=\"0\" bordercolor=\"gray\">
					<tr><td colspan=\"2\">&nbsp;</td></tr>
					<tr><td colspan=\"2\">
						<table width=\"80%\" align=\"center\" cellspacing=\"5\" cellpadding=\"2\" border=\"0\" bordercolor=\"gray\">
							<tr><td colspan=\"2\" class=\"style1\"><strong>$response</strong></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
				</form>";
		break;
		case 6: // logout
			$display = "<form action=\"index.php\" method=\"post\" name=\"frmLogin\" id=\"frmLogin\">
			<table width=\"90%\" cellspacing=\"5\" cellpadding=\"2\" border=\"0\" bordercolor=\"gray\">
				<tr><td colspan=\"2\" align=\"center\">You have been logged out.</td></tr>
				<tr><td>&nbsp;</td>
					<td class=\"style3\"><input type=\"submit\" name=\"btnLogIn\" id=\"btnLogIn\"  value=\"Continue\"></td></tr>
			</table>
			</form>";

		break;
		case 7: // send password reset request to existing member
			// table name
			$tbl_name = "tbl_members";
			
			// Random reset code
			$reset_code = md5(uniqid(rand()));
			 
			// values sent from form
			$email = strtolower($_POST['uemail']);
			
			// Insert data into database
 			$sql = "UPDATE tbl_members SET passreset = '$reset_code' WHERE email = '$uemail'";
 			$result = dbQuery($sql);
			
			// if successfully inserted data into database, send confirmation link to email
			if($result){
			
				// ---------------- SEND MAIL FORM ----------------
				
				// send e-mail to ...
				$to=$email;
				
				// Your subject
                                $comp = $shopConfig["name"];
                                $comp_email = $shopConfig["email"];
				$subject = "Your $comp password reset link here";
				
				// From
				$header = "from: $comp <$comp_email>";
				
				// Your message
				$message = "Your Password Reset link \r\n";
				$message .= "Click on this link to reset your password \r\n";
				$message .= "http://".BASE_URL.$_SESSION['organization']."/?rpass=$reset_code";
				
				// send email
				$sentmail = mail($to,$subject,$message,$header);
			
			} else {
				$response = "Your email address was not found in our database";
			}
			
			// if your email successfully sent
			if($sentmail){
				$response = "A Password Reset link Has Been Sent To Your Email Address. \r\n";
				$response .= "You must click on that link to complete the password reset process. \r\n";
				$response .= "After completing the reset procedure, you may proceed to log in.";
			} else {
				$response = "Cannot send Confirmation link to your e-mail address";
			}
			$display = "<form action=\"index.php\" method=\"post\" name=\"frmLogin\" id=\"frmLogin\">
				<table width=\"90%\" cellspacing=\"10\" cellpadding=\"10\" border=\"0\" bordercolor=\"gray\">
					<tr><td colspan=\"2\">&nbsp;</td></tr>
					<tr><td colspan=\"2\">
						<table width=\"80%\" align=\"center\" cellspacing=\"5\" cellpadding=\"2\" border=\"0\" bordercolor=\"gray\">
							<tr><td colspan=\"2\" class=\"style1\"><strong>$response</strong></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
				</form>";
		break;
	} // end select
	echo $display;
?>
<script src="../jquery.js" ></script>
<script src="../jquery.maskedinput.min.js" ></script>
<script>
    $(document).ready(function(){
        $("[name='uphone']").mask("(999) 999-9999");
        
        function check(){
            
            var name = $("#uname").val().length;
            var email = $("[name='uemail']").val().length;
            var phone = $("[name='uphone']").val();
            var pword = $("#upass1").val().length;
            var pconf = $("#upass2").val().length;
            
            var re = /\([0-9]{3}\) [0-9]{3}-[0-9]{4}/; // match a full number with mask
            
            if( phone.match(re) === null ){
                phone = false;
            } else {
                phone = true;
            }
            
            if( !name || !email || !phone || !pword || !pconf ){
                $("#btnLogIn").attr("disabled","disabled");
            } else {
                $("#btnLogIn").removeAttr("disabled");
            }
            
        }
        
        $("input").keyup(check).keydown(check);
    });
</script>