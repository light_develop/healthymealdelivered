<!-- $catId = Category ID for selected Shakes -->

<head>
<style type="text/css">
.style1 {
	text-align: left;
}
.style2 {
	text-align: right;
}
.style3 {
	text-align: center;
}
</style>
</head>

<?php
$reseller_id = $_SESSION["reseller_id"];
$memberId = intval($_COOKIE['userID']);

$sql = dbQuery('SELECT orders_left FROM tbl_shop_config');
$orderLimit = dbFetchRow($sql);
$authorizeButtons = "";
$sql = "SELECT p.cat_id, c.cat_id, cat_description, pd_id, pd_description, authorize_button
		FROM tbl_product p, tbl_category c
		WHERE p.cat_id = $catId AND c.cat_id = $catId AND c.reseller_id = $reseller_id
		ORDER BY pd_description";
$result     = dbQuery($sql);
$row = array();
$slist = "<option value=\"0\" selected=\"selected\">Select Shake</option>";
while ($row = dbFetchAssoc($result)) {
	$pd_id = $row['pd_id'];
	$pd_description = $row['pd_description'];

    if($memberId) {
        $authorizeButtons .= "<div class='auth_buttons' id='authorize_button_$pd_id' style='display:none'>".$row['authorize_button']."</div>";
    }

	$shake = $row['cat_description'];
	$slist .= "<option value=\"$pd_id\">$pd_description</option>";
}
$sql = "SELECT t.tea_id, t.tea_name
		FROM tbl_teas t, tbl_reseller_tea rt
                WHERE rt.tea_id = t.tea_id AND rt.reseller_id = $reseller_id
		ORDER BY t.tea_name";
$result     = dbQuery($sql);
$row = array();
$tlist = "<option value=\"0\" selected=\"selected\">Select Tea</option>";
while ($row = dbFetchAssoc($result)) {
	$tea_id = $row['tea_id'];
	$tea_name = $row['tea_name'];
	$tlist .= "<option value=\"$tea_id\">$tea_name</option>";
}
?>

<table width="90%" cellspacing="10" cellpadding="10" border="0" bordercolor="gray">
	<tr><td colspan="2">Select a Shake from the first pull-down menu and an accompanying Tea from the second pull-down menu. Any combination of Shake-and-Tea can be chosen.</td></tr>
	<tr><td colspan="2">
		<table width="80%" align="center" cellspacing="5" cellpadding="5" border="0" bordercolor="gray">
			<tr><td class="style1"><strong><?php echo $shake; ?></strong></td>
				<td class="style2">
					<select name="shakes" id="shakes" onchange="setATCButton()">
					<?php echo $slist; ?>
					</select>
				</td>
			</tr>
			<tr><td class="style1">Select a Tea</td>
				<td class="style2">
					<select name="teas" id="teas" onchange="setATCButton()">
					<?php echo $tlist; ?>
					</select>
				</td>
			</tr>
		</table>
	</td>
	</tr>
	<tr><td align="right"><label id="lblATC">(You must select both a Shake and a Tea to complete an order)</label></td>
		<td class="style3"><input type="button" name="btnAddToCart" id="btnAddToCart" data-state="<?php echo $orderLimit[0]==0 ? 'false' : 'true'; ?>"  value="Add To Cart" onClick="addSTToCart()" disabled="disabled"></td></tr>

</table>
<input type="hidden" id="cat" name="cat" value="<?php echo $catId; ?>"/>
<?php
    echo $authorizeButtons;
?>
