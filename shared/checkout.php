<?php
require_once 'library/config.php';
require_once 'library/cart-functions.php';
require_once 'library/checkout-functions.php';
include_once ("authorize/php_cim/XML/vars.php");
include_once ("authorize/php_cim/XML/util.php");

if (isCartEmpty()) {
	// the shopping cart is still empty
	// so checkout is not allowed
	header('Location: cart.php');
} else if (isset($_GET['step']) && (int)$_GET['step'] > 0 && (int)$_GET['step'] <= 3) {
	$step = (int)$_GET['step'];

	$includeFile = '';
	if ($step == 1) {
		$includeFile = 'shippingAndPaymentInfo.php';
		$pageTitle   = 'Checkout - Step 1 of 2';
	} else if ($step == 2) {
		$includeFile = 'checkoutConfirmation.php';
		$pageTitle   = 'Checkout - Step 2 of 2';
	} else if ($step == 3) {
                $custProfID = $_SESSION['custProfID'];
                $memberID = $_SESSION['mid'];
                $customerPaymentProfileId = $_POST['customerPaymentProfileId'];
                //$customerShippingAddressId = $_POST['customerShippingAddressId'];
                $ccv = $_POST['ccv'];
                $amount = $_POST['amount'];
                $saveinfo = 0;
                if(isset($_POST['saveinfo']))
                {
                    $saveinfo = $_POST['saveinfo'];
                    $sql = "UPDATE tbl_members SET saveInfo = 1 WHERE id = $memberID";
                    $result = dbQuery($sql);
                }
                else
                {
                    $sql = "UPDATE tbl_members SET saveInfo = 0";
                    $result = dbQuery($sql);
                }
                
                $cartContent = getCartContent();
                $numItem     = count($cartContent);
                extract($cartContent);
                
                // save this order (and order for each day in 5day order)
                $orderId  = saveOrder($customerPaymentProfileId);
                
                $request = '<?xml version="1.0" encoding="utf-8"?>
                <createCustomerProfileTransactionRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                        '.MerchantAuthenticationBlock().'
                        <transaction>
                                <profileTransAuthOnly>
                                        <amount>'.$amount.'</amount>';
                                        
                        foreach($cartContent as $item)
                        {
                            $name = substr(htmlspecialchars ($item["pd_name"].' '.$item["tea_name"]),0,30);
                            $ct_id = $item['ct_id'];
                            $ct_qty = $item['ct_qty'];
                            $pd_price = $item['pd_price'];
                            
                            $request .= "<lineItems>
                                                <itemId>$ct_id</itemId>
                                                <name>$name</name>
                                                <quantity>$ct_qty</quantity>
                                                <unitPrice>$pd_price</unitPrice>
                                        </lineItems>";
                        }
                                        
                            $request .= "<customerProfileId>$custProfID</customerProfileId>
                                        <customerPaymentProfileId>$customerPaymentProfileId</customerPaymentProfileId>
                                        <order>
                                                <invoiceNumber>$orderId</invoiceNumber>
                                        </order>
                                        <recurringBilling>false</recurringBilling>";
                            
                            if(isset($ccv) && strlen($ccv) > 0)
                            {
                                 $request .= "<cardCode>$ccv</cardCode>";
                            }
                            
                            
                            $request .= "</profileTransAuthOnly>
                        </transaction>
                </createCustomerProfileTransactionRequest>";
                            
                
                //submit transaction for this order 
                $response = send_xml_request($request);

                $parsedresponse = parse_api_response($response);
                if ("Ok" == $parsedresponse->messages->resultCode) {
                    $creditResult = explode(',',$parsedresponse->directResponse);
                    
                    //summary of result parts can be found at http://www.authorize.net/support/AIM_guide.pdf on page 38
                    
                    if($creditResult[0] > 1)
                    {
                        deleteOrder($orderId);
                        //there was an issue processing the card
                        printf("<script>location.href='checkout.php?step=1&error=$creditResult[3]';</script>");
                        
                    }
                    else
                    {
                        //add Auth.Net transactionID to order table for this order
                       updateOrderId($orderId, $creditResult[6]);
                       
                       // then remove the ordered items from cart
                        for ($i = 0; $i < $numItem; $i++) {
                                $sql = "DELETE FROM tbl_cart
                                        WHERE ct_id = {$cartContent[$i]['ct_id']}";
                                $result = dbQuery($sql);					
                        }
                
                       //saveOrderItems($orderId); 
                       
/*                       $chkFive = (isset($_SESSION['chkFive'])) ? 1 : 0;
                       if(!$saveinfo && !$chkFive) 
                       {
                           //DELETE CUSTOMER INFORMATION IF NOT CHECKED and it's not a five day order
                           
                           $request = '<?xml version="1.0" encoding="utf-8"?>
                                <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                                    '.MerchantAuthenticationBlock().'
                                    <customerProfileId>'.$custProfID.'</customerProfileId>
                                </getCustomerProfileRequest>';

                            $response = send_xml_request($request);

                            $parsedresponse = parse_api_response($response);
                            if ("Ok" == $parsedresponse->messages->resultCode) {
                                $paymentProfiles = $parsedresponse->profile->paymentProfiles;
                                $shipTo = $parsedresponse->profile->shipToList;
                            }
                            
                            foreach($paymentProfiles as $profile)
                            {
                                $paymentProfileID = $profile->customerPaymentProfileId;
                                
                                $request = "<?xml version='1.0' encoding='utf-8'?>
                                                <deleteCustomerPaymentProfileRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
                                                    ".MerchantAuthenticationBlock()."
                                                    <customerProfileId>$custProfID</customerProfileId>
                                                    <customerPaymentProfileId>$paymentProfileID</customerPaymentProfileId>
                                                </deleteCustomerPaymentProfileRequest>";
                                $response = send_xml_request($request);
                            }
                            
                            foreach($shipTo as $ship)
                            {
                                $customerShippingAddressId = $ship->customerAddressId;
                                
                                $request = "<?xml version='1.0' encoding='utf-8'?>
                                            <deleteCustomerShippingAddressRequest xmlns='AnetApi/xml/v1/schema/AnetApiSchema.xsd'>
                                                ".MerchantAuthenticationBlock()."
                                                <customerProfileId>$custProfID</customerProfileId>
                                                <customerAddressId>$customerShippingAddressId</customerAddressId>
                                            </deleteCustomerShippingAddressRequest>";
                                $response = send_xml_request($request);
                            }
                           
                       }*/
                       
                        echo "<div style='margin: auto; margin-top: 100px; width: 500px; text-align: center;'>
                                <h1>Order Complete.</h1>
                                <h2>Thank you for your order.</h2>
                                <div>You will receive a confirmation email shortly.</div>
                                <div>You can also review your order by clicking \"Manage Orders.\"</div>
                                <br /><br /><br />
                                <div>If you are not redirected shortly, click <a href='../".$_SESSION['organization']."/index.php'>here</a></div>
                            </div>";
                        
                        printf("<script>setTimeout(function() {location.href='../".$_SESSION['organization']."/index.php';},5000);</script>");
                        exit;
                    }
                }
                
                
                    
                    
                    
                    
                    
		//$orderAmount = getOrderAmount($orderId);
		
		//$_SESSION['orderId'] = $orderId;
		
		// our next action depends on the payment method
		// if the payment method is COD then show the 
		// success page but when paypal is selected
		// send the order details to paypal
//		if ($_POST['hidPaymentMethod'] == 'cod') {
//			header('Location: success.php');
//			exit;
//		} else {
//			$includeFile = 'paypal/payment.php';	
//
//		}
	}
} else {
	// missing or invalid step number, just redirect
	header('Location: index.php');
}

require_once 'include/header.php';
?>
<script language="JavaScript" type="text/javascript" src="library/checkout.js"></script>
<script language="javascript">
timeTimer = setTimeout("updateDeliveryInfo()", 1000);
</script>
<?php
require_once "include/$includeFile";
require_once 'include/footer.php';
?>