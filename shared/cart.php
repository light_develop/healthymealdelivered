<?php
require_once 'library/config.php';
require_once 'library/cart-functions.php';

if (!$_SESSION) session_start();
$_SESSION['last_cart_url'] = $_SERVER['REQUEST_URI'];

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : 'view';

switch ($action) {
    case 'add' :
        addToCart();
        break;
    case 'update' :
        updateCart();
        break;
    case 'delete' :
        deleteFromCart();
        break;
    case 'view' :
}

$cartContent = getCartContent();
$numItem = count($cartContent);

$pageTitle = 'Shopping Cart';
require_once 'include/header.php';

// show the error message ( if we have any )
displayError();
$itemName = "";
if ($numItem > 0) {
    ?>

    <form action="<?php echo $_SERVER['PHP_SELF'] . "?action=update"; ?>" method="post" name="frmCart" id="frmCart">
        <table width="780" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
            <tr class="entryTableHeader"> 
                <td width="480" align="center">Item</td>
                <td width="75" align="center">Unit Price</td>
                <td width="75" align="center">Quantity</td>
                <td width="75" align="center">Total</td>
                <td width="75" align="center">&nbsp;</td>
            </tr>
            <?php
            $subTotal = 0;
            for ($i = 0; $i < $numItem; $i++) {
                extract($cartContent[$i]);
                //$productUrl = "index.php?c=$cat_id&p=$pd_id";
                $subTotal += $pd_price * $ct_qty;
				
				$itemName .= $pd_name . " Shake & " . $tea_name . " Tea /  " . $ct_qty . " qt.<br />";
				
                ?>
                <tr class="content"> 
                    <td>
                        <!--<a href="<?php //echo $productUrl; ?>">-->
                            <?php echo $pd_name . " Shake & " . $tea_name . " Tea"; ?>
                        <!--</a>-->
                    </td>
                    <td align="right"><?php echo displayAmount($pd_price); ?></td>
                    <td width="75"><input name="txtQty[]" type="text" id="txtQty[]" size="5" value="<?php echo $ct_qty; ?>" class="box" onKeyUp="checkNumber(this); toggleCartButtons()">
                        <input name="hidCartId[]" type="hidden" value="<?php echo $ct_id; ?>">
                        <input name="hidProductId[]" type="hidden" value="<?php echo $pd_id; ?>">
                    </td>
                    <td align="right"><?php echo displayAmount($pd_price * $ct_qty); ?></td>
                    <td width="75" align="center"> <input name="btnDelete" type="button" id="btnDelete" value="Delete" onClick="window.location.href='<?php echo $_SERVER['PHP_SELF'] . "?action=delete&cid=$ct_id"; ?>';" class="box"> 
                    </td>
                </tr>
                <?php
            }


            $days = 0;
            $cdate = strtotime("this friday");
            $today = strtotime("today");
            
            if( date("m/d/y", $today) === date("m/d/y", $cdate) ){
                // if today is friday, set end of repeat to next friday
                $cdate = strtotime(date("m/d/y", $cdate) . " +7 days");
            }
            
            //$difference = $cdate - $today;
            $delivery = strtotime(getDeliveryDate());
            $difference = $cdate - $delivery;

            if ($difference < 0) {
                $difference = 0;
            }//delivery is friday or later
            $days = floor($difference / 60 / 60 / 24);
            
            // calculate repeat dates
            $start_date = date("D n/j", strtotime(date("Y-m-d", $delivery) . "+1 day" )); // +1 day to exclude the original order being placed
            $end_date = date("D n/j", $cdate);
            
            // if today is friday, saturday, or sunday, order will repeat all next week

            if (isset($_SESSION['chkFive'])) {
                $_SESSION['days'] = $days;
                $chkdFive = "checked = 'checked'";
                $subFour = $subTotal * $days;
                $addFour = "(adds $" . $subFour . ")";
            } else {
                unset($_SESSION['days']);
                $chkdFive = "";
                $subFour = 0;
                $addFour = "";
            }
            if (isset($_SESSION['chkCoupon'])) {
                $chkdCoupon = "checked = 'checked'";
                //$subCoupon = 6.5;
                //$subtractCoupon = "(subtracts $6.50)";
                $subCoupon = 0;
                $subtractCoupon = "";
            } else {
                $chkdCoupon = "";
                $subCoupon = 0;
                $subtractCoupon = "";
            }
            $chkdCoupon = (isset($_SESSION['chkCoupon'])) ? "checked='checked'" : "";
            $disabled = '';
            $color = '#000';
            
            $blurb = "Order will be repeated <b>$start_date</b> to <b>$end_date</b> (+$days orders)<br /><br /> You can review, change or cancel these additional orders later using the Manage Orders page found on the home page.";
            if ($days < 1) {
                $disabled = 'disabled = "disabled"';
                $chkdFive = '';
                $color = '#777';
                $blurb = "Not available";
            }
            
            // alter blurb if any repeat days are holidays
            $todayDate = date('Y-m-d', $today);
            $fridayDate = date('Y-m-d', $cdate);
            
            $sql = "SELECT *
                    FROM tbl_holiday
                    WHERE date > '$todayDate' AND date <= '$fridayDate' AND
                        active = 1
                    ORDER BY date ASC";

            $result = dbQuery($sql);

            while ($rows[] = mysql_fetch_assoc($result));
            array_pop($rows);

            $notice = '';
            if (count($rows) > 0) {
                if (count($rows) >= $days) // if all available delivery days are holidays
                {
                    $disabled = 'disabled = "disabled"';
                    $chkdFive = '';
                    $color = '#777';
                    //$blurb = "This option is only available if there are additional available delivery days before this Friday.";
                    $blurb = "Option not available at this time.";
                }
                //////////////////////////build string////////////////////////////////
                if ($days >= 1) {
                    $days = "";

                    if (count($rows) > 1)
                        $verb = " are holidays, so an order will not be placed for those days.";
                    else
                        $verb = " is a holiday, so an order will not be placed for that day.";

                    for ($x = 0; $x < count($rows); $x++) {
                        $days .= date("l", strtotime($rows[$x]["date"]));

                        if (count($rows) > 2 && $x + 1 < count($rows)) {
                            if ($x + 2 == count($rows)) {
                                $days .= ", and ";
                            } else {
                                $days .= ", ";
                            }
                        } elseif (count($rows) > 1 && $x + 1 < count($rows)) {
                            $days .= " and ";
                        }
                    }

                    $notice = $days . $verb;


                }
            }
            
            ?>
            <tr class="content">
                <td colspan="3" align="right" style="padding: 15px 5px;">
                    <input type="checkbox" name="chkFive" id="chkFive" value="<?php echo $days; ?>" <?php echo $chkdFive . " " . $disabled; ?> onclick="toggleCartButtons()">
                    <span style="font-size: 16px; color: <?php echo $color; ?>">Would you like to order for the entire week?</span>
                    <!--<span class="question" title="<?php echo $blurb ?>">?</span>-->
                    <div><?php echo $blurb; ?></div>
                    <?php
                    echo "<div style='color: red;'>" . $notice . "</div>";
                    ?>
                </td>
                <td>
                    <div style="font-size:xx-small;text-align:center"><?php echo $addFour; ?></div>
                </td>
                <td width="75" align="center">&nbsp;</td>
            </tr>
            <tr class="content"> 
                <td colspan="3" align="right"><input type="checkbox" name="chkCoupon" <?php echo $chkdCoupon; ?> onclick="toggleCartButtons()"> Apply Coupon Code <input type="text" size="10" name="txtCoupon" id="txtCoupon"></td>
                <td><div style="font-size:xx-small;text-align:center"><?php echo $subtractCoupon; ?></div></td>
                <td width="75" align="center">&nbsp;</td>
            </tr>
           <!-- <tr class="content"> 
              <td colspan="3" align="right">Shipping </td>
             <td align="right"><?php echo displayAmount($shopConfig['shippingCost']); ?></td>
             <td width="75" align="center">&nbsp;</td>
            </tr> -->
            <tr class="content"> 
                <td colspan="3" align="right">Order Total </td>
                <td align="right"><?php echo displayAmount($subTotal + $subFour - $subCoupon); ?></td>
                <td width="75" align="center">&nbsp;</td>
            </tr>
            <tr class="content">
                <td colspan="5" align="center">
                    <strong>This order will be delivered on <?php echo date("m/d/Y", $delivery); ?></strong>
                </td>
            </tr>
            <tr class="content"> 
                <td colspan="4" align="right">
                    <input name="btnUpdate" type="submit" id="btnUpdate" value="Update Cart" class="box" style="color:red;visibility:hidden"></td>
                <td width="75" align="center">
                    &nbsp;</td>
            </tr>
        </table>
    </form>
    <?php
} else {
    ?>
    <p>&nbsp;</p><table width="550" border="0" align="center" cellpadding="10" cellspacing="0">
        <tr>
            <td><p align="center">You shopping cart is empty</p>
                <p>If you find you are unable to add anything to your cart, please ensure that 
                    your internet browser has cookies enabled and that any other security software 
                    is not blocking your shopping session.</p></td>
        </tr>
    </table>
    <?php
}

$shoppingReturnUrl = isset($_SESSION['shop_return_url']) ? $_SESSION['shop_return_url'] : 'index.php';
?>
<script language="JavaScript" type="text/javascript" src="library/checkout.js"></script>


<table width="550" border="0" align="center" cellpadding="10" cellspacing="0">
    <tr align="center"> 
        <td><input name="btnContinue" type="button" id="btnContinue" value="&lt;&lt; Continue Shopping" onClick="window.location.href='<?php echo $shoppingReturnUrl; ?>';" class="box"></td><td>
<!--			<form action='/expresscheckout.php' METHOD='POST'>-->
<!--				<input type="hidden" name="amount" value="--><?php //echo displayAmount($subTotal + $subFour - $subCoupon); ?><!--" />-->
<!--				<input type="hidden" name="item_name" value="--><?//= $itemName ?><!--" />-->
<!--				<input type='image' name='submit' src='https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif' border='0' align='top' alt='Check out with PayPal'/>-->
<!--			</form>-->
<!--		</td>-->
			<form action="/paypal/buy.php" method="post">
				<input type="hidden" name="amount" value="<?php echo $subTotal + $subFour - $subCoupon; ?>"/>
				<input type="hidden" name="item_name" value="<?= $itemName ?>"/>
				<input type="hidden" name="chkFive" value="<?php echo($chkdFive ? $days : ""); ?>">
				<input type="hidden" name="chkdCoupon" value="<?php echo($chkdCoupon ? 1 : 0); ?>">
				<input type="hidden" name="landingPage" value="login">
				<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		<td>
			<form action="/paypal/buy.php" method="post">
				<input type="hidden" name="amount" value="<?php echo $subTotal + $subFour - $subCoupon; ?>"/>
				<input type="hidden" name="item_name" value="<?= $itemName ?>"/>
				<input type="hidden" name="chkFive" value="<?php echo($chkdFive ? $days : ""); ?>">
				<input type="hidden" name="chkdCoupon" value="<?php echo($chkdCoupon ? 1 : 0); ?>">
				<input type="hidden" name="landingPage" value="billing">
				<input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
			</form>
		</td>
<?php
/*
if ($numItem > 0) {


    $id = $_SESSION['mid'];
    $sql1 = "SELECT * FROM tbl_members WHERE id =$id";

    $result1 = dbQuery($sql1);

    $rows = mysql_fetch_array($result1);
    $custProfID = $rows['customerProfileId'];
    $email = $rows['email'];
    $merchantCustomerId = getMerchantCustomerId($email);

    include_once ("../shared/authorize/php_cim/XML/vars.php");
    include_once ("../shared/authorize/php_cim/XML/util.php");

    $request = '<?xml version="1.0" encoding="utf-8"?>
                    <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                        ' . MerchantAuthenticationBlock() . '
                        <customerProfileId>' . $custProfID . '</customerProfileId>
                        <hostedProfileSettings>
                            <setting>
                                <settingName>hostedProfileReturnUrl</settingName>
                                <settingValue>http://'.BASE_URL.'shared/checkout.php?step=1</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfileReturnUrlText</settingName>
                                <settingValue>Continue to checkout</settingValue>
                            </setting>
                            <setting>
                                <settingName>hostedProfilePageBorderVisible</settingName>
                                <settingValue>true</settingValue>
                            </setting>
                        </hostedProfileSettings>
                    </getHostedProfilePageRequest>';

    $response = send_xml_request($request);

    $parsedresponse = parse_api_response($response);
    if ("Ok" == $parsedresponse->messages->resultCode) {
        $token = $parsedresponse->token;

        $_SESSION['custProfID'] = $custProfID;
    }


    $request = '<?xml version="1.0" encoding="utf-8"?>
            <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                ' . MerchantAuthenticationBlock() . '
                <customerProfileId>' . $custProfID . '</customerProfileId>
            </getCustomerProfileRequest>';

    $response = send_xml_request($request);

    $parsedresponse = parse_api_response($response);
    if ("Ok" == $parsedresponse->messages->resultCode) {
        $paymentProfile = $parsedresponse->profile->paymentProfiles;
    }

    if (isset($paymentProfile) && !empty($paymentProfile)) {
        //customer has already entered payment information
        $_SESSION['saveinfo'] = 1;
        //show button to chose info
        echo'<td>
            <input name="btnCheckout" type="button" id="btnCheckout" onClick="window.location.href=\'checkout.php?step=1\';" value="Choose Payment Information &gt;&gt;" class="box">            
            </td>';
    } else {
        //customer has not yet entered payment info
        //show button to add info
        echo'<td>
            <form method="post" action="https://test.authorize.net/profile/manage"
            id="formAuthorizeNetPage" style="display:none;">
            <input type="hidden" name="Token" value="' . $token . '"/>
            </form>
            <input name="btnCheckout" type="button" id="btnCheckout" onClick="document.getElementById(\'formAuthorizeNetPage\').submit();" value="Enter Payment Information &gt;&gt;" class="box">            
            </td>'; //https://secure.authorize.net/profile/manage
    }





    //<input name="btnCheckout" type="button" id="btnCheckout" value="Proceed To Checkout &gt;&gt;" onClick="window.location.href='checkout.php?step=1';" class="box">
    //</td>
}*/
?>  
    </tr>
</table>

<style>
    #countdown_container { margin-bottom: 10px; text-align: center; }
    #countdown_container p {
        margin: 0;
    }
    #countdown_container p:first-child {
        font-weight: bold;
        color: #008000;
    }
    #countdown_container p + p {
        margin-top: 5px;
    }
</style>
<div id="countdown_container">
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $.ajax('/shared/widgets/cut_off_countdown/index.php', {
            success : function(response) {
                $countdown_container = $('#countdown_container');
                $countdown_container.html(response);
                limit_stamp = $countdown_container.find('.time').attr('data-cut-off-time');
                limit_date = new Date( limit_stamp );
                cron_id = setInterval(updateRemainingTime, 1000);
            }
        });
    });

    var limit_stamp;
    var limit_date;

    function formateDatesDiff(date) {
        var hours   = date.getUTCHours();
        var minutes = date.getUTCMinutes(); if (minutes < 10) minutes = '0' + minutes;
        var seconds = date.getUTCSeconds(); if (seconds < 10) seconds = '0' + seconds;
        var str = hours + ':' + minutes + ':' + seconds;
        if (date.getUTCDate() > 1) {
            str = (date.getUTCDate()-1)+' days, '+str;
        }
        return str;
    }

    function updateRemainingTime() {
        var remaining = new Date(limit_date - new Date());
        var rem_str = formateDatesDiff(remaining);
        $('#countdown_container span.time').html(rem_str);

        if (rem_str == '0:00:00') {
            reloadTimer();
        }
    }

    function reloadTimer() {
        clearInterval(cron_id);
        $.ajax('/shared/widgets/cut_off_countdown/index.php', {
            success : function(response) {
                $countdown_container = $('#countdown_container');
                $countdown_container.html(response);
                limit_stamp = $countdown_container.find('.time').attr('data-cut-off-time');
                limit_date = new Date( limit_stamp );
                cron_id = setInterval(updateRemainingTime, 1000);
            }
        });
    }

    var cron_id;
</script>

<?php require_once 'include/footer.php'; ?>

<? if (isset($_SESSION) && isset($_SESSION['THANKS_FOR_ORDER']) && $_SESSION['THANKS_FOR_ORDER']) { ?>
    <script type="text/javascript">
        alert('Thank you for your order!'); 
    </script>
    <? 
    unset($_SESSION['THANKS_FOR_ORDER']);
    ?>
<? } ?>