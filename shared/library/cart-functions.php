<?php
//require_once 'config.php';

/*********************************************************
*                 SHOPPING CART FUNCTIONS 
*********************************************************/
 
function addToCart()
{
	// make sure the product id exist
	if (isset($_GET['p']) && (int)$_GET['p'] > 0) {
		$productId = (int)$_GET['p'];
		$teaId = (int)$_GET['t'];
	} else {
		header('Location: index.php');
	}
	
	// does the product exist ?
	$sql = "SELECT pd_id, pd_qty
	        FROM tbl_product
			WHERE pd_id = $productId";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) != 1) {
		// the product doesn't exist
		header('Location: ../shared/cart.php');
	} 
//	else {
//		// how many of this product we
//		// have in stock
//		$row = dbFetchAssoc($result);
//		$currentStock = $row['pd_qty'];
//
//		if ($currentStock == 0) {
//			// we no longer have this product in stock
//			// show the error message
//			setError('The product you requested is no longer in stock');
//			header('Location: ../shared/cart.php');
//			exit;
//		}
//
//	}		
	
	// current session id
	$sid = session_id();
	
	// check if the Shake/Tea combo is already
	// in cart table for this session
	$sql = "SELECT pd_id
	        FROM tbl_cart
			WHERE pd_id = $productId AND tea_id = $teaId AND ct_session_id = '$sid'";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 0) {
		// put the Shake in cart table
		$sql = "INSERT INTO tbl_cart (pd_id, tea_id, ct_qty, ct_session_id, ct_date)
				VALUES ($productId, $teaId, 1, '$sid', NOW())";
		$result = dbQuery($sql);
	} else {
		// update Shake quantity in cart table
		$sql = "UPDATE tbl_cart 
		        SET ct_qty = ct_qty + 1
				WHERE ct_session_id = '$sid' AND pd_id = $productId AND tea_id = $teaId";		
				
		$result = dbQuery($sql);		
	}	
/*	// Now check if the Tea is already
	// in cart table for this session
	$sql = "SELECT pd_id
	        FROM tbl_cart
			WHERE pd_id = $teaId AND ct_session_id = '$sid'";
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 0) {
		// put the Tea in cart table
		$sql = "INSERT INTO tbl_cart (pd_id, ct_qty, ct_session_id, ct_date)
				VALUES ($teaId, 1, '$sid', NOW())";
		$result = dbQuery($sql);
	} else {
		// update Tea quantity in cart table
		$sql = "UPDATE tbl_cart 
		        SET ct_qty = ct_qty + 1
				WHERE ct_session_id = '$sid' AND pd_id = $teaId";		
				
		$result = dbQuery($sql);		
	}	
*/
	// an extra job for us here is to remove abandoned carts.
	// right now the best option is to call this function here
	deleteAbandonedCart();
	
	header('Location: ' . $_SESSION['shop_return_url']);				
}

/*
	Get all item in current session
	from shopping cart table
*/
function getCartContent()
{
	$cartContent = array();

	$sid = session_id();
	$sql = "SELECT ct_id, ct.pd_id, ct.tea_id, ct_qty, pd_name, pd_price, pd_thumbnail, pd.cat_id, tea_name
			FROM tbl_cart ct, tbl_product pd, tbl_category cat, tbl_teas tea
			WHERE ct_session_id = '$sid' AND ct.pd_id = pd.pd_id AND cat.cat_id = pd.cat_id AND ct.tea_id = tea.tea_id";
	
	$result = dbQuery($sql);
	
	while ($row = dbFetchAssoc($result)) {
		if ($row['pd_thumbnail']) {
			$row['pd_thumbnail'] = WEB_ROOT . 'shared/images/product/' . $row['pd_thumbnail'];
		} else {
			$row['pd_thumbnail'] = WEB_ROOT . 'shared/images/no-image-small.png';
		}
		$cartContent[] = $row;
	}
	
	return $cartContent;
}

/*
	Remove an item from the cart
*/
function deleteFromCart($cartId = 0, $redirect = true)
{
	if (!$cartId && isset($_GET['cid']) && (int)$_GET['cid'] > 0) {
		$cartId = (int)$_GET['cid'];
	}

	if ($cartId) {	
		$sql  = "DELETE FROM tbl_cart
				 WHERE ct_id = $cartId";

		$result = dbQuery($sql);
	}
	
    if ($redirect) {
        header('Location: cart.php');	
    }
}

/*
	Update item quantity in shopping cart
*/
function updateCart($redirect = 1)
{
	$cartId     = $_POST['hidCartId'];
	$productId  = $_POST['hidProductId'];
	$itemQty    = $_POST['txtQty'];
	$numItem    = count($itemQty);
	$numDeleted = 0;
	$notice     = '';
        
        //$TEST = "cartID: ".$cartId." productId: ".$productId." itemQty: ".$itemQty." numItem: ".$numItem;
	
	for ($i = 0; $i < $numItem; $i++) {
		$newQty = (int)$itemQty[$i];
		if ($newQty < 1) {
			// remove this item from shopping cart
			deleteFromCart($cartId[$i]);	
			$numDeleted += 1;
		} else {
//			// check current stock
//			$sql = "SELECT pd_name, pd_qty
//			        FROM tbl_product 
//					WHERE pd_id = {$productId[$i]}";
//			$result = dbQuery($sql);
//			$row    = dbFetchAssoc($result);
//			
//			if ($newQty > $row['pd_qty']) {
//				// we only have this much in stock
//				$newQty = $row['pd_qty'];
//
//				// if the customer put more than
//				// we have in stock, give a notice
//				if ($row['pd_qty'] > 0) {
//					setError('The quantity you have requested is more than we currently have in stock. The number available is indicated in the &quot;Quantity&quot; box. ');
//				} else {
//					// the product is no longer in stock
//					setError('Sorry, but the product you want (' . $row['pd_name'] . ') is no longer in stock');
//
//					// remove this item from shopping cart
//					deleteFromCart($cartId[$i]);	
//					$numDeleted += 1;					
//				}
//			} 
							
			// update product quantity
			$sql = "UPDATE tbl_cart
					SET ct_qty = $newQty
					WHERE ct_id = {$cartId[$i]}";
				
			dbQuery($sql);
			
			// 5-day order?
			if (isset($_POST['chkFive'])) {
				$_SESSION['chkFive'] = 1;
			} else {
				unset($_SESSION['chkFive']);
			}
			
			// Coupon ??
			if (isset($_POST['chkCoupon'])) {
				$_SESSION['chkCoupon'] = 1;
			} else {
				unset($_SESSION['chkCoupon']);
			}
		}
	} // end for
	
        if($redirect)
        {
            if ($numDeleted == $numItem) {
		// if all item deleted return to the last page that
		// the customer visited before going to shopping cart
		header("Location: $returnUrl" . $_SESSION['shop_return_url']);
            } else {
		header('Location: ../shared/cart.php');	
            }
            exit;
        }
}

function isCartEmpty()
{
	$isEmpty = false;
	
	$sid = session_id();
	$sql = "SELECT ct_id
			FROM tbl_cart ct
			WHERE ct_session_id = '$sid'";
	
	$result = dbQuery($sql);
	
	if (dbNumRows($result) == 0) {
		$isEmpty = true;
	}	
	
	return $isEmpty;
}

function emptyCart() {
	$sid = session_id();
	$sql = "DELETE FROM tbl_cart
			WHERE ct_session_id = '$sid'";
	
	$result = dbQuery($sql);
}

/*
	Delete all cart entries older than one day
*/
function deleteAbandonedCart()
{
	$yesterday = date('Y-m-d H:i:s', mktime(0,0,0, date('m'), date('d') - 1, date('Y')));
	$sql = "DELETE FROM tbl_cart
	        WHERE ct_date < '$yesterday'";
	dbQuery($sql);		
}

?>