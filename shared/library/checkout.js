function setPaymentInfo(isChecked)
{
	with (window.document.frmCheckout) {
		if (isChecked) {
			txtPaymentFirstName.value  = txtShippingFirstName.value;
			txtPaymentLastName.value   = txtShippingLastName.value;
			txtPaymentAddress1.value   = txtShippingAddress1.value;
			txtPaymentAddress2.value   = txtShippingAddress2.value;
			txtPaymentPhone.value      = txtShippingPhone.value;
			txtPaymentState.value      = txtShippingState.value;			
			txtPaymentCity.value       = txtShippingCity.value;
			txtPaymentPostalCode.value = txtShippingPostalCode.value;
			
			txtPaymentFirstName.readOnly  = true;
			txtPaymentLastName.readOnly   = true;
			txtPaymentAddress1.readOnly   = true;
			txtPaymentAddress2.readOnly   = true;
			txtPaymentPhone.readOnly      = true;
			txtPaymentState.readOnly      = true;			
			txtPaymentCity.readOnly       = true;
			txtPaymentPostalCode.readOnly = true;			
		} else {
			txtPaymentFirstName.readOnly  = false;
			txtPaymentLastName.readOnly   = false;
			txtPaymentAddress1.readOnly   = false;
			txtPaymentAddress2.readOnly   = false;
			txtPaymentPhone.readOnly      = false;
			txtPaymentState.readOnly      = false;			
			txtPaymentCity.readOnly       = false;
			txtPaymentPostalCode.readOnly = false;			
		}
	}
}


function checkCCPaymentInfo()
{
	with (window.document.frmCheckout) {
		var good = hasEntry(txtBillingName) && 
					hasEntry(txtBillingAddress) && 
					hasEntry(txtBillingCity) && 
					hasEntry(txtBillingZipCode) &&
					hasEntry(txtShowNumber) &&
					hasEntry(txtCardCVV);
		if (good) {
			btnProceed.disabled="";
		} else {
			btnProceed.disabled="disabled";
		}
	}
}

function copyCardNumber() {
	with (window.document.frmCheckout) {
		hidCardNumber.value = txtShowNumber.value;
	}
}