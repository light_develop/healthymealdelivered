function showPopup(text, modal, timeout, on_close_callback) {
    modal = modal || false;
    timeout = timeout || false;
    on_close_callback = on_close_callback || undefined;

    $('.popup .text_container').html(text);
    $('.popup').fadeIn(250);
    if (modal) {
        $('.modal_popup_background').fadeIn(250);
    }
    if (timeout) {
        setTimeout(closePopup, timeout);
    }
    popup_is_opened = true;
    
    _on_close_callback = on_close_callback;
}

var popup_is_opened = false;
var _on_close_callback = undefined;

function closePopup() {
    $('.popup').fadeOut(250);
    $('.modal_popup_background').fadeOut(250);
    popup_is_opened = false;
    if (_on_close_callback) {
        _on_close_callback();
    }
    _on_close_callback = undefined;
    return false;
}

$(document).ready(function() {
    $('.popup .close_btn').click(closePopup);
});