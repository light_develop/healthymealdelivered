<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/shared/library/config.php');

date_default_timezone_set(getTimeZone());
$dbHost = 'localhost';
$dbUser = 'nutrition';
$dbPass = 'AqqT&V;ZKTT#';
$dbName = 'nutrition';

$dbConn = mysql_connect ($dbHost, $dbUser, $dbPass) or die ('MySQL connect failed. ' . mysql_error());
mysql_select_db($dbName) or die('Cannot select database. ' . mysql_error());



// Orders placed after 8:00 am TODAY will be processed for TOMORROW delivery
// Unless TOMORROW is a holiday, then orders will be processed for the next available day
// Set up various related items
$today = date("l");
$deliveryDay = "";
$timeNow = date("H:i:s");
$tomorrow = mktime(0, 0, 0, date("m"), date("d")+1, date("y"));
$tomorrow = date("l", $tomorrow);
$cutoff = getCutOffTime();

if ((($timeNow > $cutoff) && ($today == "Friday")) || ($today == "Saturday") || ($today == "Sunday")) {
	switch ($today) {
		case "Friday":
			$monday = mktime(0, 0, 0, date("m"), date("d")+3, date("y"));
			$deliveryDay = date("l", $monday);
			$deliveryDate = date("m/d/y", $monday);
			$fontColor = "red";
		break;
		case "Saturday":
			$monday = mktime(0, 0, 0, date("m"), date("d")+2, date("y"));
			$deliveryDay = date("l", $monday);
			$deliveryDate = date("m/d/y", $monday);
			$fontColor = "red";
		break;
		case "Sunday":
			$monday = mktime(0, 0, 0, date("m"), date("d")+1, date("y"));
			$deliveryDay = date("l", $monday);
			$deliveryDate = date("m/d/y", $monday);
			$fontColor = "red";
		break;
	}			
} elseif ($timeNow < $cutoff) {
	$deliveryDay = "today";
	$deliveryDate = date("m/d/y", mktime(0, 0, 0, date("m"), date("d"), date("y")));
	$fontColor = "green";
} elseif ($timeNow > $cutoff) {
	$deliveryDay = "tomorrow";
	$deliveryDate = date("m/d/y", mktime(0, 0, 0, date("m"), date("d")+1, date("y")));
	$fontColor = "red";
}

$reseller_id = $_SESSION['reseller_id'];

$dmonth = date("m", strtotime($deliveryDate));
$dday = date("d", strtotime($deliveryDate));
$sql = "SELECT name, date, repeat_yearly
        FROM tbl_holiday
        WHERE DATE_FORMAT(date, '%m') = '$dmonth' AND
              DATE_FORMAT(date, '%d') = '$dday' AND
              active = 1 AND 
              reseller_id = $reseller_id";
$result = mysql_query($sql);

if( $result !== false ){
    while($rows[] = mysql_fetch_assoc($result));
    array_pop ($rows);
}

// If there's a holiday on the delivery day, find the next day that isn't a holiday
if(count($rows) > 0)
{
    $sql = "SELECT name, date, repeat_yearly
        FROM tbl_holiday
        WHERE active = 1 AND 
              reseller_id = $reseller_id";
    $result = mysql_query($sql);

    while($rows[] = mysql_fetch_assoc($result));
    array_pop ($rows);
    
    $continue = true;
    $count = 1;
    $testDate;
    while($continue)
    {
        $continue = false;
        
        $testDate = strtotime(date("Y-m-d", strtotime($deliveryDate)) . " +$count days");
        // check to see if $testDate is a holiday
        
        if(date("l", $testDate) == "Saturday" || date("l", $testDate) == "Sunday")
        {
            $continue = true;
        }
        else
        {
            foreach ($rows as $row)
            {
                $day = date("d", strtotime($row["date"]));
                $month = date("m", strtotime($row["date"]));
                $year = date("Y", strtotime($row["date"]));
                $testDay = date("d", $testDate);
                $testMonth = date("m", $testDate);
                $testYear = date("Y", $testDate);
                $repeat = $row["repeat_yearly"];

                if($day == $testDay && $month == $testMonth)
                {
                    //this may be a holiday
                    //test year
                    if($year == $testYear || $repeat == 1)
                    {
                        //This is a holiday
                        $continue = true;
                    }
                }
            }
        }
        $count ++;
    }
    $deliveryDay = date("l", $testDate);
    $deliveryDate = date("m/d/y", $testDate);
}

$timeNow = time();

$cut_off_val = strtotime($cutoff);
$cut_off_display = date("g:i A",strtotime($cutoff));

if ($timeNow < $cut_off_val) {	
    echo "<font color = \"$fontColor\"> Orders placed before $cut_off_display today will be delivered <strong>$deliveryDay, $deliveryDate</strong></font>";
} else {
    $deliveryDate=date("m/d/y", mktime(0, 0, 0, date("m"), date("d")+1, date("y")));
    $deliveryDay=date("l", mktime(0, 0, 0, date("m"), date("d")+1, date("y")));		
    echo "<font color = \"$fontColor\"> Orders placed after $cut_off_display today will be delivered <strong>$deliveryDay, $deliveryDate</strong></font>";	
}

?>