/*
Strip whitespace from the beginning and end of a string
Input : a string
*/
function trim(str)
{
	return str.replace(/^\s+|\s+$/g,'');
}


/*
Make sure that textBox only contain number
*/
function checkNumber(textBox)
{
	while (textBox.value.length > 0 && isNaN(textBox.value)) {
		textBox.value = textBox.value.substring(0, textBox.value.length - 1)
	}
	
	textBox.value = trim(textBox.value);
/*	if (textBox.value.length == 0) {
		textBox.value = 0;		
	} else {
		textBox.value = parseInt(textBox.value);
	}*/
}

/*
	Check if a form element is empty.
	If it is display an alert box and focus
	on the element
*/
function isEmpty(formElement, message) {
	formElement.value = trim(formElement.value);
	
	_isEmpty = false;
	if (formElement.value == '') {
		_isEmpty = true;
		alert(message);
		formElement.focus();
	}
	
	return _isEmpty;
}

function hasEntry(formElement) {
	elementValue = trim(formElement.value);
	
	_hasEntry = true;
	if (elementValue == '') {
		_hasEntry = false;
		// alert(message);
		// formElement.focus();
	}
	
	return _hasEntry;
}

/*
	Set one value in combo box as the selected value
*/
function setSelect(listElement, listValue)
{
	for (i=0; i < listElement.options.length; i++) {
		if (listElement.options[i].value == listValue)	{
			listElement.selectedIndex = i;
		}
	}	
}

// Additional LCS functions

function _getAuthorizeButton() {
    return document.querySelector("#authorize_button_" + document.getElementById("shakes").value);
}

function _authorizeFormSubmitFunction() {
    _setCookieData();
    this.submit();
}

function setATCButton() {
// LCS - Enable Add-to-cart button only if shake and tea selected

	var btnState = (document.getElementById("shakes").value > 0 && document.getElementById("teas").value > 0 && $('#btnAddToCart').attr('data-state') == 'true');
	var btnAdd = document.getElementById("btnAddToCart");
	btnAdd.disabled = (btnState) ? '' : 'disabled';

    if(btnState) {
        var authorizeButton = _getAuthorizeButton();
        showAuthButton(authorizeButton);

        authorizeButton.querySelector("form").onsubmit = _authorizeFormSubmitFunction; // setting listeners for tea and product id
    } else {
        _hideAuthButtons();
    }
}

function _setCookieData() {
    jQuery.cookie("tea", document.getElementById("teas").value, "/");
    jQuery.cookie("shake", document.getElementById("shakes").value, "/");
}

function showAuthButton(authorizeButton) {
    /* working on styling authorize.net buttons */
    _hideAuthButtons();
    authorizeButton.style.display = "block";
}

function _hideAuthButtons() {
    var authButtons = document.querySelectorAll(".auth_buttons");

    for(i=0; i< authButtons.length; i++) {
        authButtons[i].style.display = "none";
    }
}

function addSTToCart() {
	var pd_id = document.getElementById("shakes").value;
	var tea_id = document.getElementById("teas").value;
	var cart_url = "../shared/cart.php?action=add&p=" + pd_id + "&t=" + tea_id;
	window.location.href = cart_url;
}
//function checkLoginInfo()
// THIS FUNCTION GOES AWAY AFTER TESTING
//{
//	with (window.document.frmLogin) {
//		if (isEmpty(txtUName, 'Enter first and last name')) {
//			return false;
//		} else if (isEmpty(txtUEmail, 'Enter an email address')) {
//			return false;
//		} else if (isEmpty(txtUPhone, 'Enter a phone number')) {
//			return false;
//		} else if (isEmpty(txtUPass, 'Enter a password')) {
//			return false;
//		} else {
//			return true;
//		}
//	}
//}

function registerReset(loginIndex) {
	document.frmRR.login.value = loginIndex;
	document.frmRR.submit();
}

function toggleLogIn() {
    
	var enableStatus = false;
	var passwordsMatch = false;
	if (document.getElementById("upass")) {
		enableStatus = (document.frmLogin.uemail.value != '') && (document.frmLogin.upass.value != '');
	}	
	if (document.getElementById("uname")) {
		passwordsMatch = (document.frmLogin.upass1.value != '') && (document.frmLogin.upass2.value != '') && (document.frmLogin.upass1.value == document.frmLogin.upass2.value);
		enableStatus = (document.frmLogin.uname.value != '') && (document.frmLogin.uphone.value != '') && (document.frmLogin.uemail.value != '');
		enableStatus = enableStatus && passwordsMatch;
	}	
	document.frmLogin.btnLogIn.disabled = (enableStatus) ? '' : 'disabled';
        
}

function checkForDuplicateEmail() {
	checkForExists();
	var passFail = document.frmLogin.hidDuplicateEmail.value;
	if (passFail=="false") {
		alert ("A user with this email address is already registered. Try again.");
	} else {
		document.forms["frmLogin"].submit();
	}
}

function checkForValidEmail() {
	checkForExists();
	var passFail = document.frmLogin.hidDuplicateEmail.value;
	if (passFail=="true") {
		alert ("The email address you entered is not valid. Try again.");
	} else {
		document.forms["frmLogin"].submit();
	}
}

function checkForExists() {
 // check email and password against database to see if user exists
 // If so, then continue. If not, then stay at login screen.
 	with (window.document.frmLogin) {
		var emailadd = uemail.value;
		//alert (emailadd);
		if (window.XMLHttpRequest) {
		  xmlhttp=new XMLHttpRequest();
		}	else {// code for IE5, IE6, later
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function() {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200) {
		    var result = xmlhttp.responseText;
		    // alert (result);
		    if (result == "duplicate") {
		    	document.frmLogin.hidDuplicateEmail.value=false;
		    } else {
		    	document.frmLogin.hidDuplicateEmail.value=true;
		    }
		  }
		}
		var gprand=parseInt(Math.random()*999999999999999);
		xmlhttp.open("GET","../shared/include/checkEmailExists.php?rand="+gprand+"&em="+emailadd,false);
		xmlhttp.send();
	}
}

function updateDeliveryInfo() {
	getDeliveryInfo();
    timeTimer = setTimeout("updateDeliveryInfo()", 20000);
}

function getDeliveryInfo() {
	// Get the current time, delivery day
	if (window.XMLHttpRequest) {
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE5, IE6, later
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("deliveryInfo").innerHTML=xmlhttp.responseText;
	    }
	  }
	var gprand=parseInt(Math.random()*999999999999999);
	xmlhttp.open("GET","../shared/library/getDeliveryInfo.php?rand="+gprand,true);
	xmlhttp.send();
}

function toggleCartButtons() {
	document.frmCart.btnUpdate.style.visibility = "visible";
	document.getElementById('btnCheckout').disabled = "disabled";
}	

function clearInvalidCards(memberId) {
    $.ajax({
        url: "../shared/library/clearInvalidCards.php",
        type: "POST",
        data: {mem_id: memberId},
        async: false,
        error: function(jqXHR, textStatus, errorThrown){alert("textStatus: "+textStatus+", errorThrown: "+errorThrown);}
    });
}

function getUrlParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}