<?php
require_once 'config.php';

//LCS 091711
if((include '../shared/include/cryptor.php') !== 1)
{
    include 'include/cryptor.php';
}

/*********************************************************
*                 CHECKOUT FUNCTIONS 
*********************************************************/
function saveOrder($paymentId, $shippingId = 0, $fiveField = 'chkFive')
{
	$orderId       = 0;
	
	// LCS 100411
	// Changed from 5 to 0, will trigger alert in admin > order fulfillment process, of some sort
	$shippingCost  = 0;
	$requiredField = array('customerPaymentProfileId');
						   
	if (checkRequiredPost($requiredField)) {
	    extract($_POST);
		
		// make sure the first character in the 
		// customer and city name are properly upper cased
//		$hidShippingFirstName = ucwords($hidShippingFirstName);
//		$hidShippingLastName  = ucwords($hidShippingLastName);
//		$hidPaymentFirstName  = ucwords($hidPaymentFirstName);
//		$hidPaymentLastName   = ucwords($hidPaymentLastName);
//		$hidShippingCity      = ucwords($hidShippingCity);
//		$hidPaymentCity       = ucwords($hidPaymentCity);
		
		//LCS 091711
		//Encrypt CC data before storing
		global $key;
		//$cardData = $hidCardType."|".$hidCardNumber."|".$hidCardMonth."|".$hidCardYear."|".$hidBillingName."|".$hidBillingAddress."|".$hidBillingCity."|".$hidBillingState."|".$hidBillingZipCode;
		//$cardData = encryptdata($cardData, $key) or die("Failed to complete encryption.");
				
		//$cartContent = getCartContent();
		//$numItem     = count($cartContent);
		
		//LCS - added fields for CC data
		// save order & get order id
		$organization = $_SESSION['organization'];
		$memberId = $_SESSION['mid'];
		$chkFive = (isset($_SESSION[$fiveField])) ? 1 : 0;
		$chkCoupon = (isset($_SESSION['chkCoupon'])) ? 1 : 0;
                
                //if after 8
//                if(date("H")>8)
//                {
//                    if(date("w",strtotime( "tomorrow" ))>5)
//                    {
//                        $deliveryDate = strtotime( "next monday" );
//                    }
//                    else
//                    {
//                        $deliveryDate = strtotime( "tomorrow" );
//                    }
//                }
//                else
//                {
//                    $deliveryDate = strtotime( "today" );
//                }
                
                $deliveryDate = strtotime(getDeliveryDate());
                $endDeliv = strtotime("this friday");
                $today = strtotime("today");
                
                if( date("m/d/y", $today) === date("m/d/y", $endDeliv) ){
                    // if today is friday, set end of repeat to next friday
                    $endDeliv = strtotime(date("m/d/y", $endDeliv) . " +7 days");
                }
                
                $numRepeat = 1;
                if($chkFive && $deliveryDate < $endDeliv)
                {
                    $delivDay = date("w", $deliveryDate); //5 = friday
                    $numRepeat = 6-$delivDay;//create this many orders
                    
                }
                
                $cartContent = getCartContent();
                $numItem     = count($cartContent);
                
                
                for($x = 0; $x < $numRepeat; $x++)
                {
                    if(!isHoliday(date("Y-m-d",$deliveryDate)))
                    {

                        $org_id = $_SESSION["organization_id"];
                        
                        $deliveryDate = date ("Y-m-d H:i:s", $deliveryDate);
                        $sql = "INSERT INTO tbl_order(od_date, od_last_update, od_org, od_org_id, mem_id, od_five_day, od_coupon, od_delivery_date, payment_profile_id, shipping_address_id)
                            VALUES (NOW(), NOW(), '$organization', $org_id, '$memberId', '$chkFive', '$chkCoupon', '$deliveryDate', $paymentId, $shippingId)";
                        $result = dbQuery($sql);
                        $deliveryDate = strtotime($deliveryDate . "+1 day");

                        $tempId = dbInsertId();
                        if($orderId == 0)
                        {
                            $orderId = $tempId;
                        }


                        // save order items
                        for ($i = 0; $i < $numItem; $i++) {
                                $sql = "INSERT INTO tbl_order_item(od_id, pd_id, tea_id, od_qty)
                                                VALUES ($tempId, {$cartContent[$i]['pd_id']}, {$cartContent[$i]['tea_id']}, {$cartContent[$i]['ct_qty']})";
                                $result = dbQuery($sql);					
                        }


                        // update product stock
                        for ($i = 0; $i < $numItem; $i++) {
                                $sql = "UPDATE tbl_product 
                                        SET pd_qty = pd_qty - {$cartContent[$i]['ct_qty']}
                                                WHERE pd_id = {$cartContent[$i]['pd_id']}";
                                $result = dbQuery($sql);					
                        }

                    }
                }

                // then remove the ordered items from cart
//                for ($i = 0; $i < $numItem; $i++) {
//                        $sql = "DELETE FROM tbl_cart
//                                WHERE ct_id = {$cartContent[$i]['ct_id']}";
//                        $result = dbQuery($sql);					
//                }
		
		// save cc info in member record
		//$sql = "UPDATE tbl_members SET usecc = '$hidCardSave', mdata = '$cardData'
        //WHERE id = '".$_SESSION['mid']."'";
		//$result = dbQuery($sql);

							
	}
	
	return $orderId;
}

function updateOrderId($orderId, $tansactId)
{
    $sql = "UPDATE tbl_order 
            SET an_transaction_id = $tansactId
            WHERE od_id = $orderId";
    $result = dbQuery($sql);
}

function saveOrderItems($orderId)
{
    $cartContent = getCartContent();
    $numItem     = count($cartContent);
    if ($orderId) {
        // save order items
        for ($i = 0; $i < $numItem; $i++) {
                $sql = "INSERT INTO tbl_order_item(od_id, pd_id, tea_id, od_qty)
                                VALUES ($orderId, {$cartContent[$i]['pd_id']}, {$cartContent[$i]['tea_id']}, {$cartContent[$i]['ct_qty']})";
                $result = dbQuery($sql);					
        }


        // update product stock
        for ($i = 0; $i < $numItem; $i++) {
                $sql = "UPDATE tbl_product 
                        SET pd_qty = pd_qty - {$cartContent[$i]['ct_qty']}
                                WHERE pd_id = {$cartContent[$i]['pd_id']}";
                $result = dbQuery($sql);					
        }


        // then remove the ordered items from cart
        for ($i = 0; $i < $numItem; $i++) {
                $sql = "DELETE FROM tbl_cart
                        WHERE ct_id = {$cartContent[$i]['ct_id']}";
                $result = dbQuery($sql);					
        }
    }
}

function deleteOrder($orderId)
{
    
    $sql = "DELETE FROM tbl_order WHERE od_id = $orderId";
    $result = dbQuery($sql);
    
}

/*
	Get order total amount ( total purchase + shipping cost )
*/
function getOrderAmount($orderId)
{
	$orderAmount = 0;
	
	$sql = "SELECT SUM(pd_price * od_qty)
	        FROM tbl_order_item oi, tbl_product p 
		    WHERE oi.pd_id = p.pd_id and oi.od_id = $orderId";
			
//			UNION
//			
//			SELECT od_shipping_cost 
//			FROM tbl_order
//			WHERE od_id = $orderId";
	$result = dbQuery($sql);

	if (dbNumRows($result) == 2) {
		$row = dbFetchRow($result);
		$totalPurchase = $row[0];
		
		$row = dbFetchRow($result);
		$shippingCost = $row[0];
		
		$orderAmount = $totalPurchase + $shippingCost;
	}	
	
	return $orderAmount;	
}

?>