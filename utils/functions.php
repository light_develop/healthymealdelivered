<?
function logm($tag, $message) {
    $path = '../log/log.txt';
    $fh = fopen($path, 'a');
    $text = "\n====================================================".
        "\nTAG: ".$tag.
        "\nTIME: ".date('d.m.Y H:i:s').
        "\nCONTENT: ".$message.
        "\n====================================================\n";
    fwrite($fh, $text);
    fclose($fh);
}

function logd($tag, $var, $dumper = false) {
    if ($dumper) {
        $text = $dumper($var);
    } else {
        $text = print_r($var, true);
    }
    logm($tag, $text = print_r($var, true));
}

function get_response($url, $curl_options = array()) {
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    foreach ($curl_options as $opt => $val) {
        curl_setopt($ch, $opt, $val);
    }
    $resp = curl_exec($ch);
    curl_close($ch);
    return $resp;
}

function get_json($url, $curl_options = array()) {
    return json_decode(get_response($url, $curl_options), true);
}

function get_canonical_response_data($url, $curl_options = array()) {
    $resp = get_response($url, $curl_options);
    $arrKeysAndValues = explode('&', $resp);
    $arrData = array();
    foreach ($arrKeysAndValues as $keyvalue) {
        $arrKeyValue = explode('=', $keyvalue);
        $arrData[$arrKeyValue[0]] = $arrKeyValue[1];
    }
    return $arrData;
}

function abs_url($rel_url) {
    return 'http://'.$_SERVER['SERVER_NAME'].$rel_url;
}
?>