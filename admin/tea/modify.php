<?php
if (!defined('WEB_ROOT')) {
	exit;
}

// make sure a category id exists
if (isset($_GET['teaId']) && (int)$_GET['teaId'] > 0) {
	$teaId = (int)$_GET['teaId'];
} else {
	header('Location:index.php');
}	
$reseller_id = $_SESSION["reseller_id"];
	
$sql = "SELECT t.tea_id, t.tea_name
        FROM tbl_teas t
        JOIN tbl_reseller_tea rt ON t.tea_id = rt.tea_id
        WHERE t.tea_id = $teaId AND rt.reseller_id = $reseller_id";
$result = dbQuery($sql);
$row = dbFetchAssoc($result);

if( $row !== false ){
    extract($row);
} else {
    echo "Could not modify this record";
    exit;
}

?>
<p>&nbsp;</p>
<form action="processTea.php?action=modify&teaId=<?php echo $teaId; ?>" method="post" enctype="multipart/form-data" name="frmTea" id="frmTea">
 <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
  <tr> 
   <td width="150" class="label">Tea Name</td>
   <td class="content"><input name="txtName" type="text" class="box" id="txtName" value="<?php echo $tea_name; ?>" size="30" maxlength="50"></td>
  </tr>
 </table>
 <p align="center"> 
  <input name="btnModify" type="button" id="btnModify" value="Save Modification" onClick="checkTeaForm();" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" value="Cancel" onClick="window.location.href='index.php';" class="box">
 </p>
</form>