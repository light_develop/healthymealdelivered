<?php
require_once '../../shared/library/config.php';
require_once '../library/functions.php';

checkUser();

$reseller_id = $_SESSION["reseller_id"];

$action = isset($_GET['action']) ? $_GET['action'] : '';
switch ($action) {
	
    case 'add' :
        addTea();
        break;
      
    case 'modify' :
        modifyTea();
        break;
        
    case 'delete' :
        deleteTea();
        break;
    
	   
    default :
        // if action is not defined or unknown
        // move to main category page
        header('Location: index.php');
}


/*
    Add a Tea
*/
function addTea()
{
    $name        = $_POST['txtName'];
    global $reseller_id;
    
    $sql    = "INSERT INTO tbl_teas (tea_name) 
               VALUES ('$name')";
    $result = dbQuery($sql) or die('Cannot add tea' . mysql_error());
    
    $teaId = dbInsertId();
    
    $sql    = "INSERT INTO tbl_reseller_tea (reseller_id, tea_id) 
               VALUES ($reseller_id, $teaId)";
    $result = dbQuery($sql) or die('Cannot add tea' . mysql_error());
    
    header('Location: index.php?');
}


/*
    Modify a Tea
*/
function modifyTea()
{
    $teaId       = (int)$_GET['teaId'];
    $name        = $_POST['txtName'];
    global $reseller_id;
     
    $sql    = "UPDATE tbl_teas 
               SET tea_name = '$name'
               WHERE tea_id = $teaId";
           
    $result = dbQuery($sql) or die('Cannot update tea. ' . mysql_error());
    header('Location: index.php');              
}

/*
    Remove a Tea
*/
function deleteTea()
{
    if (isset($_GET['teaId']) && (int)$_GET['teaId'] > 0) {
        $teaId = (int)$_GET['teaId'];
    } else {
        header('Location: index.php');
    }
    
    global $reseller_id;
    
    // first make sure the specified category belongs to the reseller
    $q = "SELECT t.tea_id FROM tbl_teas t JOIN tbl_reseller_tea rt ON t.tea_id = rt.tea_id WHERE t.tea_id = $teaId AND rt.reseller_id = $reseller_id";
    $result = dbQuery($q);
    
    if( $result !== false && mysql_num_rows($result) > 0 ){

        // first, make sure no other reseller is referencing this tea
        $q = "SELECT DISTINCT reseller_id FROM tbl_reseller_tea WHERE tea_id = $teaId";
        $result = dbQuery($q);
        
        if( $result !== false && mysql_num_rows($result) > 1 ){
            // other resellers reference this tea. Only delete this reseller's link
            $sql = "DELETE FROM tbl_reseller_tea
                    WHERE tea_id = $teaId AND reseller_id = $reseller_id";
            dbQuery($sql);
            
        } else {
            // this is the only reseller to reference this tea. We can safely delete
            $sql = "DELETE FROM tbl_teas 
                    WHERE tea_id = $teaId";
            dbQuery($sql);
            
            $sql = "DELETE FROM tbl_reseller_tea
                    WHERE tea_id = $teaId AND reseller_id = $reseller_id";
            dbQuery($sql);
            
        }
        
    }
    
    header('Location: index.php');
}

?>