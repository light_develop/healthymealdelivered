<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$reseller_id = $_SESSION["reseller_id"];
	
// for paging
// how many rows to show per page
$rowsPerPage = 5;

$sql = "SELECT t.tea_id, t.tea_name
        FROM tbl_teas t
        JOIN tbl_reseller_tea rt ON t.tea_id = rt.tea_id
		WHERE rt.reseller_id = $reseller_id
		ORDER BY tea_name";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage);
?>
<p>&nbsp;</p>
<form action="processTea.php?action=addTea" method="post"  name="frmListTea" id="frmListTea">
 <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="text">
  <tr align="center" id="listTableHeader"> 
   <td>Tea Name</td>
   <td width="75">Modify</td>
   <td width="75">Delete</td>
  </tr>
  <?php
  
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);
		
		if ($i%2) {
			$class = 'row1';
		} else {
			$class = 'row2';
		}
		
		$i += 1;
				
?>
  <tr class="<?php echo $class; ?>"> 
   <td><?php echo $tea_name; ?></td>
   <td width="75" align="center"><a href="javascript:modifyTea(<?php echo $tea_id; ?>);">Modify</a></td>
   <td width="75" align="center"><a href="javascript:deleteTea(<?php echo $tea_id; ?>);">Delete</a></td>
  </tr>
  <?php
	} // end while


?>
  <tr> 
   <td colspan="5" align="center">
   <?php 
   echo $pagingLink;
   ?></td>
  </tr>
<?php	
} else {
?>
  <tr> 
   <td colspan="5" align="center">No Teas Yet</td>
  </tr>
  <?php
}
?>
  <tr> 
   <td colspan="5">&nbsp;</td>
  </tr>
  <tr> 
   <td colspan="5" align="right"> <input name="btnAddTea" type="button" id="btnAddTea" value="Add Tea" class="box" onClick="addTea()"> 
   </td>
  </tr>
 </table>
 <p>&nbsp;</p>
</form>