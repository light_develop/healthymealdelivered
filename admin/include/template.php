<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$self = WEB_ROOT . 'admin/index.php';
?>
<html>
<head>
<title><?php echo $pageTitle; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<?php echo WEB_ROOT;?>admin/include/admin.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/javascript" src="<?php echo WEB_ROOT;?>shared/library/common.js"></script>

<link type="text/css" href="<?php echo WEB_ROOT;?>shared/library/jquery-ui/css/custom-theme/jquery-ui-1.8.23.custom.css" rel="Stylesheet" />	
<script type="text/javascript" src="<?php echo WEB_ROOT;?>shared/library/jquery-ui/js/jquery-1.8.0.min.js"></script>
<script type="text/javascript" src="<?php echo WEB_ROOT;?>shared/library/jquery-ui/js/jquery-ui-1.8.23.custom.min.js"></script>


<script>
    $(document).ready(function(){
        $("#txtDate").datepicker();
    });
</script>
<?php
$n = count($script);
for ($i = 0; $i < $n; $i++) {
	if ($script[$i] != '') {
		echo '<script language="JavaScript" type="text/javascript" src="' . WEB_ROOT. 'admin/library/' . $script[$i]. '"></script>';
	}
}
?>
<style>
    .subTitle {
        position: absolute; 
        color: white; 
        font-variant: small-caps; 
        top: 3.75em; left: 19.6em; 
        font-size: 27px; 
        font-style: italic; 
        font-weight: bold;
        text-shadow: -1px 0px 1px #b22, 1px 0px 1px #b22, 0px 1px 1px #b22, 0px -1px 1px #b22;
    }
    
</style>
</head>
<body>
<table width="750" border="0" align="center" cellpadding="0" cellspacing="1" class="graybox">
  <tr>
    <td colspan="2">
        <div style="position: relative;">
            <img src="<?php echo WEB_ROOT; ?>shared/images/cartheader.png" width="750" >
            <span class="subTitle">Administrator</span>
        </div>
    </td>
  </tr>
  <tr>
    <td width="150" valign="top" class="navArea">
      <a href="<?php echo WEB_ROOT; ?>admin/" class="leftnav">Home</a> 
	  <a href="<?php echo WEB_ROOT; ?>admin/category/" class="leftnav">Categories</a>
	  <a href="<?php echo WEB_ROOT; ?>admin/product/" class="leftnav">Shakes</a> 
          <a href="<?php echo WEB_ROOT; ?>admin/tea/" class="leftnav">Teas</a> 
	  <a href="<?php echo WEB_ROOT; ?>admin/order/?status=New" class="leftnav">Orders</a> 
	  <a href="<?php echo WEB_ROOT; ?>admin/member/?status=1" class="leftnav">Members</a> 
	  <a href="<?php echo WEB_ROOT; ?>admin/reports/?status=New" class="leftnav">Reports</a> 
	   <a href="<?php echo WEB_ROOT; ?>admin/reports/customer.php" class="leftnav">Report - Customer</a> 
	   <a href="<?php echo WEB_ROOT; ?>admin/reports/product.php" class="leftnav">Report - Product</a> 
		<a href="<?php echo WEB_ROOT; ?>admin/config/" class="leftnav">Shop Config</a> 
        <?php if(isset($_SESSION["is_admin"]) && $_SESSION["is_admin"] == 1): ?>
	  <a href="<?php echo WEB_ROOT; ?>admin/user/" class="leftnav">Users</a>
        <?php endif; ?>
          <a href="<?php echo WEB_ROOT; ?>admin/holiday/" class="leftnav">Holidays</a>
	  <a href="<?php echo $self; ?>?logout" class="leftnav">Logout</a>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
    <td width="600" valign="top" class="contentArea"><table width="100%" border="0" cellspacing="0" cellpadding="20">
        <tr>
          <td>
<?php
require_once $content;	 
?>
          </td>
        </tr>
      </table></td>
  </tr>
</table>
<p>&nbsp;</p>
<p align="center">Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.ordermnaonline.com"> www.ordermnaonline.com</a></p>
</body>
</html>
