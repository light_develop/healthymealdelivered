// JavaScript Document
function checkAddUserForm()
{
	with (window.document.frmAddUser) {
		if (isEmpty(txtUserName, 'Enter user name')) {
			return;
		} else if (isEmpty(txtPassword, 'Enter password')) {
			return;
		} else {
			submit();
		}
	}
}

function checkModifyUserForm()
{
    with (window.document.frmAddUser) {
        if (isEmpty(txtUserName, 'Enter user name')) {
            return;
        } else if (isEmpty(txtNewPassword1, 'Enter password')) {
            return;
        } else if (txtNewPassword1.value !== txtNewPassword2.value) {
            alert("Passwords must match");
            return;
        } else {
            submit();
        }
    }
}

function addUser()
{
	window.location.href = 'index.php?view=add';
}

function changePassword(userId)
{
	window.location.href = 'index.php?view=modify&userId=' + userId;
}

function deleteUser(userId)
{
	if (confirm('Delete this user?')) {
		window.location.href = 'processUser.php?action=delete&userId=' + userId;
	}
}

