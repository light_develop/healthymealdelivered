// JavaScript Document
function checkTeaForm()
{
    with (window.document.frmTea) {
        if (isEmpty(txtName, 'Enter tea name')) {
                return;
        } else {
                submit();
        }
    }
}

function addTea()
{
    targetUrl = 'index.php?view=add';

    window.location.href = targetUrl;
}

function modifyTea(teaId)
{
	window.location.href = 'index.php?view=modify&teaId=' + teaId;
}

function deleteTea(teaId)
{
	if (confirm('Deleting tea cannot be undone.\nContinue anyway?')) {
		window.location.href = 'processTea.php?action=delete&teaId=' + teaId;
	}
}