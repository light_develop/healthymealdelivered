// JavaScript Document
function checkHolidayForm()
{
    with (window.document.frmHoliday) {
		if (isEmpty(txtName, 'Enter holiday name')) {
			return;
		} else if (isEmpty(txtDate, 'Enter date of holiday')) {
			return;
		} else {
			submit();
		}
	}
}

function addHoliday(parentId)
{
	targetUrl = 'index.php?view=add';
	if (parentId != 0) {
		targetUrl += '&parentId=' + parentId;
	}
	
	window.location.href = targetUrl;
}

function modifyHoliday(catId)
{
	window.location.href = 'index.php?view=modify&id=' + catId;
}

function deleteCategory(catId)
{
	if (confirm('Deleting category will also delete all products in it.\nContinue anyway?')) {
		window.location.href = 'processCategory.php?action=delete&catId=' + catId;
	}
}

function deleteImage(catId)
{
	if (confirm('Delete this image?')) {
		window.location.href = 'processCategory.php?action=deleteImage&catId=' + catId;
	}
}