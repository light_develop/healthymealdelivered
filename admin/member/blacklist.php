<?php

if (!defined('WEB_ROOT')) {
	exit;
}

require_once('../../shared/include/cryptor.php');

$reseller_id = $_SESSION["reseller_id"];

if(isset($_POST["b_email"]) && strlen($_POST["b_email"])>0)
{
    $sql = "INSERT INTO tbl_blacklist (email, reseller_id) VALUES ('" . $_POST["b_email"]."', $reseller_id)";
    if(!mysql_query($sql))
    {
        echo "Unable to add email to black list. An error occurred: ".mysql_error();
    }
}
if(isset($_POST["d_email"]))
{
    $sql = "DELETE FROM tbl_blacklist WHERE email= '" . $_POST["d_email"]."' AND reseller_id = $reseller_id";
    if(!mysql_query($sql))
    {
        echo "Unable to remove email from black list. An error occurred: ".mysql_error();
    }
}

$rowsPerPage = 10;

$sql = "SELECT email FROM tbl_blacklist WHERE reseller_id = $reseller_id ORDER BY email";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage, '');
?>

<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="text">
            <tr align="center" id="listTableHeader">
                <td style="width: 500px;">Email</td>
                <td style="width: 50px;">&nbsp;</td>
            </tr>
            <?php
            $parentId = 0;
            if (dbNumRows($result) > 0) {
                $i = 0;
                $userIDs = array();
                while ($row = dbFetchAssoc($result)) {
                    extract($row);
                    
                    if ($i % 2) {
                        $class = 'row1';
                    } else {
                        $class = 'row2';
                    }

                    $i += 1;
                    ?>
                    <tr class="<?php echo $class; ?>">
                        <td><a href="mailto:<?php echo $email; ?>"><?php echo $email ?></a></td>
                        <td>
                            <form action="" method="post" id="email_form" name="email_form" onclick="return confirm('Are you sure you want to permanently remove this email from the blacklist?')" style="margin-bottom:0" >
                                <input type="submit" id="deleteEmail" name="deleteEmail" class="box" value="Delete" />
                                <input type="hidden" id="d_email" name="d_email" value="<?php echo $email; ?>" />
                            </form>
                        </td>
                    </tr>
        <?php
    } // end while
    
    ?>
                
                <tr> 
                    <td colspan="5" align="center">
                    <?php
                        echo $pagingLink;
                    ?>
                    </td>
                </tr>
                        <?php
                    } else {
                        ?>
                <tr> 
                    <td colspan="5" align="center">No Blacklisted Emails Found </td>
                </tr>
    <?php
}
?>
    
        </table>
<br />
<form action="" method="post" >
Add Email to Blacklist: <input type="text" name="b_email" id="b_email" /> <input type="submit" id="add_btn" name="add_btn" value="Add" />
</form>