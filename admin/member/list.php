<?php
if (!defined('WEB_ROOT')) {
	exit;
}

require_once('../../shared/include/cryptor.php');

$reseller_id = $_SESSION["reseller_id"];

if(isset($_POST['chkActive']))
{
    $checks = $_POST['chkActive'];
    $userIds = explode(",", $_POST["memberIds"]);
    $activeUsers = array();
    $suspendUsers = array();
    foreach($userIds as $id)
    {
        if(in_array($id, $checks))
        {
            $activeUsers[] = $id;
        }
        else
        {
            $suspendUsers[] = $id;
        }
    }
    
    if(count($activeUsers) > 0)
    {
        $sql = "UPDATE tbl_members
                SET status = 1
                WHERE id IN (".implode(",",$activeUsers).")";

        dbQuery($sql);
    }
    
    if(count($suspendUsers) > 0)
    {
        $sql = "UPDATE tbl_members
                SET status = 0
                WHERE id IN (".implode(",",$suspendUsers).")";

        dbQuery($sql);
    }
}

if (isset($_GET['status']) && $_GET['status'] != '') {
	$status = $_GET['status'];
	$org = '';
	$sql2   = " AND status = '$status'";
	$queryString = "&status=$status";
} else {
	$status = 2;
	$org = '';
	$sql2   = '';
	$queryString = '';
}	
// for paging
// how many rows to show per page
$rowsPerPage = 10;
$months = array("Jan" => "January","Feb" => "February","Mar" => "March","Apr" => "April","May" => "May","Jun" => "June","Jul" => "July","Aug" => "August","Sep" => "September","Oct" => "October","Nov" => "November","Dec" => "December");

// get organizations
$s = "SELECT organization_id FROM tbl_reseller_organization WHERE reseller_id = $reseller_id";
$sr = dbQuery($s);
$orgs = array();
while($row = dbFetchAssoc($sr)) {
    extract($row);
    
    $orgs[] = $organization_id;
}
$orgs = implode(",", $orgs);

$sql = "SELECT id, name, status, email, phone, usecc, DATE_FORMAT(sdate, '%m-%d-%y') as sdate, mdata FROM tbl_members WHERE organization_id IN ($orgs) $sql2 ORDER BY email";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage, $queryString);

$active = array('Active', 1);
$suspended = array('Suspended', 0);
$orderStatus = array($active, $suspended);
$orderStatusOption = '';
foreach ($orderStatus as $stat) {
	$orderStatusOption .= "<option value=\"$stat[1]\"";
	if ($stat[1] == $status) {
		$orderStatusOption .= " selected";
	}
	
	$orderStatusOption .= ">$stat[0]</option>\r\n";
}
?> 
<p><a href="index.php?view=blacklist">Email Blacklist</a></p>
<form action="" method="post"  name="frmOrderList" id="frmOrderList">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="text">
        <tr align="center">
            <td align="right"></td>
            <td width="75"></td> 
            <td width="200" align="right">Status</td>
            <td width="75"><select name="cboOrderStatus" class="box" id="cboOrderStatus" onChange="viewMember();">
                    <option value="" selected>All</option>
                    <?php echo $orderStatusOption; ?>
                </select></td>
        </tr>
    </table>
 </form>
    <form method="post" action="" >
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="text">
            <tr align="center" id="listTableHeader"> 
                <td width="153">Name</td>
                <td width="216">Email</td>
                <td width="100">Phone</td>
                <td width="58">Joined</td>
                <td width="46">Active</td>
            </tr>
            <?php
            $parentId = 0;
            if (dbNumRows($result) > 0) {
                $i = 0;
                $userIDs = array();
                while ($row = dbFetchAssoc($result)) {
                    extract($row);
                    $name = $name;
                    $userIDs[] = $id;
                    

                    if ($i % 2) {
                        $class = 'row1';
                    } else {
                        $class = 'row2';
                    }

                    $i += 1;
                    ?>
                    <tr class="<?php echo $class; ?>"> 
                        <td><?php echo $name; ?></td>
                        <td><a href="mailto:<?php echo $email; ?>"><?php echo $email ?></a></td>
                        <td><?php echo $phone; ?></td>
                        <td><?php echo $sdate; ?></td>
                        <td><input type="checkbox" name="chkActive[]" value="<?php echo $id; ?>" <?php if ($status) echo "checked='checked'"; ?> /></td>
                    </tr>
        <?php
    } // end while
    
    $userIDs = implode(",", $userIDs);
    ?>
                <tr>
                    <td colspan="5" align="right">
                        <input type="submit" id="updateMembersButton" name="updateMembersButton" class="box" value="Update" />
                        <input type="hidden" id="memberIds" name="memberIds" value="<?php echo $userIDs; ?>" />
                    </td>
                </tr>
                <tr> 
                    <td colspan="5" align="center">
                <?php
                echo $pagingLink;
                ?></td>
                </tr>
                        <?php
                    } else {
                        ?>
                <tr> 
                    <td colspan="5" align="center">No Members Found </td>
                </tr>
    <?php
}
?>
    
        </table>
    </form>
    <p>&nbsp;</p>
