<?php
if (!defined('WEB_ROOT')) {
    exit;
}

$reseller_id = $_SESSION["reseller_id"];

//                    x     day     of   month in year
function calcFloatHol($nTh, $nTargetday, $nMonth, $nYear)//for $nTargetday, 0=Sunday, 1=Monday, etc. for $nMonth, 1=Jan, 2=Feb, etc.
{// most logic from http://michaelthompson.org/technikos/holidays.php
    
    $nEarliestDate = 1 + 7 * ($nTh - 1);
    $nWeekday = date("w",mktime(0,0,0,$nMonth,$nEarliestDate,$nYear));
    
    if( $nTargetday == $nWeekday ) $nOffset = 0;
    else
    {
        if( $nTargetday < $nWeekday ) $nOffset = $nTargetday + (7 - $nWeekday);
        else $nOffset = ($nTargetday + (7 - $nWeekday)) - 7;
    }
    
    $tHolidayDate = mktime(0,0,0,$nMonth,$nEarliestDate + $nOffset,$nYear);
    
    if(date("l", $tHolidayDate) == "Saturday")
    {
        $tHolidayDate = strtotime($tHolidayDate." -1 day");
    }
    elseif(date("l", $tHolidayDate) == "Sunday")
    {
        $tHolidayDate = strtotime($tHolidayDate." +1 day");
    }
    
    return $tHolidayDate;
}

function getHoliday($date)
{
    $date = strtotime($date);
    if(date("l", $date) == "Saturday")
    {
        $date = strtotime(date("Y-m-d",$date)." -1 day");
    }
    elseif(date("l", $date) == "Sunday")
    {
        $date = strtotime(date("Y-m-d",$date)." +1 day");
    }
    
    return $date;
}

//echo "1st Friday of August 2012: " . date("Y-m-d", calcFloatHol(1, 5, 8, 2012));

if(isset($_POST["resetButton"]) || isset($_POST["insertButton"]))
{
    if(isset($_POST["resetButton"]))
    {
        $sql = "DELETE from tbl_holiday WHERE reseller_id = $reseller_id";
        $result = dbQuery($sql);
    }
    
    $year = date("Y");
    $holidays = array();
    
    //New Years
    $holidays[] = array("New Years Day", date("Y-m-d", getHoliday($year."-1-1")));
    
    //MLK Day ( 3rd Monday in January )
    $holidays[] = array("MLK Day", date("Y-m-d", calcFloatHol(3, 1, 1, $year)));
    
    //President's Day ( 3rd Monday in February )
    $holidays[] = array("President's Day", date("Y-m-d", calcFloatHol(3, 1, 2, $year)));
    
    //Memorial Day ( Last Monday in May )
    $Monday = calcFloatHol(4, 1, 5, $year);
    $plusWeek = strtotime(date("Y-m-d", $Monday)." +1 week");
    if(date("m", $plusWeek) == "05")//if there is a 5th Monday in May
    {
        $Monday = calcFloatHol(5, 1, 5, $year);
    }
    $holidays[] = array("Memorial Day", date("Y-m-d", $Monday));
    
    //Independence Day
    $holidays[] = array("Independence Day", date("Y-m-d", getHoliday($year."-7-4")));
    
    //Labor Day ( 1st Monday in September )
    $holidays[] = array("Labor Day", date("Y-m-d", calcFloatHol(1, 1, 9, $year)));
    
    //Columbus Day ( 2nd Monday in October )
    $holidays[] = array("Columbus Day", date("Y-m-d", calcFloatHol(2, 1, 10, $year)));
    
    //Veteran's Day
    $holidays[] = array("Veteran's Day", date("Y-m-d", getHoliday($year."-11-11")));
    
    //Thanksgiving Day ( 4th Thursday in November )
    $holidays[] = array("Thanksgiving Day", date("Y-m-d", calcFloatHol(4, 4, 11, $year)));
    
    //Christmas Day
    $holidays[] = array("Christmas Day", date("Y-m-d", getHoliday($year."-12-25")));
    
    //New Years Eve
    $holidays[] = array("New Years Eve", date("Y-m-d", getHoliday($year."-12-31")));
    
    
    //enter into database
    $count = count($holidays);
    $i = 0;
    
    $sql = "REPLACE INTO tbl_holiday (name, date, reseller_id) VALUES";
    
    foreach($holidays as $holiday)
    {
        $sql .= "(\"".$holiday[0]."\",\"".$holiday[1]."\", $reseller_id)";
        
        if(++$i != $count)
        {
            $sql .= ",";
        }
    }
    
    $result = dbQuery($sql);
}


$sql = "SELECT id, date, active, name, repeat_yearly
        FROM tbl_holiday
        WHERE reseller_id = $reseller_id
		ORDER BY date ASC";
$result = dbQuery($sql);
?> 
<p>&nbsp;</p>
<form action="processUser.php?action=addUser" method="post"  name="frmListUser" id="frmListUser">
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="text">
        <tr align="center" id="listTableHeader"> 
            <td>Name</td>
            <td>Date</td>
            <td>Active</td>
            <td>Repeat</td>
        </tr>
        <?php
        while ($row = dbFetchAssoc($result)) {
            extract($row);

            if ($i % 2) {
                $class = 'row1';
            } else {
                $class = 'row2';
            }

            $i += 1;
            ?>
            <tr class="<?php echo $class; ?>">
                <td><a href="javascript:modifyHoliday(<?php echo $id; ?>);"><?php echo $name; ?></a></td>
                <td><?php echo $date; ?></td>
                <td align="center"><?php if($active){ echo "Yes";} else {echo "No";} ?></td>
                <td align="center"><?php if($repeat_yearly){ echo "Yes";} else {echo "No";} ?></td>
            </tr>
            <?php
        } // end while
        ?>
        <tr> 
            <td colspan="5">&nbsp;</td>
        </tr>
        <tr> 
            <td colspan="5" align="right"><input name="btnAddHoliday" type="button" id="btnAddHoliday" value="Add Holiday" class="box" onClick="addHoliday()"></td>
        </tr>
    </table>
    <p>&nbsp;</p>
</form>
<form method="post" action="" style="display: inline;" title="Removes custom holidays, leaving only default holidays for this year" onsubmit="return confirm('This will remove all your custom holidays. Are you Sure you want to continue?')">
    <input type="submit" id="resetButton" name="resetButton" value="Reset Holidays" class="box" />
</form>
<form method="post" action="" style="display: inline;">
    <input type="submit" id="insertButton" name="insertButton" value="Add Year's Regular Holidays" class="box" title="Adds all the default holidays for this year" />
</form>