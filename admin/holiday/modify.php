<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$reseller_id = $_SESSION["reseller_id"];

$id = $_GET["id"];
if(isset($_POST['txtDate']) && isset($_GET["id"]))
{
    $date = strtotime($_POST["txtDate"]);
    $date = date( 'Y-m-d H:i:s', $date );
    $name = $_POST["txtName"];
    $repeat = (isset($_POST['chkRepeat']) && $_POST['chkRepeat'] == 'on') ? 1 : 0;
    $active = (isset($_POST['chkActive']) && $_POST['chkActive'] == 'on') ? 1 : 0;
    
    $sql = "UPDATE tbl_holiday 
            SET date = '$date',
                name = '$name',
                active = $active,
                repeat_yearly = $repeat
            WHERE id = $id AND reseller_id = $reseller_id";
    
    $result = mysql_query($sql);
    
    //redirect
    echo "<script>location.href='../holiday'</script>";
}

$sql = "SELECT *
        FROM tbl_holiday
        WHERE id = $id AND reseller_id = $reseller_id";

$result = mysql_query($sql);
if( mysql_num_rows($result) < 1 ){
    echo "You can not modify this holiday";
    exit;
}

extract(mysql_fetch_array($result));

$parentId = (isset($_GET['parentId']) && $_GET['parentId'] > 0) ? $_GET['parentId'] : 0;
?> 
<style>.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 1.1em; }</style>
<form action="" method="post" name="frmHoliday" id="frmHoliday">
 <p align="center" class="formTitle">Modify Holiday</p>
 
 <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
  <tr> 
   <td width="150" class="label">Date</td>
   <td class="content"><input name="txtDate" type="text" class="box" id="txtDate" size="30" maxlength="50" value="<?php echo date("m/d/Y", strtotime($date)); ?>"></td>
  </tr>
  <tr> 
   <td width="150" class="label">Name</td>
   <td class="content"><input name="txtName" type="text" class="box" id="txtName" size="30" maxlength="50" value="<?php echo $name; ?>"></td>
  </tr>
  <tr> 
   <td width="150" class="label">Active</td>
   <td class="content"><input name="chkActive" type="checkbox" class="box" id="chkActive" <?php if($active) echo "checked='checked'"; ?> /></td>
  </tr>
  <tr> 
   <td width="150" class="label">Repeat Yearly</td>
   <td class="content"><input name="chkRepeat" type="checkbox" class="box" id="chkRepeat" <?php if($repeat_yearly) echo "checked='checked'"; ?> />
       Repeat this holiday on the same day every year
   </td>
  </tr>
 </table>
 <p align="center"> 
  <input name="btnAddHoliday" type="button" id="btnAddHoliday" value="Update" onClick="checkHolidayForm();" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" value="Cancel" onClick="window.location.href='index.php';" class="box">
 </p>
</form>