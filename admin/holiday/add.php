<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$reseller_id = $_SESSION["reseller_id"];

if(isset($_POST['txtDate']))
{
    $date = strtotime($_POST["txtDate"]);
    $date = date( 'Y-m-d H:i:s', $date );
    $name = $_POST["txtName"];
    $repeat = (isset($_POST['chkRepeat']) && $_POST['chkRepeat'] == 'on') ? 1 : 0;
    $active = (isset($_POST['chkActive']) && $_POST['chkActive'] == 'on') ? 1 : 0;
    
    $sql = "REPLACE INTO tbl_holiday (date, name, active, repeat_yearly, reseller_id)
            VALUES ('$date', '$name', $active, $repeat, $reseller_id)";
    
    $result = mysql_query($sql);
    
    //redirect
    echo "<script>location.href='../holiday'</script>";
}

$parentId = (isset($_GET['parentId']) && $_GET['parentId'] > 0) ? $_GET['parentId'] : 0;
?> 
<style>.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 1.1em; }</style>
<form action="" method="post" name="frmHoliday" id="frmHoliday">
 <p align="center" class="formTitle">Add Holiday</p>
 
 <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="entryTable">
  <tr> 
   <td width="150" class="label">Date</td>
   <td class="content"><input name="txtDate" type="text" class="box" id="txtDate" size="30" maxlength="50"></td>
  </tr>
  <tr> 
   <td width="150" class="label">Name</td>
   <td class="content"><input name="txtName" type="text" class="box" id="txtName" size="30" maxlength="50"></td>
  </tr>
  <tr> 
   <td width="150" class="label">Active</td>
   <td class="content"><input name="chkActive" type="checkbox" class="box" id="chkActive" checked="checked" /></td>
  </tr>
  <tr> 
   <td width="150" class="label">Repeat Yearly</td>
   <td class="content"><input name="chkRepeat" type="checkbox" class="box" id="chkRepeat" />
       Repeat this holiday on the same day every year
   </td>
  </tr>
 </table>
 <p align="center"> 
  <input name="btnAddHoliday" type="button" id="btnAddHoliday" value="Add Holiday" onClick="checkHolidayForm();" class="box">
  &nbsp;&nbsp;<input name="btnCancel" type="button" id="btnCancel" value="Cancel" onClick="window.location.href='index.php';" class="box">  
 </p>
</form>