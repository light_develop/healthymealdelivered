<?php
require_once '../../shared/library/config.php';
require_once '../library/functions.php';

checkUser();

$action = isset($_GET['action']) ? $_GET['action'] : '';

switch ($action) {
	
    case 'modify' :
        modifyShopConfig();
        break;
    

    default :
        // if action is not defined or unknown
        // move to main page
        header('Location: index.php');
}

function offsetDecode( $offset ) {
    list($hours, $minutes, $seconds) = explode(':', $offset);

    if ($hours < 0) {
        $return = $hours*60*60 - $minutes*60 - $seconds;
    } else {
        $return = $hours*60*60 + $minutes*60 + $seconds;
    }

    return $return;
}

function modifyShopConfig()
{
	$shopName            = $_POST['txtShopName'];
	$address             = $_POST['mtxAddress'];
	$phone               = $_POST['txtPhone'];
	$email               = $_POST['txtEmail'];
	$shipping            = strlen($_POST['txtShippingCost']) > 0 ? $_POST["txtShippingCost"] : 0.00;
	$currency            = $_POST['cboCurrency'];
	$sendEmail           = $_POST['optSendEmail'];
    $sc_order_time_limit = $_POST['timeTodayDeliveryLimit'];
    $sc_order_limit = $_POST['timeTodayOrderLimit'];
    $sc_server_timezone  = $_POST['tzServerTimeZone'];
        
    $reseller_id = $_SESSION["reseller_id"];
    
    // first we'll see if they have a config row to update
    $check = "SELECT id FROM tbl_shop_config WHERE reseller_id = $reseller_id";
    $checkr = dbQuery($check);
    
    // if they have a config row, update it
    if( mysql_num_rows($checkr) > 0 ){

        $sql = "UPDATE tbl_shop_config
                SET sc_name =             '$shopName', 
                    sc_address =          '$address', 
                    sc_phone =            '$phone', 
                    sc_email =            '$email',
                    sc_shipping_cost =     $shipping, 
                    sc_currency =          $currency, 
                    sc_order_email =      '$sendEmail',
                    sc_order_time_limit = '$sc_order_time_limit',
                    sc_order_limit = '$sc_order_limit',
                    sc_server_timezone =  '$sc_server_timezone'
                WHERE reseller_id = $reseller_id";
        $result = dbQuery($sql);
        
    } else {
        // no config row yet, add
        
        $sql = "INSERT INTO tbl_shop_config 
                   (sc_name, 
                    sc_address, 
                    sc_phone, 
                    sc_email,
                    sc_shipping_cost, 
                    sc_currency, 
                    sc_order_email, 
                    sc_order_time_limit, 
                    sc_order_limit,
                    sc_server_timezone,
                    reseller_id)
                VALUES 
                   ('$shopName', 
                    '$address', 
                    '$phone', 
                    '$email', 
                     $shipping, 
                     $currency, 
                    '$sendEmail', 
                    '$sc_order_time_limit', 
                    '$sc_order_limit',
                    '$sc_server_timezone',
                     $reseller_id)";
        $result = dbQuery($sql);
        
    }

    //date_default_timezone_set($sc_server_timezone);
    
	header("Location: index.php");    
}

?>