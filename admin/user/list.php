<?php
if (!defined('WEB_ROOT')) {
	exit;
}

require_once('../../shared/include/cryptor.php');

$reseller_id = $_SESSION["reseller_id"];

// for paging
// how many rows to show per page
$rowsPerPage = 10;

$sql = "SELECT user_id, user_name, user_password, admin, user_regdate, r.name as reseller FROM tbl_user JOIN tbl_reseller_user ru USING(user_id) JOIN tbl_reseller r ON r.id = ru.reseller_id ORDER BY user_name";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage, "");

?> 

    <form method="post" action="" >
        <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="text">
            <tr align="center" id="listTableHeader"> 
                <td width="153">Name</td>
                <td>Reseller</td>
                <td width="58">Joined</td>
            </tr>
            <?php
            $parentId = 0;
            if (dbNumRows($result) > 0) {
                $i = 0;
                $userIDs = array();
                while ($row = dbFetchAssoc($result)) {
                    extract($row);
                    $name = $user_name;
                    $userIDs[] = $user_id;
                    $sdate = $user_regdate;

                    if ($i % 2) {
                        $class = 'row1';
                    } else {
                        $class = 'row2';
                    }

                    $i += 1;
                    ?>
                    <tr class="<?php echo $class; ?>"> 
                        <td><a href="index.php?view=modify&userId=<?php echo $user_id; ?>"><?php echo $name; ?></a></td>
                        <td><?php echo $reseller; ?></td>
                        <td><?php echo $sdate; ?></td>
                    </tr>
        <?php
    } // end while
    
    $userIDs = implode(",", $userIDs);
    ?>
                <tr>
                    <td colspan="5" align="right">
                        
<!--                        <form method="post" action="add.php" style="display: inline;">
                            <input type="submit" id="updateMembersButton" name="updateMembersButton" class="box" value="Add" />
                        </form>-->
                        
                        <input type="hidden" id="memberIds" name="memberIds" value="<?php echo $userIDs; ?>" />
                    </td>
                </tr>
                <tr> 
                    <td colspan="5" align="center">
                <?php
                echo $pagingLink;
                ?></td>
                </tr>
                        <?php
                    } else {
                        ?>
                <tr> 
                    <td colspan="5" align="center">No Members Found </td>
                </tr>
    <?php
}
?>
    
        </table>
    </form>
    <p>&nbsp;</p>
