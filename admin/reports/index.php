<?php
require_once '../../shared/library/config.php';
require_once '../library/functions.php';

$_SESSION['login_return_url'] = $_SERVER['REQUEST_URI'];
checkUser();

$view = (isset($_GET['view']) && $_GET['view'] != '') ? $_GET['view'] : '';

switch ($view) {
	case 'list' :
		$content 	= 'list.php';		
		$pageTitle 	= 'Shop Admin Control Panel - View Members';
		break;

	case 'detail' :
		$content 	= 'detail.php';		
		$pageTitle 	= 'Shop Admin Control Panel - Member Detail';
		break;

	case 'modify' :
		modifyStatus();
		//$content 	= 'modify.php';		
		//$pageTitle 	= 'Shop Admin Control Panel - Modify Orders';
		break;

	default :
		$content 	= 'list.php';		
		$pageTitle 	= 'Shop Admin Control Panel - View Members';
}




$script    = array('order.js');

require_once '../include/template.php';
?>
