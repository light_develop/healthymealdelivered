<?php
require_once '../../shared/library/config.php';
require_once '../library/functions.php';

$content = 'customer_content.php';

if(isset($_GET["pdf"]))
{
	require_once $content;
}
else
{
	require_once '../include/template.php';
}	