<?php 

$rdate = strtotime("-2 day");

$current_date = isset($_GET["date"]) ? $_GET["date"] : date("Y-m-d",strtotime("-1 day")); 
$rcurrent_date = isset($_GET["date"]) ? $_GET["date"] : date("Y-m-d",$rdate);

$current_date2 = isset($_GET["date2"]) ? $_GET["date2"] : date("Y-m-d");

$cut_off_time = getCutOffTime();
$cut_off_minus_one_second = date('H:i:s', strtotime($cut_off_time.' - 1 second'));

$show_date = date("m/d/Y",strtotime("-1 day"));
$show_date2 = date("m/d/Y");

if(isset($_GET["date"]))
{
	$show_date = explode("-",$current_date);
	
	$show_date = $show_date[1] . "/" . $show_date[2] . "/" . $show_date[0];
}

if(isset($_GET["date2"]))
{
	$show_date2 = explode("-",$current_date2);
	$show_date2 = $show_date2[1] . "/" . $show_date2[2] . "/" . $show_date2[0];
}

$title = 'Customer Fulfillment Report for '.$show_date;

if($show_date2 != $show_date)
{
	$title .= " - " . $show_date2;
}

$sql_text = "SELECT DISTINCT tbl_members.name,tbl_category.cat_name,tbl_product.pd_name,tbl_teas.tea_name

FROM tbl_order

INNER JOIN tbl_order_item ON tbl_order_item.od_id = tbl_order.od_id
INNER JOIN  tbl_members ON  tbl_members.id = tbl_order.mem_id
INNER JOIN tbl_teas ON tbl_teas.tea_id = tbl_order_item.tea_id
INNER JOIN tbl_product ON tbl_product.pd_id = tbl_order_item.pd_id
INNER JOIN tbl_category ON tbl_category.cat_id = tbl_product.cat_id

WHERE tbl_order.od_date BETWEEN   STR_TO_DATE('$rcurrent_date $cut_off_time','%Y-%m-%d %H:%i:%s') AND  STR_TO_DATE('$current_date2 $cut_off_minus_one_second','%Y-%m-%d %H:%i:%s')";

if(!isset($_GET["status"]))
{
	$_GET["status"] = "Paid";
}

if(isset($_GET["status"]) && $_GET["status"] != "0")
{
	$sql_text .= " AND tbl_order.od_status = '" . $_GET["status"] . "'";
}


$order_status = $_GET["status"];

$result     = dbQuery($sql_text);
$rows = dbGetRows($sql_text);
$rows = array_unique($rows);
$customers = array();

while($row = dbFetchAssoc($result)) {
	
	$name = explode(" ",$row["name"]);
	$row["display_name"] = isset($name[1]) ? $name[1] . ", " . $name[0] : $name[0];
	$customers[strtolower($row["display_name"])][] = $row;
}
ksort($customers);



if(isset($_GET["pdf"]))
{
	require_once($_SERVER["DOCUMENT_ROOT"] . "/admin/include/pdf/tcpdf.php");	
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetTitle($title);
	
	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	$pdf->AddPage();
	
	ob_start();
	
}
if(!isset($_GET["pdf"])) {
?>
<style>.ui-widget { font-family: Verdana,Arial,sans-serif; font-size: 1.1em; }</style>
<table>
<tr>
	<td>Report period:&nbsp;</td>
	<td>
		<label for="from">from</label>
		<input type="text" id="from" style="width:120px;" value="<?= $show_date ?>" name="from">
		<label for="to">to</label>
		<input type="text" id="to" style="width:120px;"  value="<?= $show_date2 ?>" name="to">
	</td>
</tr>
<tr>
<td>Status:&nbsp;</td>

<td>
<select id="status">
		<option value="0" <?= ($order_status  == "0" ? "selected='selected'" : "") ?>>All</option>
		<?php 
			$orderStatus = array('New', 'Paid', 'Completed', 'Cancelled');
			$orderStatusOption = '';
			foreach ($orderStatus as $stat) 
			{ 
			
			$orderStatusOption .= "<option value=\"$stat\"";
			if($order_status != "0")
			{
				if ($order_status  == $stat) {
					$orderStatusOption .= " selected='selected'";
				}
			}	
				$orderStatusOption .= ">$stat</option>\r\n";
			}
			
			print $orderStatusOption;
		?>
</select>
</td>
</tr>
</table>
<script type="text/javascript">

	
	function reloadResult()
	{
		var sdate = $("#from").val().split("/");
		var sdate2 = $("#to").val().split("/");
		
		var nsdate = sdate[2] + "-" + sdate[0] + "-" + sdate[1];
		var nsdate2 = sdate2[2] + "-" + sdate2[0] + "-" + sdate2[1];
		
		document.location.href = '/admin/reports/customer.php?date=' + nsdate + "&date2=" + nsdate2+"&status=" + $("#status").val() ;
	}
	
	$(function() {
		$( "#from" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
				$( "#to" ).datepicker( "option", "minDate", selectedDate );
			},
			showOn:'both',buttonText: "...",
			onSelect: function(dt,obj) { reloadResult(); }
		});
		$( "#to" ).datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
				$( "#from" ).datepicker( "option", "maxDate", selectedDate );
			},
			showOn:'both',buttonText: "...",
			onSelect: function(dt,obj) { reloadResult(); }
		});
		
		$("#status").change(function(){
			reloadResult();
		});
	});
</script>

<?php  } ?>
<h3><?= $title ?></h3>
<br />
<br />
<table cellpadding="5">
	<thead>
		<tr>
			<th><strong>Name</strong></th>
			<th><strong>Shake number</strong></th>
			<th><strong>Tea flavor</strong></th>
		</tr>
	</thead>
	<?php $i = 0; foreach($customers as  $key=>$value) { foreach($value as $v) { $i++; ?>
	<tr>
		<td style="text-transform:capitalize;"><?= $i ?>. <?= $v["display_name"] ?></td>
		<td><?= $v["cat_name"] ?> - <?= $v["pd_name"] ?>, </td>
		<td><?= $v["tea_name"] ?></td>
	</tr>
	<?php } } ?>
</table>
<?php if(isset($_GET["pdf"])) { 
	$content = ob_get_contents();
	ob_end_clean();
	$pdf->writeHTML($content, true, false, true, false, '');
	$pdf->Output('customer_fulfillment_report_for_' . $current_date . '.pdf', 'D');
} ?>
<?php if(!isset($_GET["pdf"])) { ?>
<br />
<iframe id="pdf_frame" style="display:none;" name="pdf_frame"></iframe>
<input type="button" value="Download as PDF" onclick="document.getElementById('pdf_frame').src = '/admin/reports/customer.php?pdf&date=<?= $current_date ?>&date2=<?= $current_date2 ?>&status=<?= $order_status ?>';"  />
<?php } ?>