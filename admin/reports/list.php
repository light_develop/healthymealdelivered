<?php
if (!defined('WEB_ROOT')) {
	exit;
}

$reseller_id = $_SESSION["reseller_id"];

if( isset($_GET["reseller_id"]) && $_GET["reseller_id"] != '' && $_SESSION["is_admin"] ){
    $reseller_id = $_GET["reseller_id"];
}

if ((isset($_GET['status']) && $_GET['status'] != '') && (isset($_GET['org']) && $_GET['org'] != '')) {
	$status = $_GET['status'];
	$org = $_GET['org'];
	$sql2   = " AND od_status = '$status' AND od_org_id = '$org'";
	$queryString = "&status=$status&org=$org";
} elseif (isset($_GET['org']) && $_GET['org'] != '') {
	$status = '';
	$org = $_GET['org'];
	$sql2   = " AND od_org_id = '$org'";
	$queryString = "&org=$org";
} elseif (isset($_GET['status']) && $_GET['status'] != '') {
	$status = $_GET['status'];
	$org = '';
	$sql2   = " AND od_status = '$status'";
	$queryString = "&status=$status";
} else {
	$status = '';
	$org = '';
	$sql2   = '';
	$queryString = '';
}	
// for paging
// how many rows to show per page
$rowsPerPage = 10;

$sql = "SELECT o.od_id, 
               o.mem_id,
               m.id, 
               name, 
               od_date,
               od_status,
               SUM(pd_price * od_qty) AS od_amount,
               od_five_day,
               od_delivery_date
	    FROM tbl_order o, tbl_order_item oi, tbl_product p, tbl_members m, tbl_reseller_organization ro
		WHERE oi.pd_id = p.pd_id AND o.od_id = oi.od_id AND o.mem_id = m.id AND ro.organization_id = o.od_org_id AND ro.reseller_id = $reseller_id $sql2
		GROUP BY od_id
		ORDER BY od_delivery_date ASC";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage, $queryString);

// populate organization dropdown
$sql = "SELECT o.id, o.name FROM tbl_organization o JOIN tbl_reseller_organization ro ON o.id = ro.organization_id WHERE ro.reseller_id = $reseller_id";
$orgsResult = dbQuery($sql);
$orderOrgOption = '';
while($row = dbFetchAssoc($orgsResult)) {
    extract($row);
    
    $orderOrgOption .= "<option value=\"". $id ."\"";
    if ($id == $org) {
            $orderOrgOption .= " selected";
    }

    $orderOrgOption .= ">$name</option>\r\n";
}

$orderStatus = array('New', 'Paid', 'Completed', 'Cancelled');
$orderStatusOption = '';
foreach ($orderStatus as $stat) {
	$orderStatusOption .= "<option value=\"$stat\"";
	if ($stat == $status) {
		$orderStatusOption .= " selected";
	}
	
	$orderStatusOption .= ">$stat</option>\r\n";
}

// check if this user is an admin
if( $_SESSION["is_admin"] ){

    // populate reseller dropdown
    $sql = "SELECT id, name FROM tbl_reseller WHERE active = 1";
    $orgsResult = dbQuery($sql);
    $resellerOption = '';
    while($row = dbFetchAssoc($orgsResult)) {
        extract($row);

        $resellerOption .= "<option value=\"". $id ."\"";
        if ($id == $reseller_id) {
                $resellerOption .= " selected";
        }

        $resellerOption .= ">$name</option>\r\n";
    }
    
}
?> 
<p>&nbsp;</p>
<form action="processOrder.php" method="post"  name="frmOrderList" id="frmOrderList">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="text">
        <tr align="center">
            
            <?php if( $_SESSION["is_admin"] ): ?>
            
            <td align="right">Reseller</td>
            <td width="75">
                <select name="reseller" class="box" id="reseller" onChange="viewOrder();">
                    <option value="" selected>All</option>
                    <?php echo $resellerOption; ?>
                </select>
            </td>
            
            <?php endif; ?>
            
            <td align="right">Organization</td>
            <td width="75">
                <select name="cboOrderOrg" class="box" id="cboOrderOrg" onChange="viewOrder();">
                    <option value="" selected>All</option>
                    <?php echo $orderOrgOption; ?>
                </select>
            </td>
            <td width="200" align="right">Status</td>
            <td width="75">
                <select name="cboOrderStatus" class="box" id="cboOrderStatus" onChange="viewOrder();">
                    <option value="" selected>All</option>
                    <?php echo $orderStatusOption; ?>
                </select>
            </td>
        </tr>
    </table>

 <table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="text">
  <tr align="center" id="listTableHeader"> 
   <td>Order #</td>
   <td>Customer Name</td>
   <td>Amount</td>
   <td>Ordered</td>
   <td>Deliver</td>
   <td>Status</td>
  </tr>
  <?php
$parentId = 0;
if (dbNumRows($result) > 0) {
	$i = 0;
	
	while($row = dbFetchAssoc($result)) {
		extract($row);
		$name = $name;
		
		if ($i%2) {
			$class = 'row1';
		} else {
			$class = 'row2';
		}
		
		$i += 1;
?>
  <tr class="<?php echo $class; ?>"> 
   <td><a href="<?php echo $_SERVER['PHP_SELF']; ?>?view=detail&oid=<?php echo $od_id; ?>"><?php echo $od_id; ?></a></td>
   <td><?php echo $name ?></td>
   <td align="right"><?php echo displayAmount($od_amount); ?></td>
   <td align="center"><?php echo $od_date; ?></td>
   <td align="center"><?php echo $od_delivery_date; ?></td>
   <td align="center"><?php echo $od_status; ?></td>
  </tr>
  <?php
	} // end while

?>
  <tr> 
   <td colspan="5" align="center">
   <?php 
   echo $pagingLink;
   ?></td>
  </tr>
<?php
} else {
?>
  <tr> 
   <td colspan="5" align="center">No Orders Found </td>
  </tr>
  <?php
}
?>

 </table>
 <p>&nbsp;</p>
</form>