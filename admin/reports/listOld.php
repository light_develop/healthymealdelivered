<?php
if (!defined('WEB_ROOT')) {
	exit;
}


if ((isset($_GET['status']) && $_GET['status'] != '') && (isset($_GET['org']) && $_GET['org'] != '')) {
	$status = $_GET['status'];
	$org = $_GET['org'];
	$sql2   = " AND od_status = '$status' AND od_org = '$org'";
	$queryString = "&status=$status&org=$org";
} elseif (isset($_GET['org']) && $_GET['org'] != '') {
	$status = '';
	$org = $_GET['org'];
	$sql2   = " AND od_org = '$org'";
	$queryString = "&org=$org";
} elseif (isset($_GET['status']) && $_GET['status'] != '') {
	$status = $_GET['status'];
	$org = '';
	$sql2   = " AND od_status = '$status'";
	$queryString = "&status=$status";
} else {
	$status = '';
	$org = '';
	$sql2   = '';
	$queryString = '';
}	
// for paging
// how many rows to show per page
$rowsPerPage = 10;

$sql = "SELECT o.od_id, 
               o.mem_id,
               m.id, 
               name, 
               od_date,
               od_status,
               SUM(pd_price * od_qty) AS od_amount,
               od_five_day,
               od_delivery_date
	    FROM tbl_order o, tbl_order_item oi, tbl_product p, tbl_members m
		WHERE oi.pd_id = p.pd_id and o.od_id = oi.od_id and o.mem_id = m.id $sql2
		GROUP BY od_id
		ORDER BY od_id DESC";
$result     = dbQuery(getPagingQuery($sql, $rowsPerPage));
$pagingLink = getPagingLink($sql, $rowsPerPage, $queryString);

$orderOrg = array('ING', 'MLT', 'SRT', 'Trinity');
$orderOrgOption = '';
foreach ($orderOrg as $organiz) {
	$orderOrgOption .= "<option value=\"". strtolower($organiz)."\"";
	if (strtolower($organiz) == $org) {
		$orderOrgOption .= " selected";
	}
	
	$orderOrgOption .= ">$organiz</option>\r\n";
}
$orderStatus = array('New', 'Paid', 'Completed', 'Cancelled');
$orderStatusOption = '';
foreach ($orderStatus as $stat) {
	$orderStatusOption .= "<option value=\"$stat\"";
	if ($stat == $status) {
		$orderStatusOption .= " selected";
	}
	
	$orderStatusOption .= ">$stat</option>\r\n";
}
?> 
<p>&nbsp;</p>
<form action="processOrder.php" method="post"  name="frmOrderList" id="frmOrderList">
    <table width="100%" border="0" cellspacing="0" cellpadding="2" class="text">
        <tr align="center">
            <td align="right">Organization</td>
            <td width="75"><select name="cboOrderOrg" class="box" id="cboOrderOrg" onChange="viewOrder();">
                    <option value="" selected>All</option>
                    <?php echo $orderOrgOption; ?>
                </select></td>
            <td width="200" align="right">Status</td>
            <td width="75"><select name="cboOrderStatus" class="box" id="cboOrderStatus" onChange="viewOrder();">
                    <option value="" selected>All</option>
                    <?php echo $orderStatusOption; ?>
                </select></td>
        </tr>
    </table>
        
    <table width="100%" border="0" align="center" cellpadding="5" cellspacing="1" class="text">
        <tr align="center" id="listTableHeader"> 
            <td width="60">Order #</td>
            <td width="150">Customer Name</td>
            <td width="60">Amount</td>
            <td >Ordered</td>
            <td >Deliver</td>
            <td width="70">Status</td>
        </tr>
  <?php
$parentId = 0;
if (dbNumRows($result) > 0) {
	$i = 0;
	
        $rows = array(); //array of orders
        
	while($row = dbFetchAssoc($result)) {
		extract($row);
		$name = $name;
		
		if ($i%2) {
			$class = 'row1';
		} else {
			$class = 'row2';
		}
		
                $order = array (
                    "name" => $name,
                    "od_id" => $od_id,
                    "mem_id" => $mem_id,
                    "id" => $id,
                    "od_date" => $od_date,
                    "od_status" => $od_status,
                    "od_amount" => $od_amount,
                    "od_five_day" => $od_five_day,
                    "od_delivery_date" => $od_delivery_date
                );
                $rows[] = $order;
                if($od_five_day)// if it's a five day order, add an order for each delivery day
                {
                    $delivDay = date("w", strtotime($od_delivery_date)); //5 = friday
                    $numRepeat = 5-$delivDay;//create this many more orders
                    for($i=1; $i <= $numRepeat; $i++)
                    {
                        $date = date("Y-m-d", strtotime($od_delivery_date . " +".$i." day"));
                        $order = array (
                            "name" => $name,
                            "od_id" => $od_id,
                            "mem_id" => $mem_id,
                            "id" => $id,
                            "od_date" => $od_date,
                            "od_status" => $od_status,
                            "od_amount" => $od_amount,
                            "od_five_day" => $od_five_day,
                            "od_delivery_date" => $date
                        );
                        $rows[] = $order;
                    }
                }
		$i += 1;
                
         } // end while     
        
        // Define the custom sort function
        function custom_sort($a,$b) {
            
            return $a['od_delivery_date']>$b['od_delivery_date'];
            
        }
        // Sort the multidimensional array
        usort($rows, 'custom_sort');
        
        
        foreach ($rows as $row)
        {
            $date = date("Y-m-d", strtotime($row['od_date']))."</br>".date("h:i A", strtotime($row['od_date']));
            //$date = $row['od_date'];
            echo "<tr class='$class'> 
                    <td width='60'><a href='".$_SERVER['PHP_SELF']."?view=detail&oid=".$row['od_id']."'>".$row['od_id']."</a></td>
                    <td>".$row['name']."</td>
                    <td width='60' align='right'>".displayAmount($row['od_amount'])."</td>
                    <td align='center'>$date</td>
                    <td align='center'>".$row['od_delivery_date']."</td>
                    <td width='70' align='center'>".$row['od_status']."</td>
                 </tr>";
        }
         
?>
  
        <tr> 
                <td colspan="5" align="center">
                    <?php
                    echo $pagingLink;
                    ?></td>
            </tr>
            <?php
        } else {
            ?>
            <tr> 
                <td colspan="5" align="center">No Orders Found </td>
            </tr>
            <?php
        }
        ?>
    
    </table>
    <p>&nbsp;</p>
</form>