<?php
if (!defined('WEB_ROOT')) {
	exit;
}

if (!isset($_GET['oid']) || (int)$_GET['oid'] <= 0) {
	header('Location: index.php');
}

$orderId = (int)$_GET['oid'];

$reseller_id = $_SESSION["reseller_id"];

// get ordered items
$sql = "SELECT pd_name, pd_price, od_qty, tea_name
	    FROM tbl_order_item oi, tbl_product p, tbl_teas t
		WHERE oi.pd_id = p.pd_id and oi.tea_id = t.tea_id and oi.od_id = $orderId
		ORDER BY od_id ASC";

$result = dbQuery($sql);
$orderedItem = array();
while ($row = dbFetchAssoc($result)) {
	$orderedItem[] = $row;
}


// get order information
$sql = "SELECT DATE_FORMAT(od_date, '%m-%d-%y %h:%i %p') as od_date, DATE_FORMAT(od_last_update, '%m-%d-%y %h:%i %p') as od_last_update, od_status, od_memo, od_org_id
	    FROM tbl_order 
		WHERE od_id = $orderId";

$result = dbQuery($sql);
extract(dbFetchAssoc($result));

// check to make sure this user is allowed to view this order
$check = "SELECT organization_id FROM tbl_reseller_organization WHERE organization_id = $od_org_id AND reseller_id = $reseller_id";
$checkr = dbQuery($check);
if( $checkr === false || mysql_num_rows($checkr) < 1 ){
    echo "You cannot view this order";
    exit;
}

$orderStatus = array('New','Paid','Shipped','Completed','Cancelled','Error');
$orderOption = '';
foreach ($orderStatus as $status) {
	$orderOption .= "<option value=\"$status\"";
	if ($status == $od_status) {
		$orderOption .= " selected";
	}
	
	$orderOption .= ">$status</option>\r\n";
}
?>
<p>&nbsp;</p>
<form action="" method="get" name="frmOrder" id="frmOrder">
    <table width="550" border="0"  align="center" cellpadding="5" cellspacing="1" class="detailTable">
        <tr> 
            <td colspan="2" align="center" id="infoTableHeader">Order Detail</td>
        </tr>
        <tr> 
            <td width="150" class="label">Order Number</td>
            <td class="content"><?php echo $orderId; ?></td>
        </tr>
        <tr> 
            <td width="150" class="label">Order Date</td>
            <td class="content"><?php echo $od_date; ?></td>
        </tr>
        <tr> 
            <td width="150" class="label">Last Update</td>
            <td class="content"><?php echo $od_last_update; ?></td>
        </tr>
        <tr> 
            <td class="label">Status</td>
            <td class="content"> <select name="cboOrderStatus" id="cboOrderStatus" class="box">
                    <?php echo $orderOption; ?> </select> <input name="btnModify" type="button" id="btnModify" value="Modify Status" class="box" onClick="modifyOrderStatus(<?php echo $orderId; ?>);"></td>
        </tr>
    </table>
</form>
<p>&nbsp;</p>
<table width="550" border="0"  align="center" cellpadding="5" cellspacing="1" class="detailTable">
    <tr id="infoTableHeader"> 
        <td colspan="4">Ordered Item</td>
    </tr>
    <tr align="center" class="label">
    	<td width="15">Qty</td> 
        <td style="width: 356px">Item</td>
        <td style="width: 69px">Unit Price</td>
        <td>Total</td>
    </tr>
    <?php
$numItem  = count($orderedItem);
$subTotal = 0;
for ($i = 0; $i < $numItem; $i++) {
	extract($orderedItem[$i]);
	$subTotal += $pd_price * $od_qty;
?>
    <tr class="content">
    	<td><?php echo $od_qty; ?></td> 
        <td style="width: 356px"><?php echo "$pd_name Shake & $tea_name Tea"; ?></td>
        <td align="right" style="width: 69px"><?php echo displayAmount($pd_price); ?></td>
        <td align="right"><?php echo displayAmount($od_qty * $pd_price); ?></td>
    </tr>
    <?php
}
?>
    <tr class="content"> 
        <td colspan="3" align="right">Total</td>
        <td align="right" style="width: 69px"><?php echo displayAmount($subTotal); ?></td>
    </tr>
</table>
<p>&nbsp;</p>
<p align="center"> 
    <input name="btnBack" type="button" id="btnBack" value="Back" class="box" onClick="window.history.back();">
</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
