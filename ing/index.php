<?php

require_once 'reseller.php';


require_once '../shared/library/config.php';


require_once '../shared/library/category-functions.php';


require_once '../shared/library/product-functions.php';

require_once '../shared/library/cart-functions.php';
require_once '../shared/library/common.php';


$_SESSION['shop_return_url'] = $_SERVER['REQUEST_URI'];

// TODO: may have to change link in login.php, lines 308, 384

$shakeInfo = (isset($_GET['shakeInfo']) && $_GET['shakeInfo'] != '') ? $_GET['shakeInfo'] : 0;
$contact = (isset($_GET['contact']) && $_GET['contact'] != '') ? $_GET['contact'] : 0;

$catId = (isset($_GET['c']) && $_GET['c'] != '1') ? $_GET['c'] : 0;
//$pdId = (isset($_GET['p']) && $_GET['p'] != '') ? $_GET['p'] : 0;
$login = (isset($_POST['login']) && $_POST['login'] != '') ? $_POST['login'] : 0;
$confirm = (isset($_GET['confirm']) && $_GET['confirm'] != '') ? $_GET['confirm'] : 0;
$rpass = (isset($_GET['rpass']) && $_GET['rpass'] != '') ? $_GET['rpass'] : 0;
$payment = (isset($_GET['id']) && $_GET['id'] != '') ? $_GET['id'] : 0;
$policy = (isset($_GET['policy']) && $_GET['policy'] != '') ? $_GET['policy'] : 0;

$manage = (isset($_GET['manage']) && $_GET['manage'] != '') ? $_GET['manage'] : 0;

if ($confirm) { // incoming url is response to account creation confirmation
    $login = 3;
} else if ($rpass) { // response to user-generated password reset confirmation
    $login = 4;
}

require_once '../shared/include/header.php';
?>
<style type="text/css">
    <!--
    body {
        background-image: url(../shared/images/online-shop-background.jpg);
        background-repeat: repeat-x;
    }

    .style11 {
        text-align: center;
        vertical-align: middle;
        z-index: 10;
        position: relative;
    }

    .style12 {
        font-size: x-small;
        vertical-align: middle;
    }

    -->
</style>

<link href="../shoppingcartCSS.css" rel="stylesheet" type="text/css"/>

<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td colspan="3">
            <?php require_once '../shared/include/top.php'; ?>
        </td>
    </tr>
    <tr>
        <td colspan="3" bgcolor="#FFFFFF" class="style11" align="center">
            &nbsp;
        </td>
    </tr>
    <tr valign="top">
        <td width="150" height="200" bgcolor="#FFFFFF" id="leftnav">
            <?php require_once '../shared/include/leftNav.php'; ?>
            <br>

            <form action="index.php" name="frmNewLogin" method="post">
                <?php
                $manageLink = '';
                if (isset($_SESSION['mname']) && $_SESSION['mname'] != "") {
                    $who = $_SESSION['mname'];
                    $mode1 = "Logout";
                    $mode2 = 6;
                    $manageLink = '<div style="text-align:left;padding-left:10px; padding-bottom: 10px;">
                                    <a href="index.php?manage=1"><strong>Manage Orders</strong></a>
                                </div>';

                } else {
                    $mode1 = "Login";
                    $who = "";
                    $mode2 = 1;
                }
                echo $manageLink;
                ?>
                <div style="text-align:left;padding-left:10px">
                    <a href="javascript: document.frmNewLogin.submit()"><strong><?php echo $mode1; ?></strong></a>
                </div>
                <input name="login" value="<?php echo $mode2; ?>" type="hidden"/>
            </form>
        </td>
        <td bgcolor="#FFFFFF" class="itemdescription2">
            <table width="480" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="20">&nbsp;</td>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="13" height="13" valign="top"><img
                                                    src="../shared/images/top-round-left.gif" alt="" width="13"
                                                    height="13"/></td>
                                            <td height="13"><img src="../shared/images/fill-box.gif" alt="" width="100%"
                                                                 height="13"/></td>
                                            <td width="13" height="13" valign="top"><img
                                                    src="../shared/images/top-round-right.gif" alt="" width="13"
                                                    height="13"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" bgcolor="#EADCB9" align="center">
                                    <p>
                                        <?php
                                        // Determine content based on url
                                        /*if ($pdId)
                                                        {
                                            require_once '../shared/include/productDetail.php';
                                        }
                                                        else*/
                                        if ($catId)
                                        {
                                            require_once '../shared/include/productList.php';
                                        }
                                        else if ($login)
                                        {
                                            require_once '../shared/include/members/login.php';
                                        }
                                        else if ($manage)
                                        {
                                            require_once '../shared/include/manage.php';
                                        }
                                        else if ($payment)
                                        {
                                            require_once '../shared/include/managePayment.php';
                                        }
                                        else if ($policy)
                                        {
                                            require_once '../shared/include/policy.php';
                                        }
                                        else if ($shakeInfo)
                                        {
                                            require_once "shakeInfo.php";
                                        }
                                        else if ($contact)
                                        {
                                            require_once "../shared/include/contact.php";
                                        }
                                        else
                                        {
                                        require_once '../shared/include/categoryList.php'; ?>

                                    <p>
                                    <div id="ordersInfo" style="text-align:center">&nbsp;</div>
                                    </p>

                                    <?php } ?>

                                    </p>

                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="13" height="14" valign="top"><img
                                                    src="../shared/images/bottom-round-left.gif" width="13"
                                                    height="13"/></td>
                                            <td height="13" valign="top"><img src="../shared/images/fill-box.gif"
                                                                              width="100%" height="14"/></td>
                                            <td width="13" height="14" valign="top"><img
                                                    src="../shared/images/bottom-round-right.gif" width="13"
                                                    height="13"/></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td width="20">&nbsp;</td>
                </tr>
            </table>
        </td>
        <td width="150" align="center" bgcolor="#FFFFFF">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td width="15">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <div
                            style="text-align:center;font-family: Arial, Helvetica, sans-serif;font-weight:bold;font-size:small">
                            <?php echo $who; /*
                if($who != ""){
                ?>
                <br />
                <a href="?id=<?php echo $_SESSION['mid']; ?>" style="font-size:smaller;">Manage Payment Methods</a>
                <?php }*/
                            ?>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><?php require_once '../shared/include/miniCart.php'; ?></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr valign="top">
        <td height="72" colspan="3" id="leftnav2"><img src="../shared/images/cartfooter.png" width="780" height="72"/>
        </td>
    </tr>
</table>
<script language="javascript">
    timeTimer = setTimeout("updateDeliveryInfo()", 1000);
    $(document).ready(function () {
        $.ajax('/shared/widgets/order_limit/index.php', {
            success: function (response) {
                $('#ordersInfo').html(response);
            }
        });
        setInterval(function () {
            $.ajax('/shared/widgets/order_limit/index.php', {
                success: function (response) {
                    $('#ordersInfo').html(response);
                }
            });
        }, 3000);
        setInterval(function () {
            $.ajax('/shared/library/receiveMessages.php', {
                success: function (response) {
                    
                }
            });
        }, 5000);
    });
</script>

<!--div class="modal_popup_background"></div>
<div class="popup">
    <div class="content_wrapper">
        <a class="close_btn" href="#">Close</a><br/>
        <div class="text_container"></div>
    </div>
</div-->

<script type="text/javascript">
    // show popup if it is successful payment
    $(document).ready(function () {
        if (getUrlParameterByName('payment') == 'success') {
            //alert('Thank you for your order!'); 
            //window.history.pushState({}, "Title", "/");
        }
    });
</script>

<?php
require_once '../shared/include/footer.php';
?>
<!--<?= date("Y-m-d H:i:s"); ?>-->
