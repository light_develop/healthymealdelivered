<?php
class Response
{
    const RESPONSE_CODE = "x_response_code";
    const AMOUNT = "x_amount";
    const PRODUCT_PRICE = "pd_price";

    protected $_post;
    protected $_tea;
    protected $_shake;
    protected $_product;

    public function __construct($post) {
        $this->_post = $post;
        $this->_tea = @$_COOKIE["tea"];
        $this->_shake = @$_COOKIE["shake"];
    }

    public function addOrder() {
        $sql = $this->_getInsertOrderSql();
        dbQuery($sql);
    }

    public function initProduct() {
        if(!empty($this->_product)) {
            return $this->_product;
        }

        if($this->hasResponse()) {
            $sql = "SELECT pd_price FROM tbl_product WHERE pd_id = ".$this->_shake;
            $productQuery = dbFetchAssoc(dbQuery($sql));

            if($productQuery) {
                $this->_product = new ArrayObject();

                foreach($productQuery as $key=>$value) {
                    $this->_product[$key] = $value;
                }
            }
        }
    }

    public function getMemberId() {
        return intval($_COOKIE['userID']);
    }

    public function getAmount() {
        return $this->_post[self::AMOUNT];
    }

    protected function _getInsertOrderSql() {
        return "INSERT INTO `tbl_order`
								(`od_date`,
								`od_last_update`,
								`od_org`,
								`od_org_id`,
								`mem_id`,
								`od_five_day`,
								`od_coupon`,
								`od_delivery_date`,
								`payment_profile_id`,
								`shipping_address_id`,
								`amount`)
                            VALUES
                            	(NOW(),
                            	NOW(),
                            	1,
                            	1,
                            	'".$this->getMemberId()."',
                            	'0',
                            	'0',
                            	NOW(),
                            	'0',
                            	'0',
                            	'".$this->getAmount()."')";
    }

    protected function _getInsertOrderItemSql() {
        $orderId = (int)dbInsertId();
        if(!$orderId) {
            throw new Exception("Cannot retrieve last order information");
        }

        return "INSERT INTO `tbl_order_item` (`od_id`,`pd_id`,`tea_id`,`od_qty`) VALUES
							({$orderId}, {$this->_shake}, {$this->_tea}, {$this->getQty()})";
    }

    public function getQty() {
        $this->initProduct();

        $amount = $this->_post[self::AMOUNT];
        $price = $this->_product[self::PRODUCT_PRICE];

        if($amount && $price) {
            return round($amount/$price, 0);
        } else {
            throw new Exception("Unable to calculate qty");
        }
    }

    public function hasResponse() {
        return !empty($this->_post) && !empty($this->_tea) && !empty($this->_shake);
    }

    public function isApproved() {
        return ($this->hasResponse() && isset($this->_post[self::RESPONSE_CODE]) && $this->_post[self::RESPONSE_CODE] == "1");
    }

    public function addOrderItem() {
        $sql = $this->_getInsertOrderItemSql();
        dbQuery($sql);
    }

    public function flushCookie() {
        unset($_COOKIE["tea"]);
        unset($_COOKIE["shake"]);
        setcookie("tea", "", -1);
        setcookie("shake", "", -1);
    }
}