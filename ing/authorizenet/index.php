<?php
require_once '../../shared/library/config.php';
require_once '../../shared/library/cart-functions.php';

require "Response.php";

$response = new Response($_POST);

try {
    if($response->isApproved()) {
        $response->addOrder();
        $response->addOrderItem();
        $response->flushCookie();
    } else {
        echo "You have problems with transaction. Please contact  us!";
    }
} catch (Exception $e) {
    echo $e->getMessage()."<br>Please Contact us!";
}

?>
<script src="/shared/library/jquery-1.7.2.min.js"></script>
<script src="/shared/library/jquery.cookie.js"></script>
<script>
    document.cookie = "tea=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/ing";
    document.cookie = "shake=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/ing";
</script>

<a href="/ing">To Main Page</a>

