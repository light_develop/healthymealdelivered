<?php
require_once '../../shared/library/config.php';
require_once '../../shared/library/cart-functions.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : 'view';
$cartId = (isset($_POST['hidCartId']) && $_POST['hidCartId'] != '') ? $_POST['hidCartId'] : '';

switch ($action) {
    case 'update' :
        foreach($_POST['txtQty'] as $qty)
        {
            if(!is_numeric($qty))
            {
                printf("<script>alert('Quantity must be a number');</script>");
                break 2;
            }
            $num = intval($qty);
            if(!($num > 0))
            {
                printf("<script>alert('Quantity must be a positive number');</script>");
                break 2;
            }
        }
        
        updateCart(0);
        
        break;
    case 'remove' :
        deleteFromCart($cartId);
        break;
}


$cartContent = getCartContent();
$numItem = count($cartContent);
?>
<!DOCTYPE html> 
<html> 
	<head> 
	<title>Minot Nutrition Addiction</title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1"> 

        <?php require 'head.php'; ?>
</head> 

<body> 

    <!-- Home -->
    
<div data-role="page" id="page1">
    
    <div data-theme="b" data-role="header">
        <h3>
            &nbsp;
        </h3>
        <a data-role="button" data-direction="reverse" data-transition="slide" href="index.php"
        data-icon="arrow-l" data-iconpos="left" class="ui-btn-left" data-theme="a">
            Main
        </a>
    </div> <!-- end header -->
    <div id="main" data-role="content" style="padding: 15px;">
        <h1>Your cart</h1>
        
        <?php
        
        if ($numItem > 0) {
            ?>
        <form id="updateForm" name="updateForm" method="post" action="cart.php?action=update" data-ajax="false" />
        <?php
            $subTotal = 0;
            for ($i = 0; $i < $numItem; $i++) {
                extract($cartContent[$i]);
                $updateUrl = "?action=update&cid=$ct_id";
                $removeUrl = "?action=remove&cid=$ct_id";
                $subTotal += $pd_price * $ct_qty;
        ?>
        
        
        <div class="cartItem">
            <span class="cartFirstLine">
                <span class="cartItemName"><?php echo $pd_name . " Shake <br />& " . $tea_name . " Tea"; ?></span>
                <span class="cartItemPrice"><?php echo displayAmount($pd_price); ?></span>
            </span>
            <span class="cartQty"><input id="<?php echo $ct_id; ?>" name="txtQty[]" type="text" value="<?php echo $ct_qty; ?>"/></span>
            <span class="cartUpdate"><a href="<?php echo $updateUrl; ?>" onclick="document.updateForm.submit();">update</a></span>
            <span class="cartSub"><strong>subtotal</strong><?php echo displayAmount($pd_price * $ct_qty); ?></span>
            <span class="cartRemove"><a href="<?php echo $removeUrl; ?>">remove</a></span>
        </div>
        
        
        <input name="hidCartId[]" type="hidden" value="<?php echo $ct_id; ?>">
        <input name="hidProductId[]" type="hidden" value="<?php echo $pd_id; ?>">
        
        <?php
            }
            
            $days = 0;
            $cdate = strtotime("this friday");
            $today = strtotime("today");
            //$difference = $cdate - $today;
            $delivery = strtotime(getDeliveryDate());
            $difference = $cdate - $delivery;

            if ($difference < 0) {
                $difference = 0;
            }//delivery is friday or later
            $days = floor($difference / 60 / 60 / 24);


            // The following code was lifted from the full version in shared/cart.php

            $subFour = $subTotal * $days;

            $chkdFive = '';
            if (isset($_SESSION['FiveDayOrder'])&&$_SESSION['FiveDayOrder']!=0) {
                $_SESSION['days'] = $days;
                $chkdFive = "checked = 'checked'";                
            } else {
                unset($_SESSION['days']);
                $chkdFive = "";
            }
            if (isset($_SESSION['chkCoupon'])) {
                $chkdCoupon = "checked = 'checked'";
                $subCoupon = 6.5;
                $subtractCoupon = "(subtracts $6.50)";
            } else {
                $chkdCoupon = "";
                $subCoupon = 0;
                $subtractCoupon = "";
            }
            $chkdCoupon = (isset($_SESSION['chkCoupon'])) ? "checked='checked'" : "";
            $disabled = '';
            $color = '#000';
            $blurb = "This option will repeat this order once a day for each day this week until this Friday.<br /> You can review, change or cancel these additional orders later using the Manage Orders page found on the home page.";
            if ($days < 1) {
                $disabled = 'disabled = "disabled"';
                $chkdFive = '';
                $color = '#777';
                $blurb = "This option is only available if there are additional available delivery days before this Friday.";
            }
            
            $today = date('Y-m-d');
            $friday = date('Y-m-d', strtotime('this friday'));

            $sql = "SELECT *
                    FROM tbl_holiday
                    WHERE date > '$today' AND date <= '$friday' AND
                        active = 1
                    ORDER BY date ASC";

            $result = dbQuery($sql);

            while ($rows[] = mysql_fetch_assoc($result));
            array_pop($rows);

            $notice = '';
            if (count($rows) > 0) {
                if (count($rows) >= $days)
                {
                    $disabled = 'disabled = "disabled"';
                    $chkdFive = '';
                    $color = '#777';
                    $blurb = "This option is only available if there are additional available delivery days before this Friday.";
                }
                //////////////////////////build string////////////////////////////////
                if ($days >= 1) {
                    $days = "";

                    if (count($rows) > 1)
                        $verb = " are holidays, so an order will not be placed for those days.";
                    else
                        $verb = " is a holiday, so an order will not be placed for that day.";

                    for ($x = 0; $x < count($rows); $x++) {
                        $days .= date("l", strtotime($rows[$x]["date"]));

                        if (count($rows) > 2 && $x + 1 < count($rows)) {
                            if ($x + 2 == count($rows)) {
                                $days .= ", and ";
                            } else {
                                $days .= ", ";
                            }
                        } elseif (count($rows) > 1 && $x + 1 < count($rows)) {
                            $days .= " and ";
                        }
                    }

                    $notice = $days . $verb;


                }
            }
            
            $repeat = "repeat";
            $display1 = "hide";
            $display2 = "show";
            
            if(strlen($chkdFive) > 0)
            {
                $repeat = "repeatSmall";
                $display1 = "show";
                $display2 = "hide";
            }
       ?>
        
        </form>
        
        <form method="post" action="checkout.php?step=1" name="checkForm" id="checkForm">
        <div class="cartItem">
            <div id="repeat" class="<?php echo $repeat; ?>">
                <input id="chkFive" name="chkFive" type="checkbox" autocomplete="off" value="<?php echo $days; ?>" <?php echo $chkdFive . " " . $disabled; ?>>
                <label for="chkFive">
                    Repeat this order
                </label>
                
                <input id="fromForm" name="fromForm" type="hidden" value="1" />
            </div>
            <span id="repeatPrice" class="cartRepeatPrice <?php echo $display1; ?>" ><?php echo displayAmount($subFour); ?></span>
            <span class="info">Repeat this order for each day this week (holidays excluded)</span>
        </div>
        </form>
        
        <div class="cartTotal">
            <span id="jimmy" style="float: right;" class="<?php echo $display2; ?>" ><?php echo displayAmount($subTotal - $subCoupon); ?></span>
            <span id="wRepeat" style="float: right;" class="<?php echo $display1; ?>" ><?php echo displayAmount($subTotal + $subFour - $subCoupon); ?></span>
            <span class="totalLabel">Total</span>
        </div>
        
        <div class="delivery">
            This order will be delivered on <strong><?php echo date("m/d/Y", $delivery); ?></strong>
        </div>
        
        <?php
        if ($numItem > 0) {

            if(isset($_SESSION['mid']) && $_SESSION['mid'] != '')
            {
                $id = $_SESSION['mid'];
                $sql1 = "SELECT * FROM tbl_members WHERE id =$id";

                $result1 = dbQuery($sql1);

                $rows = mysql_fetch_array($result1);
                $custProfID = $rows['customerProfileId'];
                $email = $rows['email'];
                $merchantCustomerId = getMerchantCustomerId($email);

                include_once ("../../shared/authorize/php_cim/XML/vars.php");
                include_once ("../../shared/authorize/php_cim/XML/util.php");

                $request = '<?xml version="1.0" encoding="utf-8"?>
                                <getHostedProfilePageRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                                    ' . MerchantAuthenticationBlock() . '
                                    <customerProfileId>' . $custProfID . '</customerProfileId>
                                    <hostedProfileSettings>
                                        <setting>
                                            <settingName>hostedProfileReturnUrl</settingName>
                                            <settingValue>http://'.BASE_URL.$_SESSION['organization'].'/mobile/checkout.php?step=1</settingValue>
                                        </setting>
                                        <setting>
                                            <settingName>hostedProfileReturnUrlText</settingName>
                                            <settingValue>Continue to checkout</settingValue>
                                        </setting>
                                        <setting>
                                            <settingName>hostedProfilePageBorderVisible</settingName>
                                            <settingValue>true</settingValue>
                                        </setting>
                                    </hostedProfileSettings>
                                </getHostedProfilePageRequest>';

                $response = send_xml_request($request);

                $parsedresponse = parse_api_response($response);
                if ("Ok" == $parsedresponse->messages->resultCode) {
                    $token = $parsedresponse->token;

                    $_SESSION['custProfID'] = $custProfID;
                }


                $request = '<?xml version="1.0" encoding="utf-8"?>
                        <getCustomerProfileRequest xmlns="AnetApi/xml/v1/schema/AnetApiSchema.xsd">
                            ' . MerchantAuthenticationBlock() . '
                            <customerProfileId>' . $custProfID . '</customerProfileId>
                        </getCustomerProfileRequest>';

                $response = send_xml_request($request);

                $parsedresponse = parse_api_response($response);
                if ("Ok" == $parsedresponse->messages->resultCode) {
                    $paymentProfile = $parsedresponse->profile->paymentProfiles;
                }

                if (isset($paymentProfile) && !empty($paymentProfile)) {
                    //customer has already entered payment information
                    $_SESSION['saveinfo'] = 1;
                    //show button to choose info
                    echo'<a data-role="button" data-transition="slide" data-theme="b" href="#" onClick="document.getElementById(\'checkForm\').submit();">
                            Start Checkout
                        </a>';
                } else {
                    //customer has not yet entered payment info
                    //show button to add info
                    echo'<form method="post" action="https://test.authorize.net/profile/manage"
                        id="formAuthorizeNetPage" style="display:none;">
                        <input type="hidden" name="Token" value="' . $token . '"/>
                        </form>
                        <a data-role="button" data-transition="slide" data-theme="b" href="#" onClick="document.getElementById(\'formAuthorizeNetPage\').submit();">
                            Start Checkout
                        </a>'; //https://secure.authorize.net/profile/manage
                }
            }
            else
            {
                ?>
                <h2 align="center">You must log in to check out</h2>
                <p class="info">Please log in or create an account to complete the checkout process.</p>
                <?php
            }
        }
        ?>
        
    
    <?php
    } else {
    ?>
    <h2 align="center">You shopping cart is empty</h2>
    <div class="info">If you find you are unable to add anything to your cart, please ensure that 
        your internet browser has cookies enabled and that any other security software 
        is not blocking your shopping session.</div>
    <?php
}

?>
    </div><!-- end content -->
    
   <?php include_once "footer.php"; ?>
    
    </div>
    
</body>
</html>