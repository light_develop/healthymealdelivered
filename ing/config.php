<?php 
ini_set('display_errors', 'On');
//ob_start("ob_gzhandler");
//error_reporting(E_ALL);

// start the session
@session_start();

$_domain  = "www.healthymealdelivered.com";
$_parent_path = "/";
$base_url = $_domain.$_parent_path;




define('BASE_URL', $base_url);

$size = (isset($_POST['size']) && $_POST['size'] != '') ? $_POST['size'] : 0;

if ($size == 'full')
    $_SESSION["viewFull"] = 1;
else if ($size == 'small')
    $_SESSION["viewFull"] = 0;

// check if the user is visiting on a mobile device
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

if( !function_exists("getCDYNELicense") ){
    function getCDYNELicense(){
        return "229a98fc-93ea-4beb-a878-9af7c24f5374";
    }
}

// require_once '../../private/credents.php';
// database connection config
// development
// $dbHost = 'localhost';
// $dbUser = 'u_nutritio';
// $dbPass = 'R0WPEzum';
// $dbName = 'nutrition';

// setting up the web root and server root for
// this shopping cart application
$thisFile = str_replace('\\', '/', __FILE__);
$docRoot = $_SERVER['DOCUMENT_ROOT'];


$webRoot  = str_replace(array($docRoot, 'shared/library/config.php'), '', $thisFile);
$srvRoot  = str_replace('shared/library/config.php', '', $thisFile);

define('WEB_ROOT', $webRoot);
define('SRV_ROOT', $srvRoot);
require_once $docRoot.'/credents.php';
// these are the directories where we will store all
// category and product images
define('CATEGORY_IMAGE_DIR', 'shared/images/category/');
define('PRODUCT_IMAGE_DIR',  'shared/images/product/');

// some size limitation for the category
// and product images

// all category image width must not 
// exceed 75 pixels
define('MAX_CATEGORY_IMAGE_WIDTH', 75);

// do we need to limit the product image width?
// setting this value to 'true' is recommended
define('LIMIT_PRODUCT_WIDTH',     true);

// maximum width for all product image
define('MAX_PRODUCT_IMAGE_WIDTH', 300);

// the width for product thumbnail
define('THUMBNAIL_WIDTH',         75);

if (!get_magic_quotes_gpc()) {
	if (isset($_POST)) {
		foreach ($_POST as $key => $value) {
                    if( !is_array($value) )
			$_POST[$key] =  trim(addslashes($value));
		}
	}
	
	if (isset($_GET)) {
		foreach ($_GET as $key => $value) {
                    if( !is_array($value) )
			$_GET[$key] = trim(addslashes($value));
		}
	}	
}

// since all page will require a database access
// and the common library is also used by all
// it's logical to load these library here
require_once 'database.php';
require_once 'common.php';

// get the shop configuration ( name, addres, etc ), all page need it
$shopConfig = getShopConfig();


if(isMobile())
{    
    $dirs = explode('/',getcwd());
    $dir  = $dirs[count($dirs)-1];
    
    if(!isset($_SESSION["viewFull"]))
        $_SESSION["viewFull"] = 0;
    
    if((!isset($_SESSION["isMobile"]) || $dir != 'mobile') && $_SESSION["viewFull"] == 0)
    {
        $_SESSION["isMobile"] = true;
        header("location: ".$base_url.$_SESSION['organization']."/mobile");
    }
}
else
{
    $_SESSION["isMobile"] = false;
}
?>