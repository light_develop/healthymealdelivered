<?php
header("Location: /ing/");
//printf("<script >location.href='/ing/'</script>");
exit;

/**
 * Version 0.0.1
 */

require_once 'library/config.php';
require_once 'library/category-functions.php';
require_once 'library/product-functions.php';
require_once 'library/cart-functions.php';

$_SESSION['shop_return_url'] = $_SERVER['REQUEST_URI'];

$catId  = (isset($_GET['c']) && $_GET['c'] != '1') ? $_GET['c'] : 0;
$pdId   = (isset($_GET['p']) && $_GET['p'] != '') ? $_GET['p'] : 0;

require_once 'include/header.php';
?><style type="text/css">
<!--
body {
	background-image: url(images/online-shop-background.jpg);
	background-repeat: repeat-x;
}
-->
</style>
<link href="shoppingcartCSS.css" rel="stylesheet" type="text/css" />
<table width="780" border="0" align="center" cellpadding="0" cellspacing="0">
 <tr>
  <td colspan="3" bgcolor="#FFFFFF">
  <?php require_once 'include/top.php'; ?>
  </td>
 </tr>
 <tr valign="top">
  <td width="150" height="400" bgcolor="#FFFFFF" id="leftnav"><?php
require_once 'include/leftNav.php';
?></td>
  <td bgcolor="#FFFFFF" class="itemdescription2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="20">&nbsp;</td>
      <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="13" height="13" valign="top"><img src="images/top-round-left.gif" alt="" width="13" height="13" /></td>
              <td height="13"><img src="images/fill-box.gif" alt="" width="100%" height="13" /></td>
              <td width="13" height="13" valign="top"><img src="images/top-round-right.gif" alt="" width="13" height="13" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top" bgcolor="#EADCB9"><p>
            <?php
if ($pdId) {
	require_once 'include/productDetail.php';
} else if ($catId) {
	require_once 'include/productList.php';
} else {
	require_once 'include/categoryList.php';
}
?>
          </p></td>
        </tr>
        <tr>
          <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="13" height="14" valign="top"><img src="images/bottom-round-left.gif" width="13" height="13" /></td>
              <td height="13" valign="top"><img src="images/fill-box.gif" width="100%" height="14" /></td>
              <td width="13" height="14" valign="top"><img src="images/bottom-round-right.gif" width="13" height="13" /></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top">&nbsp;</td>
        </tr>
      </table></td>
      <td width="20">&nbsp;</td>
    </tr>
  </table></td>
  <td width="130" align="center" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td>&nbsp;</td>
      <td width="15">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><?php require_once 'include/miniCart.php'; ?></td>
      <td>&nbsp;</td>
    </tr>
  </table></td>
 </tr>
 <tr valign="top">
   <td height="72" colspan="3" id="leftnav2"><img src="../cart/images/online-shop-footer.png" width="780" height="72" /></td>
  </tr>
</table>
<?php
require_once 'include/footer.php';
?>
