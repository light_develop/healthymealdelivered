<?php

function checkUser($id) {
	$query = "SELECT id FROM tbl_members WHERE id = $id";
	$result = mysql_query($query) or die (mysql_error());
	
	if($result)
	{
		return false;
	}
	else
	{
		return true;
	}
}
function checkUserEmail($email) {
	$query = "SELECT email FROM tbl_members WHERE email = '$email'";
	$result = mysql_query($query) or die (mysql_error());
	$canAdd = mysql_fetch_array($result);
	// echo $canAdd;
	// echo 'is canAdd';
	if($canAdd)
	{
		return false;
	}
	else
	{
		return true;
	}
}
function readData($id,$bin) {
	global $key;
	$canRead = checkUser($id);
	if(!$canRead)
	{
		$query = "SELECT mdata, email FROM tbl_members WHERE id = $id";
		//echo $query;
		$result = mysql_query($query) or die (mysql_error());
		$dbBlob = mysql_fetch_row($result);
		$data = $dbBlob[0];
		$theEmail = $dbBlob[1];
		//echo $data.'<br/>';
		$blob = decryptdata($data,$key) or die('Could not decrypt data: readData()');
		/*$exploded = explode('|',$blob);
		
		for ($x=0;$x < 16;$x++)
		{
			if($bin[$x] == '1')
			{
				$newString[] = $exploded[$x];
			}
		}
		$newString = implode('|',$newString);
		
		return $newString;*/
		return $blob.'|'.$theEmail;
	}
	else
	{
		return 0;
	}
}
function writeData($id,$email,$bin,$str) { 
	global $key;
	$canAdd = checkUserEmail($email);
	if($canAdd)
	{
		//$newsql = "INSERT INTO tbl_members (id, mdata) VALUES (default, default)";
		$date = date("Y-m-d");
		$counter = 0;
		$exploded = explode('|',$str);
		for ($x=0;$x < 17;$x++)
		{
			if($bin[$x] == '1')
			{
				if($x == 0)//puts the server timestamp on the string
				{
					$newString[] = $date;///adds start date
					$newString[] = $date;///adds last mod date
					$x++;
				}
				else
				{
					$newString[] = $exploded[$counter++];
				}
			}
			else
			{
				$newString[] = 'null';
			}
		}
		$newString = implode('|',$newString);
		$encryData = encryptdata($newString,$key) or die ('Could not encrypt data: writeData()');
		

		$add = "INSERT INTO tbl_members (id,email,mdata) 
				VALUES (default,'$email','$encryData')";
				
		$query = mysql_query($add) or die (mysql_error());
		
		return mysql_insert_id();
	}
	else
	{
		return 0;
	}
}
function modData($id, $email, $bin,$str) {
	global $key;
	$canModify = checkUser($id);
	$date = date("Y-m-d");
	if(!$canModify)
	{
		$newStr = explode('|',$str);
		$counter = 0; 
		
		$sql = "SELECT mdata FROM tbl_members WHERE id = $id";
		$query = mysql_query($sql) or die (mysql_error());
		$string = mysql_fetch_array($query);
		$decryData = decryptdata($string[0],$key) or die('Could not decrypt data: modData()');
		$db_string = explode('|',$decryData);
		
		//THIS LOOP MODIFIES THE STRING WITH THE NEW INFO SKIPPING THE START DATE
		for($x=0;$x<17;$x++)
		{
			if($bin[$x] == '1')
			{
				if($x == 1)
				{
					$db_string[$x] = $date;
				}
				elseif ($x != 0)
				{
					$db_string[$x] = $newStr[$counter++];
				}
			}
		}
		
		//BUILD THE STRING
		$newDbString = implode('|',$db_string);
		$encryData = encryptdata($newDbString,$key) or die ('Could not encrypt data: modData()');
		//UPDATE TABLE
		if(!$email)
		{
			$newsql =  	"UPDATE tbl_members
						SET mdata = '$encryData'
						WHERE id = $id";
			mysql_query($newsql) or die (mysql_error());
		}
		else
		{
			$newsql =  	"UPDATE tbl_members
						SET mdata = '$encryData', email = '$email'
						WHERE id = $id";
			mysql_query($newsql) or die (mysql_error());
		}
		return 1;
	}
	else
	{
		return 0;
	}	
}
function delData($id,$email,$bin,$str) {
	global $key;
	$canDelete = checkUser($id);
	if(!$canDelete)
	{
		if($str != '')
		{
			//echo 'made it pass str if';
			$newStr = explode('|',$str); 
			$sql = "SELECT mdata FROM tbl_members WHERE id = $id";
			$query = mysql_query($sql) or die (mysql_error());
			$string = mysql_fetch_array($query);
			$decryData = decryptdata($string[0], $key) or die('Could not decrypt data: delData()');
			$db_string = explode('|',$decryData);
			
			//THIS LOOP LOOKS FOR THE DATA TO BE DELETED
			for($x=2;$x<17;$x++)
			{
				if($bin[$x] == '1')
				{	
					$db_string[$x] = 'null';				
				}		
			}
			//BUILD THE STRING
			$newDbString = implode('|',$db_string);
			$encryData = encryptdata($newDbString, $key) or die ('Could not encrypt data: delData()');
			//UPDATE TABLE
			if(!$email)
			{
				$newsql =  	"UPDATE tbl_members
							SET mdata = '$encryData'
							WHERE id = $id";
				mysql_query($newsql) or die (mysql_error());
			}
			else
			{
				$newsql =  	"UPDATE tbl_members
							SET mdata = '$encryData', email = '$email'
							WHERE id = $id";
				mysql_query($newsql) or die (mysql_error());
			}
			return 1;
		}
		else
		{
			if($email)
			{
				//echo 'made it pass email if';
				$newsql = 	"UPDATE tbl_members
							SET email = 'null'
							WHERE id = $id";
				mysql_query($newsql) or die (mysql_error());
				
				return 1;
			}
			else
			{
				return 0;
			}
		}
	}
	else
	{
		return 0;
	}
}
include 'shared/include/cryptor.php';

/*	///TESTING BLOCK///
$id = 19;
$bin = '0110000100000000010'; 
$bin = '0110000100000000011'; 
$bin = '1111111111111111100'; 
$bin = '1111111111111111101'; 
$string = 'name|status|email|pass|passrst|phone|usecc|sdate|card_type|card_num|card_month|card_year|
		   card_name|card_addy|card_city|card_state|card_zip';
$mstring = 'new_status|new_email|new_sdate';*/

///MAIN PROGRAM///
$id = $_GET["id"];
$email = $_GET["email"];
$bin = $_GET["binary"];
$string = $_GET["data"];

$dbHost = 'localhost';
$dbUser = 'ordermna_access';
$dbPass = 'GFw1&gB%m}Jq';
$dbName = 'ordermna_cartdb';

$con = mysql_pconnect($dbHost,$dbUser,$dbPass);

if(!$con)
{
	die('Could not connect to database: '.mysql_error());
}
else
{
	//echo "Connected to database<br/><br/>";
	mysql_select_db($dbName);
}
	
//GRABS THE 2 RIGHT MOST DIGITS
$operation = $bin[(strlen($bin)-1)-1];
$operation .= $bin[strlen($bin)-1];


switch ($operation)
{
	case "00": //READ
	{
		$read = readData($id,$bin);
		echo $read;
		break;
	}
	case "01": //WRITE
	{	
		$write = writeData($id,$email,$bin,$string);
		echo $write;
		break;
	}
	case "10": //MODIFY
	{
		$modified = modData($id,$email,$bin,$string);
		echo $modified;
		break;
	}
	case "11": //DELETE
	{
		$deleted = delData($id,$email,$bin,$string);
		echo $deleted;
		break;
	}
}
?>
